import { Card, Row, Col, Pagination, notification, Rate } from "antd";
import {Link, NavLink} from "react-router-dom";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { addToCart } from "../../service/ApiService/cartApi";
import { useSelector } from "react-redux";
import { getShop, getShopId } from "../../service/ApiService/shopApi";
import { useEffect, useState } from "react";
import { addViewProduct } from "../../service/ApiService/productsApi";
const { Meta } = Card;

const ProductPage = ({ product }) => {
  const user = useSelector((state) => state.user);
  const [shop, setShop] = useState({});

  const handleAddCart = async () => {
    const res = await addToCart({
      user: user.id,
      shop: product.shop.id,
      product: product.id,
    });
    if (res.status === 200) {
      notification.success({ message: "Thêm vào giỏ hàng thành công" });
    }
  };

  const handleClick = async (productId) => {
   if(user.id){
     const { data } = addViewProduct({
       user: user.id,
       product: productId,
     });
   }
  };

  return (
    <div className="product-list">
      <Card
        onClick={() => handleClick(product.id)}
        className="mt-4 pro_card"
        hoverable
        style={{maxWidth: "100%",height:310 }}
        cover={
          <img
            style={{
              borderRadius: "10px",
              maxHeight: "100%",
              height: "175px",
            }}
            alt="example"
            src={product?.img}
          />
        }
      >
        <Link to={`/product/${product?.id}`}>
          <Meta
            style={{ padding: "10px" }}
            title={product?.name}
            description={
              <div>
                <span
                  style={{ color: "rgb(255, 66, 78)" }}
                >{`${Intl.NumberFormat().format(product?.price)} đ`}</span>
                <div
                  style={{
                    color: "rgb(143 139 139)",
                    fontSize: "12px",
                    marginTop: "5px",
                    width: "100%",
                  }}
                >
                  {product?.shop?.name}
                </div>
                {product?.rate ? (
                  <div className="d-flex">
                    <Rate
                      style={{ fontSize: "10px" }}
                      disabled
                      defaultValue={product?.rate/product?.slot}
                    />
                    <span style={{ marginTop: "4px", marginLeft: "3px" }}>
                    ({product?.slot})
                  </span>
                  </div>
                ):(
                  <div style={{fontSize:"10px"}}>
                    Chưa có đánh giá
                  </div>
                  )}
              </div>
            }
          />
        </Link>

        <div>
          <ShoppingCartOutlined
            onClick={handleAddCart}
            className="add-cart"
            style={{ fontSize: "24px" }}
          />
        </div>
      </Card>
    </div>
  );
};
export default ProductPage;
