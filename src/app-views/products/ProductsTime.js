import '../cusstom.css'
import {Card, Row, Col,Pagination} from "antd";
import { NavLink } from 'react-router-dom'
const {Meta} = Card;
let active = 1;

const ProductTime = ({product}) => {
      return(
      <>
            <Col xxl={4} xl={4} xs={12} sm={6}
            >
              <div  className="product-list">
                <Card
                  className="mt-4 pro_card"
                  hoverable
                  style={{width: 200, maxWidth: "100%"}}
                  cover={<img style={{borderRadius: "10px"}} alt="example"
                              src={product.img}/>}
                >
                  <NavLink to={`/product/detail`}> <Meta title={product.name} description={product.price}/> </NavLink>
                </Card>
              </div>
            </Col>

      </>
  )
}
export default ProductTime;