import { Card, Table, Empty,Row,Col } from "antd";
import { getMyOder } from "../../service/ApiService/oderUserApi";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";


const MyOder = () => {
  const [orders, setOrders] = useState([]);
  const user = useSelector((state) => state.user);
  useEffect(() => {
    getOder();
  }, []);
  const getOder = async () => {
    const { data } = await getMyOder(user.id);
    setOrders(data.results);
  };
  return (
    <div className="container">
      <Card title="Đơn hàng của bạn">
        {orders.length > 0 ? (
            orders.map(item => {
              return (
                <Card style={{fontSize:"13px"}} className="mt-2" key={item.id}>
                   <div>Mã đơn hàng: <span>{item.code}</span></div>
                   <div className="product-list">
                     Sản phẩm :
                     <Row gutter={16}>
                     {item.products.map(value=>{
                        return (
                          <Col span={8} className="mt-2">
                              <div>{value.product?.name}</div>
                              <img width="100px" src={value.product?.img}/>
                          </Col>
                        )
                      })}
                     </Row>
                   </div>
                   <div className="mt-2">Trạng thái: <span style={{color: 'rgb(52 181 20)'}}>{item.status? "Đã nhận hàng" : "Chờ xác nhận"}</span></div>
                </Card>
              )
            })
        ) : (
          <Empty />
        )}
      </Card>
    </div>
  );
};
export default MyOder;
