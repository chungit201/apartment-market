import "../cusstom.css";
import {Button, Col, Pagination, Row, Spin, Tabs, Card, Skeleton} from "antd";
import {useEffect, useState} from "react";
import ProductPage from "../products/ProductPage";
import SliderSale from "../sale-slide/SliderSale";
import CategoriesLayOut from "../../components/common-component/Categories";
import {
  getProducts,
  getProductSale,
  getProductViewTop,
  listProductTime,
} from "../../service/ApiService/productsApi";
import ProductsTime from "../products/ProductsTime";
import {EditOutlined, LoadingOutlined} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {getCategory} from "../../service/ApiService/categoryApi";
import ProductCarousel from "react-elastic-carousel";
import moment from "moment";
import ShopsLayOut from "../../components/common-component/Shops";
import {Link} from "react-router-dom";
import {getShops} from "../../service/ApiService/shopApi";
import {parse} from "@fortawesome/fontawesome-svg-core";

const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;

const {TabPane} = Tabs;

function callback(key) {
}

const HomePage = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [productSale, setProductSale] = useState([]);
  const [productTime, setProductTime] = useState([]);
  const [category, setCategory] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [viewMore, setViewMore] = useState(false);
  const [topView, setTopView] = useState([]);
  const user = useSelector((state) => state.user);
  const [page, setPage] = useState(2);
  const [totalPageView, setTotalPageView] = useState(0);
  const [totalPageTime, setTotalPageTime] = useState(0);
  const [totalView, setTotalView] = useState(0);
  const [topShop, setTopShop] = useState([]);
  const limit = 24;

  const breakPoints = [
    {width: 100, itemsToShow: 1},
    {width: 100, itemsToShow: 2},
    {width: 100, itemsToShow: 3},
    {width: 100, itemsToShow: 4},
  ];

  const showModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    showModal();
    getDataProducts();
    getDataProductSale();
    getDataCategories();
    getProductTime();
    getTopProducts();
    getTopShop();
  }, []);

  const getProductTime = async () => {
    let today = new Date();
    let time = today.getHours();
    if (19 < time && time < 24) {
      console.log(1);
      const {data} = await listProductTime("snacks", 1, 12);
      setTotalPageTime(data.totalResults)
      setProductTime(data.results);
    } else if (5 < time && time < 10) {
      console.log(2);
      const {data} = await listProductTime("breakfast", 1, 12);
      setTotalPageTime(data.totalResults)
      setProductTime(data.results);
    } else if (10 < time && time < 14) {
      console.log(3);
      const {data} = await listProductTime("rice", 1, 12)
      setTotalPageTime(data.totalResults)
      setProductTime(data.results);
    } else {
      console.log(4);
      const {data} = await listProductTime("food", 1, 12);
      setTotalPageTime(data.totalResults)
      setProductTime(data.results);
    }
  };
  const getDataProductSale = async () => {
    const {data} = await getProductSale();
    setProductSale(data.results);
  };
  const getDataCategories = async () => {
    const {data} = await getCategory(1, 16);
    setCategory(data.results);
  };

  const getDataProducts = async () => {
    setLoading(true);
    await getProducts(1, limit).then(async (res) => {
      setProducts(res.data.results);
      setTotalPage(res.data.totalResults);
    });
    await setLoading(false);
  };
  const handleChange = async () => {
    setViewMore(true);
    setPage(page + 1);
    await getProducts(page, limit).then(async (res) => {
      setProducts(products.concat(res.data.results));
    });
    setViewMore(false);
  };

  const getTopShop = async () => {
    const {data} = await getShops({
      page: 1,
      limit: 3,
      sortBy: "-order",
    });
    setTopShop(data.results);
  };

  const getTopProducts = async () => {
    const getTimestamp = moment().subtract(7, "d");
    const toDay = new Date();
    const {data} = await getProductViewTop({
      page: 1,
      limit: 24,
      start: moment(getTimestamp).format("x"),
      end: toDay.getTime(),
      action: "active",
      sortBy: "-views",
    });
    setTotalView(data.totalResults);
    setTopView(data.results);
    setTotalPageView(data.totalPages);
  };

  const handleChangView = async (value) => {
    const getTimestamp = moment().subtract(7, "d");
    const toDay = new Date();
    const {data} = await getProductViewTop({
      page: value,
      limit: 24,
      start: moment(getTimestamp).format("x"),
      end: toDay.getTime(),
      sortBy: "-views",
    });
    setTopView(data.results);
  };
  return (
    <Row>
      <Col span={24}>
        <div className="container">
          <Row gutter={8}>
            <Col xxl={24} xl={24} xs={0} sm={0} lg={0}>
              <h5 className="mt-4">
                Giảm giá{" "}
                <img src="https://frontend.tikicdn.com/_desktop-next/static/img/dealFlashIcon.svg"/>
              </h5>
              <ProductCarousel
                style={{backgroundColor: "#fff", borderRadius: "10px"}}
                showEmptySlots={false}
                className="mt-2"
                breakPoints={breakPoints}
              >
                {productSale.map((item) => {
                  return (
                    <Skeleton loading={loading} active avatar>
                      <SliderSale key={item.id} product={item}/>;
                    </Skeleton>
                  );
                })}
              </ProductCarousel>
            </Col>
          </Row>

        </div>
        <div className="container">

          <h5 className="mt-4">Danh mục nổi bật</h5>
          <Card className="mt-2">
            <div className="container">
              <Row gutter={16}>
                {category.map((item) => {
                  return <CategoriesLayOut key={item.id} category={item}/>;
                })}
              </Row>
            </div>
          </Card>

        </div>

        <div className="container mt-4">
          <Row gutter={16}>
            <Col span={8}>
              <img
                style={{maxWidth: "100%", borderRadius: "20px"}}
                src="https://salt.tikicdn.com/cache/w400/ts/banner/da/66/b1/42c61f831ab028c9c5d1504a9b337bb1.png.webp"
              />
            </Col>

            <Col span={8}>
              <img
                style={{maxWidth: "100%", borderRadius: "20px"}}
                src="https://salt.tikicdn.com/cache/w400/ts/banner/e1/38/23/50d62cd9172f45a5d689b915a7fb3199.png.webp"
              />
            </Col>

            <Col span={8}>
              <img
                style={{maxWidth: "100%", borderRadius: "20px"}}
                src="https://salt.tikicdn.com/cache/w400/ts/banner/0c/d3/9f/1746c188f796000804e04918a7141530.png.webp"
              />
            </Col>
          </Row>
        </div>

        <div className="container">
          <div className="title-content mt-4">
            <h5>Gợi ý hôm nay</h5>
            <Card>
              <div className="container">
                <Row gutter={8}>
                  {productTime.map((item) => {
                    return (
                      <Col xxl={4} xl={4} xs={12} sm={6}>
                        <ProductPage key={item.id} product={item}/>
                      </Col>
                    );
                  })}
                </Row>
              </div>
            </Card>
            <div className="mt-2" style={{width: "100%"}}>
              <Pagination
                style={{float: "right"}}
                defaultCurrent={1}
                total={parseInt(`${totalPageTime}`)}
              />
            </div>
          </div>

          <div className="title-content mt-4">
            <h5>Sản phẩm nổi bật</h5>
            <Card>
              <div className="container">
                <Row gutter={8}>
                  {topView &&
                    topView.map((item) => {
                      return (
                        <Col xxl={4} xl={4} xs={12} sm={6}>
                          <ProductPage key={item.id} product={item.product}/>
                        </Col>
                      );
                    })}
                </Row>
              </div>
            </Card>
            <div className="mt-2" style={{width: "100%"}}>
              <Pagination
                onChange={handleChangView}
                style={{float: "right"}}
                defaultCurrent={1}
                total={parseInt(`${totalPageView}0`)}
              />
            </div>
          </div>

          <div className="title-content mt-4 ">
            <h5>Mới nhất</h5>
            <div className="container">
              <Row gutter={8}>
                {products.map((item) => {
                  return (
                    <Skeleton loading={loading} active avatar>
                      <Col xxl={4} xl={4} xs={12} sm={6}>
                        <ProductPage key={item.id} product={item}/>
                      </Col>
                    </Skeleton>
                  );
                })}
              </Row>
            </div>
            {products.length !== totalPage ? (
              <div className="text-center mt-2">
                <Button
                  style={{marginBottom: "10px"}}
                  loading={viewMore}
                  className="view_more"
                  onClick={handleChange}
                >
                  Xem thêm
                </Button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </Col>
    </Row>
  );
};
export default HomePage;
