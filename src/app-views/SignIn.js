import { Form, Button, Input, Typography } from "antd";
import { Link } from "react-router-dom";
const { Title } = Typography;



const SignIn = () => {
    
    return (

        <div className="formsignin">

            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 8,
                }}
               
            >
                <Title style={{ textAlign: 'center',marginTop:"30px" }} level={1} >Đăng nhập</Title>
                <Form.Item
                    label="Họ tên"
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: 'Bạn chưa nhập họ tên',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Mât khẩu"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Bạn chưa nhập mật khẩu',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>




                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 12,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Đăng Nhập
                    </Button>
                    <Link style={{marginLeft:"10px", fontSize:"15px"}} to={"/signup"}>Tạo tài khoản</Link>
                </Form.Item>
            </Form>

        </div>
    )
}
export default SignIn