import {Card, Col,Rate, Row} from "antd";
import Item from "../products/item";
import {Link} from "react-router-dom";

const {Meta} = Card
const SliderSale = ({product}) => {

  return (
    <Item  style={{width: "200px"}}>
   <Link to={`/product/${product.id}`}>
     <Card
       className="card-box"
       hoverable
       style={{width: 240, maxHeight: "100%", position: "relative"}}
     >
       <div className="box-img d-flex justify-content-center" style={{height: "150px"}}>
         <img style={{maxHeight: "100%"}} src={product.img}/>
       </div>

       <div>
           <Meta style={{padding:"10px"}}
             title={product.name}
             description={
               <div>
                 <div className="d-flex">
                 <p style={{textDecorationLine:"line-through"}}>{`${Intl.NumberFormat().format(product.price)} đ`}</p>
                 <p style={{ color:"rgb(255, 66, 78)",marginLeft:"15px"}}>{`${Intl.NumberFormat().format(product.priceSale)} đ`}</p>
                   </div>

                 <div >
                   <p style={{ color:"rgb(143 139 139)",fontSize:"12px",marginTop:"4px"}}>{product.shop.name}</p>
                   <Rate style={{fontSize:"10px"}} disabled defaultValue={5} />
                 </div>
               </div>
             }
           />
       </div>
     </Card>
   </Link>
    </Item>
  )
}

export default SliderSale