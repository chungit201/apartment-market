import {Avatar, Button, Card, Col, Form, Input, Modal, Row} from "antd";
import {CommentOutlined, LikeOutlined, UserOutlined} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {draftToMarkdown} from "markdown-draft-js";
import ReactMarkdown from "react-markdown";
import {addPosts, getPosts} from "../../service/ApiService/postApi";
import WeatherWidget from "../../components/common-component/Weather-withget";


const PostPage = () => {
  const [visible, setVisible] = useState(false);
  const user = useSelector(state => state.user);
  const [editorState, setEditorState] = useState(undefined);
  const [posts,setPosts] = useState([]);

  useEffect(()=>{
    getDataPosts()
  },[])

  const getDataPosts = async () => {
    const {data} = await getPosts();
    setPosts(data.results)
  }

  const formatStatus = (value) => {
    const content = value.getCurrentContent();
    const rawObject = convertToRaw(content);
    return draftToMarkdown(rawObject);
  }

  const handleOk = async () => {
    const {data} = await addPosts({
      user: user.id,
      content: formatStatus(editorState)
    })
    setPosts(state=> [data.post,...state]);
    setVisible(false)
  }
  const handleEditorChange = (editorState) => {
    setEditorState(editorState)
  }
  return (
    <div className="container">
      <Modal
        title="Add status"
        visible={visible}
        width="70vw"
        onCancel={() => {
          setVisible(false)
        }}
        onOk={handleOk}
      >
        <Editor
          editorState={editorState}
          onEditorStateChange={handleEditorChange}
          wrapperClassName="wrapper-class"
          editorClassName="editor-class"
          toolbarClassName="toolbar-class"
        />
      </Modal>
      <Row gutter={16}>
        <Col span={18}>
          <Card>
            <div className="d-flex">
              <div>
                <Avatar src={user.avatar}
                        icon={<UserOutlined/>} className="mr-3"/>
              </div>
              <Input placeholder="Update your status..." onClick={() => {
                setVisible(true)
              }}/>
            </div>
          </Card>

          <div className="post-item">
            {posts && posts.map(item=>{
              return (
                <Card className="mt-2">
                  <div className="d-flex">
                    <Avatar size={40} src={item.user.avatar}/>
                    <div style={{marginLeft: "10px"}}>
                      <div>{item.user.fullName}</div>
                      <span>1h trước</span>
                    </div>
                  </div>
                  <div className="content mt-2">
                    <ReactMarkdown children={item.content}/>
                  </div>
                  <div style={{borderTop: "1px solid rgb(233 233 233)" ,borderBottom: "1px solid rgb(233 233 233)"}}>
                    <Row className="mt-1">
                      <Col span={12}>
                        <Button style={{width: "100%",border:0}}>
                          <div className="d-flex justify-content-center">
                            <LikeOutlined/>
                            <span>0</span>
                          </div>
                        </Button>

                      </Col>
                      <Col span={12}>
                        <Button style={{width: "100%",border:0}}>
                          <div className="d-flex justify-content-center">
                            <CommentOutlined />
                            <span>0</span>
                          </div>
                        </Button>
                      </Col>
                    </Row>
                  </div>
                  <Row className="mt-2">
                    <Col span={24}>
                      <Input placeholder="Nhập bình luận ..."/>
                    </Col>
                  </Row>
                  <div className="list-comment">

                  </div>
                </Card>
              )
            })}
          </div>
        </Col>

        <Col span={6}>
          <WeatherWidget/>
        </Col>
      </Row>
    </div>
  )
}

export default PostPage