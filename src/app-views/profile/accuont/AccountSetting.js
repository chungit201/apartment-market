import {Card, Col, Form, Row, Button, Avatar, notification} from "antd";
import {Tabs} from 'antd';
import AccountInfo from "./Account-info";
import Security from "./Security";
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import moment from 'moment'
import {CameraOutlined} from "@ant-design/icons";
import {storage} from "../../../firebase"
import {updateUser} from "../../../service/ApiService/userApi";
const {TabPane} = Tabs;

function callback(key) {
}

const handleSubmit = (values) => {
  moment(values.bi).format('YYYY MM DD')
}

const AccountSetting = () => {
  const [currentUser, setCurrenUser] = useState();
  const [selectedFile, setSelectedFile] = useState()
  const user = useSelector(state => state.user);

  useEffect(() => {
    setCurrenUser(user);
  }, [user])
  const [form] = Form.useForm();
  form.setFieldsValue(user);
  const handleSubmit = (values) => {
    const ref = storage.ref(`images/${selectedFile.name}`);
    const upload = ref.put(selectedFile);
    upload.on(
      "state_changed",
      snapshot => {
      },
      error => {
      }, () => {
        storage.ref('images')
          .child(selectedFile.name)
          .getDownloadURL()
          .then(async (url) => {
            const {data} = await updateUser(user.id, {
              ...values,
              avatar: url
            });
            if (data) {
              notification.success({message: data.message});
            }
          })
      }
    )
  }
  return (
    <>
      <div className="container account-header mt-4">
        <Form
          initialValues={{remember: true}}
          form={form}
          layout="vertical"
          onFinish={handleSubmit}
          requiredMark={false}>
          <Card>
            <Row>
              <Col span={20}>
                <h4>Thông tin tài khoản</h4>
              </Col>
              <Col span={4} className="d-flex justify-content-center">
                <Form.Item>
                  <Button htmlType="submit" type="primary">Save</Button>
                </Form.Item>
              </Col>
            </Row>
            <Tabs defaultActiveKey="1" onChange={callback}>
              <TabPane tab="Thông tin" key="1">
                <div className="d-flex" style={{position:"relative"}}>
                  <Avatar size={80} src={user.avatar}/>
                </div>
                <Row className="mt-4" gutter={16}>
                  <AccountInfo selectedFile={selectedFile} setSelectedFile={setSelectedFile}/>
                </Row>
              </TabPane>
              <TabPane tab="Đổi mật khẩu" key="2">
                <Row gutter={16}>
                  <Security/>
                </Row>
              </TabPane>
            </Tabs>
          </Card>
        </Form>
      </div>
    </>
  )
}
export default AccountSetting