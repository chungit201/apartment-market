import {Card, Col, Form, Input} from "antd";

const Security = () => {
  return (
    <Col span={24}>
       <div className="chang-pass m-auto" style={{maxWidth:"500px"}}>
         <Form.Item
           label="Mật khẩu cũ"
           name="oldPassword"
           rules={[{ required: true, message: 'Vui lòng nhập lại mật khẩu!' }]}
         >
           <Input.Password placeholder="Nhập lại mật khẩu ..."/>
         </Form.Item>
         <Form.Item
           label="Mật khẩu mới"
           name="password"
         >
           <Input.Password placeholder="Nhập mật khẩu ..."/>
         </Form.Item>
         <Form.Item
           label="Nhập lại mật khẩu"
           name="confirmPassword"
         >
           <Input.Password placeholder="Xác nhận mật khẩu ..." />
         </Form.Item>
       </div>
    </Col>
  )
}
export default Security