import {Form, Input, Col, DatePicker} from "antd";
import React, {useState} from "react";

const AccountInfo = ({selectedFile, setSelectedFile}) => {
  const handChangeImg = (e) => {
    setSelectedFile(e.target.files[0])
  }
  return (
    <>
      <Col span={12}>
          <Form.Item
            label="Họ và tên"
            name="fullName"
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Tài khoản"
            name="username"
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Số điện thoại"
            name="phone"
          >
            <Input />
          </Form.Item>
      </Col>

      <Col span={12}>
          <Form.Item
            label="Ngày sinh"
            name="birthday"
          >
              <input type="date" defaultValue="2001-09-30"/>
          </Form.Item>
        <Form.Item label="Avatar">
          <input type="file" onChange={handChangeImg}/>
        </Form.Item>
          <Form.Item
            label="Địa chỉ"
            name="address"
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="CCCD"
            name="cardId"
          >
            <Input />
          </Form.Item>
      </Col>
    </>
  )
}
export default AccountInfo