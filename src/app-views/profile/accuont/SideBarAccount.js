import React, {useState} from 'react';
import {Row, Col, Menu, Avatar} from 'antd';
import {InboxOutlined,ContainerOutlined,BellOutlined} from "@ant-design/icons";
import {Link, Outlet} from "react-router-dom";
import {useSelector} from "react-redux";

const { SubMenu } = Menu;

const SideBarAccount = () => {
    const user = useSelector(state => state.user)
    return (
        <>
            <div>
                        <Menu mode="inline">
                            <div style={{height: "50px"}}>

                                    <div style={{ marginTop: "10px"}} className="nav-profile-header">
                                        <div className="d-flex">
                                            <Avatar size={40}
                                                    src={user.avatar}/>
                                            <div className="sub-nav-user" style={{paddingLeft: "1rem"}}>
                                                <h6 style={{marginBottom: "15px"}}>Chung vương</h6>
                                                <a href="account-setting"><span>Chỉnh sửa hồ sơ</span></a>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                            <SubMenu style={{marginTop: "30px"}} key="sub1" icon={<ContainerOutlined />} title="Đơn Mua">
                                <Menu.Item key="1"><Link to="/purchase">Xem Đơn</Link></Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub2" icon={<BellOutlined />} title="Thông Báo">
                                <Menu.Item key="2"><Link to="/notification-order">Cập Nhật Đơn Hàng</Link></Menu.Item>
                                <Menu.Item key="3"><Link to="/notification-promotion">Khuyến Mãi</Link></Menu.Item>
                                <Menu.Item key="4"><Link to="/notification-wallet">Cập Nhật Ví</Link></Menu.Item>
                                <Menu.Item key="5"><Link to="/notification-activity">Hoạt Động</Link></Menu.Item>
                                <Menu.Item key="6"><Link to="/notification-rating">Cập Nhật Đánh Giá</Link></Menu.Item>
                            </SubMenu>

                            <SubMenu key="sub3" icon={<InboxOutlined/>} title="Kho Voucher">
                                <Menu.Item key="7"><Link to="/voucher-wallet">Xem Kho</Link></Menu.Item>
                            </SubMenu>

                        </Menu>
            </div>
        </>
    )
}

export default SideBarAccount;