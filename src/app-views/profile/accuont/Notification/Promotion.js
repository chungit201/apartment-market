import React from 'react';
import {
    Menu,
    Dropdown,
    Row,
    Col,
    Tabs,
    DatePicker,
    Space,
    Input,
    Button,
    Table, Empty, Card
} from "antd";
import SideBarAccount from "../SideBarAccount";

const NotificationPromotion = () => {
    return (
        <>
            <div>
                <Row>
                    <Col span={5}>
                        <SideBarAccount />
                    </Col>

                    <Col span={18} style={{ marginLeft: "30px", backgroundColor: "#fff"}}>
                        <div style={{ marginLeft: "30px"}}>
                            <div style={{ marginTop: "20px"}}>
                                <span>Đánh dấu Đã đọc tất cả</span>
                            </div>
                            <hr />
                            <div>
                                <Card>
                                    <Row gutter={16}>
                                        <div className="container">
                                            <Col style={{ padding: "20px"}}>
                                                <Row>
                                                    <Col span={3}>
                                                        <img style={{maxWidth: "100%", width: "100px"}} src={"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFhYYGRgaHB8cHBoaHBojGhocGhwcGRwaGh0dIS4lHB4rIRwaJzgmKy8xNTU1HCQ7QDs0Py40NTEBDAwMEA8QHhISHjQhISs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOAA4QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAIFBgEAB//EAD4QAAIBAgQDBQYFAwMDBQEAAAECEQAhAwQSMUFRYQUicYGRBjKhscHwE0JS0eEUI2KCkvEWM3IVQ1Oy0gf/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAhEQEBAQEAAgMBAAMBAAAAAAAAAQIRITEDEhNRQWFxIv/aAAwDAQACEQMRAD8AyyrRUFQQUdRXpVjUkFHQUNBR0FTSEVaMgqCCjotZ1NqaCmEWhotMYa1NpJoKKBUAKMq1AdUUVRUVFFUVNpwYbVMVEC1TipbSgkUFlplxQnFELRLEWhmmcVONLkVcrOlnFL4iU660BxVymr3SgOlOutLutaSgmy0F1pt1oLLWkBRlobLTLLQWFUcpdhUGFHYUMihQemvVLTXqfAeRaMi1BFphFqai1JFo6CoItHRai1NqSLTCLUEWmEFRUuqtNItDw1plbCs9UPBKIq15BRYqLQ8q1NRXkWiBanpxMC1eG1SAtXhtSaSouKC4piLUFhRDtAcWpZlp0rQHSriKVegutMutCYVcT0k60B0p11pd1rSUyTpQHSnHWguKuUdJutCdabZKC61Up9KMtDYU060FlqjlAivUWK5QfT6rR0SootMItRUWuotMItRw0phFrO1NrqpR0SuIlRx8cLvyn0iotKeTBcLv98Kqc/2xpDA2ERM8SxB+F56GkT2ws+/IiOs3A8tj4isrnszqfc6bjy2PqKm+HTj4u3y0v/UzKxvuCByAtv609g+1KkXmQwt4G4rByC1vLpJBv8qnl2kXi3Hwkn6Dyoa34s19U7P7YRhBMXA/+s/WrLL5gNEcQDH30r5ThY5U6VN51DxABB38fWtH7PdqNrkm0gCTtPd28B86m5jPXwyTsbxdvKvAWpXs/Nq43Gw9BaflR2zSCe8PsTUcrLz0UChMKUx+0kCtB2kecE257Ug3b41gQNJ0x/qMD405mqmNWLcihFaWyfaSuxXlM+R0/OfSm/v5/tR6Z6lhbEWgutNuKXZauVJdxS7rTTChOtXC6RdaC60460Flq5T6SdaEy006UF1rSVUpR1oTCm3FAZaqHKFFeqWmvUzWSLR0SuItMIlY2s7XUSmUSo4aUwq1FqQcfGCDr8Nib+lZDtztGX7shTEnlIgjlz9Ks+2M00Mu0cd/dIU+Np9RzrL44iRymwuLkMPr6Cprq+LEnmgyxYye8NUeO9/jUMwNQ1WkySOJ2M+h+4ruE/eEza1pBiG1ecH4V5jqKgwJPMiNe+/AGT51LoBJiLX0mduDfwamNzyv1ieH3yryGNBIkBoM7jTDEREbEcD+/FEIotM8bRsokTNAiaNcSYtbfrA86dTOFR3bcTHGLcvGq9xIEDaeN+AH1qYB0/DwEn4kz6U1LPB7VeCuphPLlIsKNi9sPBub6efIgifEmqgsCTExNvCuKZO/ECiVX1ytn7WJGljYkeAi3nvUFz2zEXXba8SfmRVa5kjjfoeRM89/uK5r25CJ+E/Kn0SRfdmdoFWUTx3nxaT6k+cVq8HthSFE7iOsX3r5ouJf72prJ50qwPDbw6/AelK8Zb+OafWkuoPMTQnSqfs/t5XTUTfl5fOxq4TGUgRx28LkfKp5Y4NYsvkJ0oLrTjLwobpVSoIOtBdaedKXdKuUyTrQHSnXSgulXKcpFloLrTrpQHSrlVKWivUXTXarp9WaJTKJUUSmUSue1FdRKJinSpNTRaFnkOk8vrNp6HaotE9sRjYjazP6tJWf1ao+I36CkM7lyLjvrMavzDaQR5wOhir/ADWSMEidLCxi4IPH/JCPMTypHE0hw5F7K6NcHiwUmJBgnmII6UWu3OldhZY4mHqE68MgEGNRUEe7bcAqbGwnnSWeTSQDNoIN7rwBnkPh41dvlQrqFYLJ0qT7stIVMS51DvFdQ4zwigZlVdCpBVysFGB7rqCYB2N1JDcmI4Uj6RxcOQrfrcK8bhiQwdRuJViv+k0LMJOqJIDkCdzq1MvXZR8edoNjEKd5BQj/ABjVMjgRqA864MS4mfeLE/KJtMT8KFQMyTpMDn8PhwppxpAUmIJHHcX8t/u9SyWXJfUxBhAxEzIJmPSpY4LsWERO++/5tojrxg0z6AqkQSN+f8eIr2Gu3r0P3emcTDnD1L+UDcibNo0jnHctwHjS2WBkCN9p2klQL7b0lSpISAOmrccGhSelyKXFzY8Nj9KaGJOkad4HHbWCVg7bj1HKgupJck3Bkm25KiARub/A0xKEV48x9T+xqPPhejBe7JEAnfxO3lB9agqyOQFh1J8egPpSBvLY7TEwBf8A2gmtl2Zn5CKbX09baiPOCfOsXlSADwkjzWdjfa9X3YrsXI3abX4mZHlNz402fyZlje5c6pJ5D4TPxqbpSGTY64J2gW2GwA8bH4VastRXBucpN0pd0p50oLpVSpIOlLulWDpQHStJTV7pQXSnXSgulXKcpXTXqNpr1V0dWWWwiAAzajxMR8OFOIlK5LGD+MA/xS3beaKAFHjcEAj/AHR0PzrnvbeDlt4ukSjKlV/Y+a1DQblR70b/AD/mrhErPV5Ss4qs32QrSQJndeBtG9o8fhWdzmSdEYMhdVjTeWEbI8TO1njaZNprdfhTxI9PqDFRORUkNLahs2ogxytaOhBFL7NM7sfKXdHDSwDgadDm7BoVeAvwsbEXGm9TZGCF175SEdWBmfeERIkggBrzAkGdVbftvsfUjgqjNBIlYtc8Lq3MgEGZI5YDDzD4eJF4buEgAsQx7pKizgECI5WPKpeunOvtPCszOA0wpBBbUscUdtIImJBZtjfjAqOEA7EIhIWW6xILSOUAD0513tvB0vIKmTNjOk+pMG5HnxBAd7Pyh1uoUgsyIS35Z0kloEyxMgLyPIU5V98I4QZVdhAGI4RdzGkGSJuQNieY9D5ZACHmFXuqYvbdX5HhFvekTFN5TKk4gw4Y6ndSAp1IiuwZjvoLAaLCwk3Iq2xuzkxl/swfw/cKBYxNLAatOxQHUINyZNpknSumYbDIbUo0rfux+qQR4EBrG/dPKgZZBD6e8VZSFIuVKvy66ZvaausbD0gqbONhJKM0wwViO6WKyLEalAImKoHxDrYsiqWGoiCBMajbgDy6iKapewXGy4UwDdSokk8WN+RPj+nypdSSSSRpkkmR1A+AgD96f7VwCisxMtCEMCL69OISeJ95QOnlSuEhXUimQPfJAKyp7yz+k2vIkxHOjp9RV9r8SQP07CTNjP0vUCCQOA879fSKHhsLXjqZvE2EXvNWOSy0sHM6RBXcFgWibSbwQAJuQONB2uDAgrMJab3IA4yeM3jbxrTdi4aqRoB0CNWIw4iCLcN7eNViPH9xlAtFoBP+M76dhAv4TFXmUzWGV70DjGq51MZJnbc+M8JpM93wvOysHUwYg6blZO95N+QsPKeNXbpQMs6uo0kbDaLA843PTpTwW16ztcW/NJulCZKdZKCyU5pJB8Ol3SrB0oDpVzRK90oDpVg6UF0rSaMjpr1Nfh16q6Gdwu1HRhtDGxN7CPpw51DtZ8YhToCy6+6O7qtBibnz4dKn2j2Zj6gRl8fWIt+FiaSFgQCB3ZFrzanc12biDAww2Fi3ddQCPqESbkC3X+IOX2jq5JymOzsVkgLqSw1nTLMYuTqMDbeK2WELCsh2VlsQOFXDfDU2NpJk8Tf0tvWzyuUKKFAYgCLg1n8ljLWfKSrRVFDxHZfyObcAayvbXauZDFEwcU3DKVw2KxaA5I+V7Cssy6LObWlxmR5TUZAkEfSsZ2/2C3fZCO8LaZAYyTDARG9iDE8bQFsx2nmFZnODmJDLoAR/ygmSYPEn72uOy+1nx10Oj4bi6yrqxIuJECbbxy8q05Y0znWfL532y04WHiEDUW/uA2ZmAEPEe60NcWkcCCTe5DH/ALWJmWUIS+LiC8yVYgxMd0AogMbg+SntzkYP4irpUkBlA91o7wYAQvAiLHWSLNS2VzgfDy+FEJgocR9yGGvWRH/lvx97mIpv7ix9mUxC+KG1DEdteMyyGVHVHsfyk6n22vwmNLmMAYKgIkYhEqRIAX3Pw3vdQukT+oCOApH2PxNSFiss7tiMSpgIbqrC4JOld5JAUAXBNp2jivBUo0uTDQDpgsNKRJgTc8SaVvlnrtrPZrI/1BhP+6oUODGhASR8QvOCJO22a7XyD4Lo7iUELMzI3heBUrI6TfgTp3zT5Yo0EgQwaDBtcMRI0tYyJgTY7GPafaqZnBKfhQCCwbT7gNkYnTCiZ72xuOlOVctn/GLzeeYyOcaiOQaVCn8vAnr4V0MSkBbTLtLW1MQCw2sOG1ifBINEiLmB4cx6x6UwczqAQmFG+/hLDgf3PhVNDWDgaz3LJEX4wO+1zvYmOUcTRpVjJJCD3BaWJO6gWVduVJltYlj3RYICRA324CndAGk6kJdZ96dIPCwlSPlx4hgTDJUhgAWkabkKJMQAOtr3N6vMkNbYcme8DFoCgEAnwPuzbeq/L4OHIR3ALOsBWAVhPE7mxkAfGtDh4JfQBp0ks0qSYKoixJkQCx4bi5FhStZ6rV9ilVOnjHengbWFoPHY+Zq4ZaoOxssVcQQRaTqYnnbgeEmr/O4yohdpgchNYavnw5dTyQfPprKEww57eRqWZcIpY7Cvnfb3tCV1NhkgsYvpnutY2vtI86e9pfagvl0fCIVyDqWRIlSLb241p9L4P8r4ats0mjWSAsT4cPnSeJ2ikkA2GmTwGsEifSvmvbvbDksgclSEO/LvXv1o+F2kUwXtc6AfATpM8txWkyr8W7ws4GxGQiJEr9Z5bGpZnGVGVSYLmB43/asv2J2gcRtQ98LHExcrP3e9E9r8Vg6G8g2jgYI/mnzzxP5/+uNNA516sp/Vj7YV6n9T/J9vXEqYfrVer0nn+0tG0T1/5rzZ2tF7/UAcage0EH5qxGY7cgw+Ik3hEGt/E7j4Um/bBYHSHMbsxKgDrJK+UA1pMaOPo651P1R4yPnRFzCkwGE+NfL0zbkkvpQc+8Z6wVIAt0611+1J90FzaLIRw2YIAB4mr/Ojr6iWtYiqTtjDDKNQXu3DG4HNTxCm8MDaByM4Q9tOQQpZIidGohSbx+Qeh52pfJduZl2A/EcLsxMNewH5tS773g3MASX+WvZrT2x7MD5XFxkLsn4blu8utdAm8r3tJG8+61rTPzz2ZcLk80ETXj4j4WDhDunvPrBgMbmCxFraZkVZ5z2gx3/qMLDKfhsjq0SAVIKl7kAuQTtAPI8Y+zHY2Xy2OMfMMmOgwi6KFnvtYF1NoWDe4kg8La/TWc+V98PoGQwVy+GmF3TiFAq4SMISF/8AcdB3F/U1tW1+6gtOyctqOo9/VB1vLari6qQAiDSNIk+4CZ3NT2XmEzID61RRqRVw4A0qSJLW0Bve0wDtOxJ1GVGhZVg5Me7JHIAHjH2BtXNq2FTGL2cpWASs7wYnnta/OKzua9l2TU+XxWRirDQ5LYZ1e8Lj3GtKm0gEQZJusXtB1/IP9RQfNq8e05tENx0sjR57A0prULr4P2zhjK9pa8TCxMJNS4hRHBbS47/4bqdi2sA2IFrEVY9n9hY/aDNnAqYeEDpwsPUAdKatKrKsCFg3aJO1T/8A6hjjMZhAhV3RSrFdzfUqNwLAl9p97htWh9lvaDEXL4eWfDxFdFGGXX8JVw0kr+Zh3yAtzsTJFu90X7fWXiu+Bst7NFmZEwctpR41ku2tgsMhk2RO9qCEajAMDVOu7M9ncum+HhM+5JVWGriQoVRO9zJ8qUyL61XDJVUk6SHLkAnUDOm7kEC5MX33rSZV0UaUBt0Mn0FY61StB/8AQssYLZfCYjYth4Zjw7sDyqeF2PllMrl8FTe64aA3ibgcYHoKdUjr5g/WvLHAjyip7SAGQwgIGGgHRVHyFexcnhsIZFI5EWpiOtcNLqeRSZn2TyLmXyuCx5lL7k7+JPrSuJ7EdnnfK4fL84t5NWhY1Bmp/fX9HWZxfYTs875VP92J/wDuh4vsJkCNJy8CIgPiC3k9aVmobPS++v6O1ncn7F5PBbVh4TKSI/7mIRz2Zj61DM+xmUeCyOYgj+4+4473PWtEWobvRN6/pdves7/0Tkv/AI2/3v8AvXq0GuuU/wBNf0doT4kC1ZDt0uHMmXYQqiC2/AQYA4/MVrHQkRMeVVOdyYQFlXvH3nO8cgd6PjslKMf/AEjAxYNuwBv4u8gAdPSaHiZhV4hiNpeFHgqXY13OO5fRENq93gORY3jnVdj5XDVwuvWxPe0LvzAJkx5V3ZnfazidoHROhSSRYgwZuIX856tHnTyHEZJZvwtgFszdNIUf218CJvaq/MOiSAzlvJQo3iZknqb0PMdsPpGjcmzCZsIsp2/8jc0/r31ErTMKhRjB0rJZ8Qhi210UjQoNxzPWqbP9pa0KpGHhEMEUEguf1PFgovYADhSju2I6q76mEkkgkeBGxi5nrTGYRFVSW14mrvAQAEWLSLiZG3EdKvOZPflRTJohKq4N2H4nBQsADuiO8DNyb35mrbLYRxMNCqlXTVedKl41KT+ksuk6tvWuZDE/DLYgh/1T7sBfzDjGrzjzqHs9iaiyMSbBkg2UradIN4BGx4U9dvb/AAurTIYX4Ko2Ex0uqtJs6EiWMcQDuLzPSC5lu0i92YhhP/bsXM2bSI1D1NxI4lX2c7U0qVktpZgCw90arHVFwZIPRj5Web7HCd9IKOSwFu6YvBPvcfCDY3rn1yXmvY6Fl+0bFsRwVEwRJZveEXMLYb2F6pe08VXwHdQVBaBLSxNjqgCQb7ki1oNQzuCRqV7GAbAwWm8jgRfy+CWawdKNsYt3Yi1gTe9aZxnvR1HK5RSSIA1qFFlCxYAwbEtHGDN5k11cMq47qu0ENpMsRNiARGoQR3ee3EybFVl0sigQsMBsLib7zOx6UTLYh1FmYBhAlhsVPdkgSAR3Z8JrSwdcy+ZZY0YrqbiRMXOzLYyOp8qaHtPmUIActETqAM9dUSRbjHhQWwydTCA4JkNBmeFvmPKlMJw5hFhh+USY6g7kUvpm+4OtXlva3GI1ta8Thxy47kc4NaTs/wBpiwuQxtuCLcJN4PiBXzFgynaCbhgSCRzDCJ8CJouHn3Au2o7TsfWO75RtWWvgzfRPs2D2shFzp8SP3p1cYESDNfIsv2y6mx1cLn5yAOfLxq4yntAqkLpZGsbTcH/BjHoa5t/Bqeg+hlqgWqj7M7dTEJUMDHLceINxVp+Mp4jw4+lYXNnsCM1DZqiz0NnpB1nqDYlQZ6gzUEnrr1B1V6gOO5AsJNIZ4mCXcKOgPoDFNYmKQLC9UmcdwTuznjwUcgK0xnoUmezWGoKokmZ1NxPCBxNK5bsB+9iYjFRvptMb3I+VWeBklRvxMQ6iNhf4/wAUbPFnhnGkEd1RYAcPM11TXPEV1mc0oIMQiHgd2Hhw241XHELd/QsLwjfkWjh41dZrDkkk9079f8R0qixsYliJgCwUbcr104vYY+VzbKr6iV/EBDMoEwOHCxNHxcn/AGsI6pDGCf0kzCxH3NBwcLUBJBJYyByHHwrmNjsE0AwAb+siPKq/z4BzBVWQq121SQNu6Y90fWlMnjMhVlEFXgCJN/eB4RFHwV0qziL2HE8/Oj5QAIVZbMwIfjEmR1vS9dSkmAmt1RgqEKu/uuQSJU7T7tqvuy8+yK2C9woGtSTKONnwz1sSp61kez2LM6EyWHGb6TvPOBT2vUsXZgdJM7ruJ6gxUbx32Gpz2WSC7AnDa7TvhwdJK8YFjHARG1VGf7J0MEb3X2foIg2sfS/wq77PzmvCZX95U8Cbab8Jj1qaYismhoaBIU8bQSs+scawmtZDJJkmCsjK0g72jSZkQfUV7BUh9JPvKUM/mtAk8iNq1OEm5WARuOBg+6R48Ott7e7QyqkK6qAVbvLe6nb0rT9vPKOs/hYSpCsVEwqsd1PAN0O08CKqs9lmRpWZW4PITfy1H41ps/l1hk06gwJAjieHQ2+AqgGKQuiTKbTuw4irxrvkEUzBJ7/y+R51LEwzuDI5mxBPOrEZYEQGtiXWY06ryp5Hkf3pDBxCp0EXFoYG/Q9a0mu+ghg5llvExz4eB3Hy6U/g5hWUAWM2sInpy8BY8hQ8zlkZbHSRcg7jrP5l6/8ANV6M6NG8eYI5jmKXJo4v/wCqJjVpkbahM9Vcd9T5kfGL/s7tFzAnVG4Y3HgwBPwNZbJY9rEX4H6z8DVpllLyFBVxdgPmB61z7xCbnJZrWIMg8jv8N6YZqyGBmHFrkjfmPI7H7Bq/yma1CCfvzrj1jgptn60FnrztS7tSkIXXXqW19K9VcDwxQRxqDYAIk7+NEbFHCuNizaKAqFwJcT7q3jgT9aHncNXcydt+Q6U9mnmwXaqLtFiqFVsSZY9OVbY7aqKzOYoOrT7osCDxnhzpFsr+a4UfE/Wi4Cl3ge6t/wCabxMQadO4G3UzXTL9fEMt2fli7EBoHz51HGBXU0AgGL8SafwcQaSwWDBoKYQaJ56qf28grhSqgEWXfxiu4WbGi/A79OQpzDwDLJF2pbP5XQgWIk251U1LUk8s+kh14zbz/amFTQwkWNm+h+NRy+XlSRuF9DTOChIuQbXnnzp6oWWWd1cEGFYTHT3TFWmQRV179xu6enFfifU1UIZRY3BMee4rQ5BFZAf1AT4raa5vkvIEMXuIrxYEg2uVJ2PhVtlgrCCBcAeI4Uvk0uyMJHD0i8UxlhBiDbbw5b1hqkBnuzZ0kcLeI69az/bfZd5UQdx5XrbowIuD6fxSefyykTFxRj5Lmm+cshdWEQQdQjgd5FRzYGMoY2xVsx59a0mY7Mhy4sDuP2++NU2ZyDKxYbTfpP0rrzuX0FXgY5kA7i09eRoj5U8BBHDgeYqObwCrawN7EcJ5GnstjSt5JHHiQOI6jbqK0t/zARy0q30N/IzuDV7kMQahM290/nTwP5l+IpbHyYYa1M/X0o2DlzYTB4Gst6mopqEwldQT7w4j726VLDRl60hkcyVs1uf7+FPNjTXLZYkdscRxpZ8ytCxH5enCq7HB3B8qM5C0/GXrXqpNR5V6q+o4uFxelEGNVWuJ1oq455+tTcg1j4kC25qm7Swp7oO9zVlrJuTQWInUYJPTaqz4OKhsvoUnnSWJhztxq3zRDztbpXMPJ7GBWs1zzR0s2VhY6VFcKWjkIqxx8GDQcvh3Ymia8GX7IScaCa924NWNp/TXcg8Ys/5U5mcJWx2HMU7ea7/pKmwsMguBymKPlcqYn6jankyg1nwip4iR3eVVdgtk3s4i/Dxq57NeBEW3/eksFO8I2YfEU3kyZO1j8ONZ7vYKvMu3e8qm6GZ+opbDJEX6fZpj/Sa5aQmGbfe3kakX60HWeRqJY8aXAjiYYttSOLlRJEWYQafLDlQsYA/81WbYGezHZwgg+B8tjVY+RZGEbfJv5rU4qzw3+dL4mECLj74Gt8/JTityGGLzYHcQbGnFVYg+v1FCGX4i3PceFGA6/tRb0O6+pkVJcaLff8UMsR4eJoTEcPhep4BnxjQGcUJsQj7ihNi1UyDWo9K5SsjnXqfA6uN41NczSKsOE0dDRxRxMQnl8qk8xwnxE0ov+n1NESCbx8aOJEwcI6b08i292lVnh8KKrtU3yHcVSTeuJhwSOdTTHYfYojY560u0K3AybSSAbHiKYwsqxxS5jaKMgtUsBiCTJp3VCWFgd+SfiKDiYa6z1ptMZpP1qBJJFhUy3oKol1gcaYymHBbbfrxvXdPe2J8B+1dwXIJ7rb8j+1FvgLFZgbVKDwMeApbDxGjYjzX96nq5k+cfQ1HCHBPFj9+Vd+94oIjgT5fzXjHI+v8ANLhit5+tDIFRJPM+lRJ6z4gUyeYUJxxHyqZj/H0P7VFgPuaZl3U8aA6cvnTZHJqG+H5+Y+tVKCLseX7etBbw+lPOqjgfUfzS7MOE+FXKCj+XwoTGmHE9aC+H9/8ANVFBautdrn4Z+wK7TBRXH3P0ov8AUeB9R9aQRef35UdU8fKq5AbGP/iPWuow4KR4TS6edGSf1H78qmgymK3AsPE0fDzD/q+E/SlUwz9/zRljiSPOpvAbTNPymiDNTvI8vrSir1mjordT8amyJNIwPGakFj7/AJpYLO4NEQkbW9f2qeAYkDn9+NdRh+r5n5WriYvP5fvUiwP5J6mPmaQTDj9RPl9xXkxbkT6yfpXEwydgPgflXVTvHun0gfOgCBz0+/GpBqjYcI9f2ruoUglqPIV7URtUQ1eJpE9rbxrxfnXNccJrxaafAgzA7TUDHM+n7UQsOtQkc5pwwyg/UPjPpFBbC/yo5T7tQ5I2n78qcAJwj19R8qgVOwM+P/NFZuYNQLdKqAuy1HTTNcC9afT6Bp6Vyj6RzFeo6Ov/2Q=="}/>
                                                    </Col>
                                                    <Col span={18}>
                                                        <div style={{margin: "15px"}}>
                                                            <h5>Thử thách dễ nhưng thưởng siêu hời</h5>
                                                            <h4 style={{color: "rgb(253, 48, 130)"}}></h4>
                                                            <p>Tham gia trò chơi để nhận thưởng siêu hấp dẫn. Còn chờ gì nữa chơi mau thôi.</p>
                                                        </div>
                                                    </Col>
                                                    <Col span={3}>
                                                        <div style={{margin: "20px 0"}}>
                                                            <Button type="primary" danger>
                                                                Xem chi tiết
                                                            </Button>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </div>
                                    </Row>
                                </Card>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

        </>
    )
}

export default NotificationPromotion;