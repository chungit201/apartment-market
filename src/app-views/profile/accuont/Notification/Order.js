import React from 'react';
import {
    Menu,
    Dropdown,
    Row,
    Col,
    Tabs,
    DatePicker,
    Space,
    Input,
    Button,
    Table, Empty, InputNumber, Rate, Card
} from "antd";
import SideBarAccount from "../SideBarAccount";


const NotificationOrder = () => {
    return (
        <>
            <div>
                <Row>
                    <Col span={5}>
                        <SideBarAccount/>
                    </Col>

                    <Col span={18} style={{ marginLeft: "30px", backgroundColor: "#fff"}}>
                        <div style={{ marginLeft: "30px"}}>
                            <div style={{ marginTop: "20px"}}>
                                <span>Đánh dấu Đã đọc tất cả</span>
                            </div>
                            <hr />
                            <div>
                                <Card>
                                    <Row gutter={16}>
                                        <div className="container">
                                            <Col style={{ padding: "20px"}}>
                                                <Row>
                                                    <Col span={3}>
                                                        <img style={{maxWidth: "100%", width: "100px"}} src={"https://vcdn1-dulich.vnecdn.net/2021/07/16/3-1-1626444927.jpg?w=1200&h=0&q=100&dpr=1&fit=crop&s=0nww5sftrDimoUxyn9lM5g"}/>
                                                    </Col>
                                                    <Col span={18}>
                                                        <div style={{margin: "15px"}}>
                                                            <h5>Chia sẻ nhận xét về sản phẩm</h5>
                                                            <h4 style={{color: "rgb(253, 48, 130)"}}></h4>
                                                            <p>Đơn hàng abhcsdsd đã hoàn thành. Bạn hãy đánh giá sản phẩm và giúp người khác hiểu hơn về sản phẩm nhé</p>
                                                        </div>
                                                    </Col>
                                                    <Col span={3}>
                                                        <div style={{margin: "20px 0"}}>
                                                            <Button type="primary" danger>
                                                                Đánh giá sản phẩm
                                                            </Button>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </div>
                                    </Row>
                                </Card>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

        </>
    )
}

export default NotificationOrder;