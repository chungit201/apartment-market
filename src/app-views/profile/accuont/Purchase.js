import React from "react";
import {
  Row,
  Col,
  Tabs,
  Card,
  Empty,
  Modal,
  Button,
  Form,
  Avatar,
  Comment,
  Rate,
  notification,
} from "antd";
import {EyeOutlined, DollarOutlined, ShopOutlined} from "@ant-design/icons";
import "../../../App.css";
import SideBarAccount from "./SideBarAccount";
import {
  getDataOder,
  getMyOder,
} from "../../../service/ApiService/oderUserApi";
import {Link} from "react-router-dom"
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import TextArea from "antd/es/input/TextArea";
import {addComment} from "../../../service/ApiService/commentApi";
import {addRating} from "../../../service/ApiService/productsApi";

const {TabPane} = Tabs;
const Editor = ({onChange, onSubmit, submitting, value}) => (
  <>
    <Form.Item>
      <TextArea rows={4} onChange={onChange} value={value}/>
    </Form.Item>
  </>
);
const Purchase = () => {
  const [orders, setOrders] = useState([]);
  const [visible, setVisible] = useState(false);
  const user = useSelector((state) => state.user);
  const [detailOrder, setDetailOrder] = useState(null);
  const [value, setValue] = useState("");
  const [submitting, setSubmitting] = useState(false);
  const [comments, setComments] = useState([]);
  const desc = [
    "Sản phẩm quá khác mô tả",
    "Sản phẩm hơi khác mô tả",
    "Sản phẩm gần giống mô tả",
    "Sản phẩm đúng mô tả",
    "Sản phẩm tốt vượt mong đợi",
  ];
  const [valueRate, setValueRate] = useState(0);

  useEffect(() => {
    getOrders().then(() => {
    });
  }, []);

  const getOrders = async () => {
    const {data} = await getDataOder(user.id);
    setOrders(data.results);
  };

  const callback = async (key) => {
    if (key == "all") {
      const {data} = await getDataOder(user.id);
      setOrders(data.results);

    } else {
      const {data} = await getMyOder(user.id, key);
      setOrders(data.results);
    }
  };
  const handleClickDetail = (value) => {
    setDetailOrder(value);
    setVisible(true);
  };
  const handleChange = (e) => {
    setValue(e.target.value);
  };


  const handleSubmit = async (id) => {
    setSubmitting(true);
    const comment = {
      author: user.id,
      product: id,
      content: value,
    };

    const {data} = await addComment(comment).then((res) => {
      if (res) {
        notification.success({message: "Cảm ơn bạn đánh giá sản phẩm"});
      }
    });
    setSubmitting(false);
    setComments((state) => [...state, data.comments]);
  };
  const dataStatus = ['all', "pending", "active", "refuse"]
  return (
    <>
      <Modal
        title="Chi tiết đơn hàng"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={1000}
      >
        <div style={{height: "auto", width: "100%"}}>
          {detailOrder !== null && (
            <div>
              {detailOrder.products.map((item) => {
                return (
                  <Card style={{width: "100%"}}>
                    <div style={{display: "flex"}}>
                      <div style={{width: "50%"}}>
                        <img src={item.product.img} width={100} height={100}/>
                        <p style={{margin: "10px 0"}}>{item.product.name}</p>
                        <p
                          style={{margin: "10px 0", color: "rgb(52 181 20)"}}
                        >
                          {item.product.price}
                        </p>
                      </div>
                      <div style={{width: "100%"}}>
                        <span>
                          Bạn có hài lòng với sản phẩm không:
                          <Rate
                            tooltips={desc}
                            value={valueRate}
                            onChange={async (value) => {
                              setValueRate(value);
                              const res = await addRating({
                                product: item.product.id,
                                rate: value,
                              });
                              if (res.status === 200) {
                                notification.success({
                                  message: "Cảm ơn bạn đã đánh giá chất lượng sản phẩm",
                                });
                              }
                            }}
                          />
                        </span>
                        <p>
                          <b>Nhận xét:</b>(Ý kiến của bạn sẽ thêm thông tin cho
                          nhiều người mua khác)
                        </p>
                        <Comment
                          avatar={<Avatar src={user.avatar} alt="Han Solo"/>}
                          content={
                            <Editor
                              onChange={handleChange}
                              onSubmit={handleSubmit}
                              submitting={submitting}
                              value={value}
                            />
                          }
                        />
                        <Form.Item>
                          <Button
                            htmlType="submit"
                            loading={submitting}
                            onClick={() => handleSubmit(item.product.id)}
                            type="primary"
                          >
                            Đánh giá
                          </Button>
                        </Form.Item>

                      </div>
                    </div>
                  </Card>
                );
              })}
            </div>
          )}
        </div>
      </Modal>
      <div>
        <Row>
          <Col span={4} style={{marginRight: "30px"}}>
            <SideBarAccount/>
          </Col>
          <Col span={19}>
            <Tabs defaultActiveKey="1" onChange={callback}>
              {dataStatus.map(item => {
                return (
                  <TabPane
                    tab={item === "all" ? "Tất cả" : item === "pending" ? "Chờ giao hàng" : item === "active" ? "Đã giao" : "Từ chối"}
                    key={item}>
                    {orders.length > 0 ? (
                      orders.map((item) => {
                        console.log("dssssssssssss0", item)
                        return (
                          <div style={{border: "1px solid #F0F2F5", marginBottom: "30px"}}>
                            <div>
                              <div style={{padding: "12px 24px"}}>
                                <div className="d-flex" style={{alignItems: "center", justifyContent: "space-between"}}>
                                  <div>
                                    <span style={{marginRight: "10px"}}>Tên Shop: {item.shop.name}</span>
                                    <Button icon={<EyeOutlined/>}></Button>
                                  </div>
                                  <div style={{color: "#41BA61"}}>
                                <span>{item.status === "pending" ? <div>Chờ giao hàng</div> : item.status === "active" ?
                                  <div style={{color: "#00ee6f"}}>Đã giao</div> :
                                  <div style={{color: "#fd0404"}}>Từ chối</div>}</span>
                                  </div>
                                </div>
                                <Link to={""}>
                                  {item.products.map((value) => {
                                    return (
                                      <div>
                                        <div className="pt-3">
                                          <div style={{borderBottom: "1px solid #F0F2F5"}}></div>
                                          <div className="d-flex" style={{
                                            alignItems: "center",
                                            color: "black",
                                            display: "block",
                                            justifyContent: "space-between"
                                          }}>
                                            <div className="d-flex"
                                                 style={{alignItems: "flex-start", padding: "20px 50px 0 0"}}>
                                              <div>
                                                <img style={{width: "50px", marginRight: "20px"}}
                                                     src={value.product?.img}/>
                                              </div>
                                              <div>
                                                <div style={{fontSize: "13px"}}>{value.product?.name}
                                                </div>
                                                <div style={{fontSize: "13px", color: "#AAAAAA"}}>Số
                                                  lượng: {value.quantity}</div>
                                              </div>
                                            </div>
                                            <div
                                              style={{color: "#FF4D4F"}}>{Intl.NumberFormat().format(value.product?.price)} đ
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}

                                </Link>
                              </div>
                            </div>
                            <div style={{borderBottom: "1px solid #F0F2F5"}}></div>
                            <div style={{padding: "24px 24px 12px"}}>
                              <div style={{display: "flex", justifyContent: "end", alignItems: "center"}}>
                                <div style={{marginRight: "5px"}}><DollarOutlined/></div>
                                <div style={{marginRight: "10px", fontSize: "14px", lineHeight: "20px"}}>Tổng số tiền:
                                </div>
                                <div style={{
                                  fontSize: "16px",
                                  lineHeight: "30px",
                                  color: "#FF4D4F"
                                }}>{Intl.NumberFormat().format(item.totalPrice)} đ
                                </div>
                              </div>
                            </div>
                            <div
                              style={{
                              padding: "12px 24px 24px",
                              display: item.status === "active" ? "block" : "none",
                              justifyContent: "end",
                            }}>
                              <Button
                                style={{float:"right"}}
                                type="danger" onClick={() => handleClickDetail(item)}>Đánh giá
                              </Button>
                            </div>
                          </div>
                        );
                      })
                    ) : (
                      <Empty/>
                    )}
                  </TabPane>
                )
              })}
            </Tabs>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Purchase;
