import React from "react"
import {Avatar, Menu} from "antd";
import {InboxOutlined,ContainerOutlined,BellOutlined} from "@ant-design/icons";
import {Link, Outlet} from "react-router-dom";

const { SubMenu } = Menu;

const SideBarDetail = () => {
    return (
        <>
            <div>
                <Menu mode="inline">
                    <SubMenu style={{marginTop: "30px"}} key="sub1" icon={<ContainerOutlined />} title="Đơn Mua">
                        <Menu.Item key="1"><Link to="">Xem Đơn</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub2" icon={<BellOutlined />} title="Thông Báo">
                        <Menu.Item key="2"><Link to="">Cập Nhật Đơn Hàng</Link></Menu.Item>
                        <Menu.Item key="3"><Link to="">Khuyến Mãi</Link></Menu.Item>
                        <Menu.Item key="4"><Link to="">Cập Nhật Ví</Link></Menu.Item>
                        <Menu.Item key="5"><Link to="">Hoạt Động</Link></Menu.Item>
                        <Menu.Item key="6"><Link to="">Cập Nhật Đánh Giá</Link></Menu.Item>
                    </SubMenu>

                    <SubMenu key="sub3" icon={<InboxOutlined/>} title="Kho Voucher">
                        <Menu.Item key="7"><Link to="">Xem Kho</Link></Menu.Item>
                    </SubMenu>

                </Menu>
            </div>
        </>
    )
}

export default SideBarDetail
