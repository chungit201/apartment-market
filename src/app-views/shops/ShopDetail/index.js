import React, {useEffect, useState} from "react"
import {Card, Rate, Row, Col, Space, Skeleton, Modal} from "antd";
import "./ShopDetail.css"
import {LinkOutlined} from "@ant-design/icons"
import SideBarDetail from "./component/SideBarDetail";
import ProductPage from "../../products/ProductPage";

import CategoriesLayOut from "../../../components/common-component/Categories";
import {getCategory} from "../../../service/ApiService/categoryApi";
import {getProductSale, getProductShop} from "../../../service/ApiService/productsApi";
import {useParams} from "react-router-dom";
import {getShopId} from "../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import ProductCarousel from "react-elastic-carousel";
import SliderSale from "../../sale-slide/SliderSale";
import SliderVoucher from "./component/SliderVoucher";
import {getVouchers} from "../../../service/ApiService/voucherApi";

const ShopDetail = () => {
  const {id} = useParams();
  const [category, setCategory] = useState([]);
  const [products, setProducts] = useState([]);
  const [shop, setShop] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const user = useSelector(state => state.user);
  const [productSale, setProductSale] = useState([]);
  const [loading, setLoading] = useState(false);
  const [voucher, setVoucher] = useState([]);

  const breakPoints = [
    {width: 100, itemsToShow: 1},
    {width: 100, itemsToShow: 2},
    {width: 100, itemsToShow: 3},
    {width: 100, itemsToShow: 4},
  ];

  useEffect(() => {
    getDataShop();
    productShop();
    getDataCategories();
    getDataProductSale();
    getDataVoucher();
  }, [])
  const getDataCategories = async () => {
    const {data} = await getCategory(1, 16);
    setCategory(data.results);
  };
  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShopId(id);
        setShop(data);
        resolve(data);
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }

  const productShop = async () => {
    const myShop = await getDataShop();
    if (shop) {
      const {data} = await getProductShop({shop: myShop.id, page: 1, limit: 24,action:"active"});
      setProducts(data.results);
      console.log("aaaa", data)
    }
  }
  console.log("aaal", products)

  const getDataProductSale = async () => {
    const myShop = await getDataShop();
    try{
      const {data} = await getProductSale(myShop.id);
      console.log("sale", data.results)
      setProductSale(data.results);
    } catch (err) {

    }

  };

  const getDataVoucher = async () => {
    const myShop = await getDataShop();
    try {
      const {data} = await getVouchers(myShop.id)
      console.log("voucher", data.results)
      setVoucher(data.results);
    } catch (err) {

    }
  }
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <div className="webs">
        <Modal title="Voucher" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
          <div className="text-center" style={{marginBottom:"10px"}}>Sao chép mã code để áp dụng</div>
          {voucher.map(item=>{
            return (
              <div>
                <div className="d-flex justify-content-between">
                  <div className="d-flex" style={{marginBottom:"20px"}}>
                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAADOxJREFUaAWNmMmPXVcRxs/rwW13t6e4HTsDOAqQkEEJQ4IQOxZZILFAbFkFlkQoi/DPMEiss0FICFYRAoRIEARBkIijkJhMJLbTbfdku90D3++rqvPuex0Sznv3nnOq6vuq6kz3vjf62atXDy6ubbfWDtpoRhX1SNUo6gP3D3q/oWv7aSOx+9IbT52yxGMbmDF/cZcfsNN+y8a69DG0J9byff/CqTb36uqNdC5r+KgOIiArwoVBlks1mpmxzUiAsjTpgcCH8BLIajQj6+Qdybja5QNZlUM6FGDwhv/CwyfYv26ttZnghjgSqL4tPJREJiKCdJ+2LrVtay5skmPfyrQPO2MTY47kCvxITBrejh9i8MmgqfJVMYAhMfzCCAOk2YlAKymJJTcJINpaJZMJ4CR0hQ09DsGOdW6DL5lqPvguH44lfdnOHDIo/zQTD8Z460ZtTqokCgXTyjJwKLamFWTYuukKmQqx9KYC18dYlkp4SzN0FN0JJnu5ZsOmQ9RgvyYvSi9N9hsFimhJESDPiCcmlbH+ZFgjlxl7NkBjpytTdZ82Msu1C6tNXSO4z5Ib9I2XD2RjPK2QlR+0wAKPLnzVciw8ez8DywA4ZNKBdRmAHeAkLwfbl1oEULIIJmwrGTvueMVDXkTY/UcC07IxPvQgCjO0nZs8IWRse7zQjD5QLzn19zU1zGbcJCdR+pSq1Sj7kkVfIyvvPqHSNvB0nFXo6LkbMjjog2NmpvHE46UVEUAWs1OZTvcn5DkbQ2ycMCQrqp5BBQV3zJx5B3g8214R4WN/oLMtKyRj8+DSDidGotOMqJJdzQzZmlgKx0NvSu/RtdNuEbZwDQ6K3PbmK7Z05grQBEPhCaD7t/tuF7GyKgiq8D4bEIDMGqXZw9Bp0dcV4LF9t0ssLCafxqe8nEdUMfoRZnCWr847mEFi5GPdYIagpvTN7mQygDqxGFFfJdeU18xBaMdF3usYiHDovMbPH+NxOx0QDgYydeuUtP9canCaF1tkJKSL9lwfHUKOeYtplzGoWka4R9SLGREQRNxrxMvMA1ebM2JNwwJQh4KlZ3wnoyG18TR05XJCzrvYMKC+R0J44I1qGx5ImAJW8RoXNqZXTqfendKTEzdAtzNH5tuzD19w98evv93euXHDfA6u8HCqneMRfoToQ2T/zJanIHJR8iM/ryRLfD+1GBiuWi6xJslmMOWmx0XK6Nd6hdO+Qj+nAM4dW3AS3FYW5vViFzr6nggaFJZH+i++SAV+bHUb+Ip24IA7medeuKz0yJD+QYvBoU9WXPriXzrL1IrXfUU9oZPFaL89cnqpfe3cifappYU2G0DALjs6V9/eutFe29xqf1271ugHFz7CX8Ux4dOrI2J0HNkftkfPvXBFDPoSaBk4wEjCcpx0PTFlAINEjs629p3PnW33nziGwSeWm3t77fdXVttLq2ttz+9QGah8OYkcxPAP3UA+aJetX+NjmhVVTT2pRezjKdfw9+kXup9s5Kvru58/938nQVhHZ2fbU+fPtsdPnghfTG9e9uNl6vi7vvsnFkh0K1ludgJjYSlr1X3zMeIJoOodEib6+Lavnj/e7tFSGpZLGzfbK2ubbeP2Xjs+P9MuLB9tD51aavP6UXa4hH/LxWn/dNS2T6KlMPxdVk1mitd4zwJoh4y1kwoCw23oU4ttwWzo40MBiJx8YWUxDPP+5vrN9pOL7/elCvWfrq63hdmZ9tgdy+3LZ5T44tG0JjixJReNvrkddfp0MgSNy6gjFmLgOUJNHqatIANsheW62VN2ZB/JhPzOY/Nl5Xprd0/m9jiWayZu6Znw5w/XdV1vj+pQ+MbdZ0VE4GHmwAiSTwTkIK2tPh38V9Ay1EODdy29YNPR2YltkIUxCJ/XbEbPxoDXgdI/aHsZCD4oD59ebE+sLLe/XN2AwTY8oAISEf3j2nZ7c+OtdmphzoGPkxkEKd5KDl6vAtUeRC9RZ+QY51BGdlkbIWfOOGQmGIuwsJ74cfTu5k777KlaKnrvkexb951pT91zqr2+fqO9of3y+vp2u35718kEvrWt3f22vbdDhJMBO2Nn4djwQfp9FuiRQ/pHPhe/BzJbNnCWmDk8hABjjw5nPzLfRKaR/sN/tiYSKY6l+dn2+JllX/wOuXh9u714eb29sal/bga8Ze9avI4Cf6VQg3jwGa8sqbE8YtYRgrAufiViDEPIIODymBiT8oHNP1dvtd++uwHofxZmiVPr6Qfuat974O52xzzbs/yOa/vT4JQujvnUd//EpI77uNT2oO9pUqMSiHUZCfhUyw1pW4GGdeF/fWm9Pf/aalvf2YP5Y8t9Ooq//9C97ZGTS8E1xc/PYnjDj6hoa+CGfgne+09Y9BoWsg1jms5UXZIx0lMtiIzj+aIGBbmYbUdf4pev3Giv6A+/J+5cbI/pSL5w/Ij3C+rpwvPk2xfOtss3d9qVW7eNdyiaDWKL3/g4FRKXlmVM0cllhk7r5dlfXZMZAXGPuumdCbztZ1BL3l9fygaDakfNMTh+V2ttaX7UPnNqQU/8hfbQ6WPtxBGf9gB7uaT98tPX3uv8k3EkP37K/6CNrUdYsonfIxG9lDEkNDyltdQMzNFhNlj32OTXs4O6ytbuQfv7Vc3Sh9vtl5fW2hPnlts3P326zZJtlnsWF+R2anZLqTpmnYYu+7NQNxW7103f/Ms0pEGoNrHlFdayHPRZr2VL+5EzR5veGWWT++cQfkbPmlF76YPN9scPJg8FltjK0SPGgvcfD4fwwVvLntiIIGKk481OWhKymXR5NtSPUDWfFRwytZHbHmfZfvL8YvvBl1bavcvzmumxDcmOBwDu1q5/xGGgF4E+cMaYNyIY4+Eac9t39cXr49cJCFyBOWMCVWOcvQTEUkcjXRLPcn5pvj3zxZX29KN3tPtPapNL7sFJZ/As67nylTuXC+J6Y2e3rbHZ8R8uohYu8JhFe1j3BLETzk92TL0WPRKSamd541ffHgSAUuv0gPnXOq8tAr7Kg6cXGteO3lve2thRkLtaVgd6A57VQ3PBL45lS/3i5XiNcTRw4wviqjHCMZVk4T9qhxeq+F+LdhyjDjUAANNoqC8yHOELp5s7g6lJzJHZkQNvbfL1fkDZeEv+3XvrXjH4Z+G4wK0G/YrB8anjASZhNKWUnV5RBNDoeiQAiiTex5I42UwAdfcmY+hE9vyrm+1vl2+1r19Y1HGrjfsJBR8vX9lqv3hzVb8OGYvwD5eajqco7E4KYq5khhk6LoHigVgMJqqk6AidMupKpupIyop2cfW2rmvt3uOz7cEzR7xPLpyc70tpV3vr8vbt9m8ttxff34yHIFFTRNG51KhBtQ4Vdk6GJaWmuAzteJ4jKuRBiZHJ1/mcpcwlyMLMjmOM8C/H+jgk3d7Z2NXb8G77zWi7rSzOtB8+qd8cKj965cP2lt6S4w+Mcpg+Oz6SgLOCB+s8eyBIiDk4SpzPEY5ZtJgM3nPcl6lqgGDrIgUfzUBIBlufUImnazx62oVPWz+qs2188HlgDvkprrAPXqcrXuqZNnrm51t2X4/6mDIxibdGL2QOp78qTOrCXu4CIyx8MfO1DMhq3D6kAzPET9gO+T+ae6af1eFHcAqjo4oUnWaNnLo8R2zkaP3PJKMS9lQhD2xySFaj6HqKN2TY6pv+1IuScUzipbId9jHT47dfYAyTFQon34dY/3wnTgx42HBpA7SXJC6d13LHu2HT+Gktf+Q9UeTRmzkUQqgwkxFbTrMTYFEVPl8aHWsErOACExRl6YREGQdCBhTDlK5cWU8Injke7xSZ21TE1mVg9gCVPk7YceGffj6bAKBnJaSeWGxjbrVV+xXFFn2qCCLB9hTLgkAcBrLgDLtaClnXUo3gwkUtF+PtHA6CCy77R07A4u6+BrLwniODL+Mxxod/igW4SGxAsOUkiUvvkcVhJms5o5VBVG07xHmRjIOcGqSPw8feI+jgOYwnsYhlziHoRq354q5SteSgvdZS3G3DytPsJNLO+PFtvGSQBa+TlMdYQklvofRj1ybp/sdoDwhKb1FmRF+fWm4hqFEj2LzQ1ZKKAGJkY+nIrmam8OoXtuz9szW5wxfrmufNYFTAS1pL0zOY3N1/zjyj3v2bI19RwmFsGjuC07S62VeMnqctdQ5CdHwn8Lmhx3jmDHxwIacjWgFV53/IFufNgRsT5PDXKWggYuN1Uxvq2OyWMtKIwsqJ5ugiI3DMfDRSu0MjL9uoTVeyCTwB5+gaV7bGhr/wARpfU3hkQ3zxJR57zW8EHw6CwMkInI9xCZHjMGzj52i0w3aMq0BjyMY2zHcN1BAfox/BVxLmTF/BHzzFXTW6ws88ele8GzEWBcJwvFYjSAOcUMCxNmHKCovdUG4u2RzCJ670Ha8EPgpvfenEFoMbMTxwYrH9F3TfbEPDytIDAAAAAElFTkSuQmCC"}/>
                    <div>
                      <span style={{marginLeft:"10px"}}>Mã code: <span style={{color:"#00c7b8"}}>{item.code}</span></span>
                      <div style={{marginLeft:"10px"}}>{item.name}</div>
                    </div>
                  </div>
                  <div >Còn lại {item.usages} </div>
                </div>
              </div>
            )
          })}
        </Modal>
        <div className="banner banners">
          <div className="image">
            <img width="100%" height="248"
                 src={`https://salt.tikicdn.com/cache/w1080/ts/tmp/b9/a0/f7/f713521528ac305f1a20b6a32f972178.jpg`}/>
          </div>

          <div className="logo">
            <Card
              className="d-flex"
              bordered={false}
              cover={
                <img
                  style={{width: "100px"}}
                  alt="example"
                  src={shop.avatar}
                />
              }
            >
              <div>
                <div>
                  <span>{shop.name}</span>
                </div>
                <div>
                  <Rate
                    style={{fontSize: "10px"}}
                    disabled
                    defaultValue={4}
                  />
                </div>


              </div>

            </Card>
            <div className="d-flex">
              <div>
                <img width="24" height="24"
                     src="https://salt.tikicdn.com/ts/upload/80/e0/ef/fff5ef4b4a6afe9e1ff8ce3f22dd045a.png"/>
              </div>
              <div style={{padding: "0px 20px"}}>Ngày mai trước 11h00</div>
            </div>
            <div className="d-flex" style={{marginTop: "16px"}}>
              <div>
                <img width="24" height="24"
                     src="https://salt.tikicdn.com/ts/upload/59/06/35/9b9cbfe33ebd70fb8eede4c073d426db.png"/>
              </div>
              <span style={{padding: "0px 20px",cursor:"pointer"}} onClick={showModal}>Mã giảm giá shop</span>
            </div>
          </div>
          <div className="link1">
            <div className="link2">
              <LinkOutlined width="15" height="15"/>
            </div>
          </div>
        </div>


        <Row gutter={8}>
          <Col span={24}>
            <div className="container mt-3" style={{backgroundColor: "#fff"}}>
              <Row gutter={8}>
                <Col xxl={24} xl={24} xs={0} sm={0} lg={0}>
                  <h5 className="mt-4">
                    Flash sale{" "}
                    <img src="https://frontend.tikicdn.com/_desktop-next/static/img/dealFlashIcon.svg"/>
                  </h5>
                  <ProductCarousel
                    style={{backgroundColor: "#fff", borderRadius: "10px"}}
                    showEmptySlots={false}
                    className="mt-2"
                    breakPoints={breakPoints}
                  >
                    {productSale.map((item) => {
                      return (
                        <Skeleton loading={loading} active avatar>
                          <SliderSale key={item.id} product={item}/>;
                        </Skeleton>
                      );
                    })}
                  </ProductCarousel>
                </Col>
              </Row>
            </div>
          </Col>

          {/*<Col span={12}>*/}
          {/*  <div className="container mt-3" style={{backgroundColor: "#fff"}}>*/}
          {/*    <Row gutter={8}>*/}
          {/*      <Col xxl={24} xl={24} xs={0} sm={0} lg={0}>*/}
          {/*        <h5 className="mt-4">*/}
          {/*          Voucher*/}
          {/*        </h5>*/}
          {/*        <ProductCarousel*/}
          {/*          style={{backgroundColor: "#fff", borderRadius: "10px"}}*/}
          {/*          showEmptySlots={false}*/}
          {/*          className="mt-2"*/}
          {/*          breakPoints={breakPoints}*/}
          {/*        >*/}
          {/*          {voucher.map((item) => {*/}
          {/*            return (*/}
          {/*              <Skeleton loading={loading} active avatar>*/}
          {/*                <SliderVoucher key={item.id} voucher={item}/>;*/}
          {/*              </Skeleton>*/}
          {/*            );*/}
          {/*          })}*/}
          {/*        </ProductCarousel>*/}
          {/*      </Col>*/}
          {/*    </Row>*/}
          {/*  </div>*/}
          {/*</Col>*/}
        </Row>


        <div className="container" style={{backgroundColor: "#fff", marginTop: "20px"}}>
          <Row gutter={8}>
            {products &&
              products.map((item) => {
                return (
                  <Col xxl={4} xl={4} xs={12} sm={6}>
                    <ProductPage key={item.id} product={item}/>
                  </Col>
                );
              })}
          </Row>
        </div>

        <div className="container">
          <h5 className="mt-4">Tìm theo danh mục</h5>
          <Card className="mt-2">
            <div className="container">
              <Row>
                {category.map((item) => {
                  return <CategoriesLayOut key={item.id} category={item}/>;
                })}
              </Row>
            </div>
          </Card>
        </div>
      </div>
    </>
  )
}

export default ShopDetail