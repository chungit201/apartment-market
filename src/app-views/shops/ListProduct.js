import React, {useEffect, useState} from 'react'
import {
  Table, Card, Button, Form, Modal, Input, Row, Col,Select
} from "antd";
import {NavLink} from "react-router-dom"
import {getShop} from "../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {getProductShop, removeProduct,} from "../../service/ApiService/productsApi";
import {EditOutlined, DeleteOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer, toast} from "react-toastify";
const { confirm } = Modal;
const {Column} = Table;
const { Option } = Select;
const ProductShop = () => {
  const user = useSelector(state => state.user);
  const [shop, setShop] = useState({});
  const [products, setPoducts] = useState([]);
  const [loading,setLoading] = useState(false);
  const [page,setPage] =useState(1);
  let limit = 24
  useEffect(() => {
    productShop().then(() => {
    });
  }, [user])

  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        resolve(data);
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }

  const productShop = async () => {
    setLoading(true)
    const myShop = await getDataShop();
    if (shop) {
      const {data} = await getProductShop({shop:myShop.id,page:1,limit:1000000,sortBy:"-createdAt"});
      setPoducts(data.results);
    }
    setLoading(false)
  }


  function handleChange(value) {
    console.log(`selected ${value}`);
  }
  function showDeleteConfirm(id) {
    confirm({
      title: 'Bạn chắc chắn muốn xóa sản phẩm này?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: async () => {
        try {
              const {data} = await removeProduct(id);
              const newProduct = products.filter((item) => item.id !== data.productDelete.id);
              setPoducts(newProduct);
              toast.success("Xóa sản phẩm thành công");
            } catch (error) {

            }
            console.log(id)
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  const [form] = Form.useForm();
  form.setFieldsValue(user)
  return (
    <div className="container">
      <ToastContainer/>

      <Card title="Danh sách sản phẩm" style={{paddingTop: "20px"}}>
        <Row>
          <Col span={10}>
            <Form>
              <div className="d-flex w-100">
                <Form.Item name={"name"}>
                  <Input className="w-100" style={{borderRight:"none"}} placeholder="Nội dung tìm kiếm..."/>
                </Form.Item>
                <Form.Item >
                  <Button>Tìm kiếm</Button>
                </Form.Item>
              </div>
            </Form>
          </Col>
          <Col span={14}>
            <div className="d-flex justify-content-end">
              <span>Bộ lọc: </span>
              <Select className="mx-2" defaultValue="all" style={{ width: 120 }} onChange={handleChange}>
                <Option value="all">Tất cả</Option>
                <Option value="create">Mới nhất</Option>
                <Option value="-create">Cũ nhất</Option>
              </Select>
            </div>
          </Col>
        </Row>
        <Table loading={loading} dataSource={products} >
          <Column title="Sản phẩm" render={(record) => {
            return <p style={{width: "200px"}}>{record}</p>
          }} dataIndex="name" key="name"/>
          <Column title="Giá" dataIndex="price" key="price"/>
          <Column title="Ảnh" dataIndex="img" render={(record) => {
            return <img style={{width: "100px"}} alt="ảnh" src={record}/>
          }} key="img"/>
          <Column title="Số lượng" dataIndex="quantity" key="ship"/>
          <Column
            title="Lượt xem"
            render={ (record, index) => {
              return (
                <div>
                  {record.views}
                </div>
              )
            }}
            key="view"/>
          <Column
            title="Action" key="action"
            render={(value, record, index) => {
              return (
                <div>
                  <NavLink to={`/shop/editproduct-shop/${record.id}`}><EditOutlined/></NavLink>
                  <DeleteOutlined onClick={() => showDeleteConfirm(record.id)}/>
                </div>
              )
            }}
          />
        </Table>
      </Card>
    </div>
  )
}

export default ProductShop