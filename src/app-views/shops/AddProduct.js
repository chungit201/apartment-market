import React, {createRef, useEffect, useState} from "react";
import {LoadingOutlined, PlusOutlined} from '@ant-design/icons';
import {
  Menu, DatePicker, Row, Col, Input, Radio, Button, Upload, message, Form, Card, Select, Checkbox, notification,
} from "antd";
import {Option} from "antd/es/mentions";
import {getShop} from "../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {getCategories} from "../../service/ApiService/categoryApi";
import Editor from "../../components/util-components/DynamicImport/dynamic-editor"
import {addProduct} from "../../service/ApiService/productsApi";
import {storage} from "../../firebase";
import {getProperties} from "../../service/ApiService/propertiesApi";
import ImgCrop from 'antd-img-crop';

const refEditor = createRef();
const {TextArea} = Input;

const rules = {
  validateName: [
    {
      required: true,
      message: 'Vui lòng nhập tên sản phẩm'
    }
  ],
  validatePrice: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({getFieldValue}) => ({
      validator(rule, value) {
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Giá phải là số');
      }
    })
  ],
  validateDesc: [
    {
      required: true,
      message: 'Vui lòng nhập mô tả sản phẩm'
    }
  ],
  validateQuantity: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({getFieldValue}) => ({
      validator(rule, value) {
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Số lượng phải là số');
      }
    })
  ]
}



const AddProduct = () => {
  const [sale, setSale] = useState(false);
  const [shop, setShop] = useState({});
  const [category, setCategory] = useState([]);
  const [dataCategory, setDataCategory] = useState([]);
  const [selectedFile, setSelectedFile] = useState();
  const [properties, setProperties] = useState(false);
  const [dataProperties, setDataProperties] = useState([]);
  const [colorValue, setColorValue] = useState([]);
  const [sizeValue, setSizeValue] = useState([]);
  const [imgPreview, setImgPreview] = useState([]);
  const [imgTitle, setImgTitle] = useState("");
  const [fileList, setFileList] = useState([]);
  const [imgActive, setImgActive] = useState([]);
  const [images, setImages] = useState([]);
  const [urls, setUrls] = useState([]);
  const [progress, setProgress] = useState(0);
  const [imageUrl, setImageUrl] = useState("");
  const [loading, setLoading] = useState(false)
  const [priceValue,setPriceValue] = useState("")
  const [from] = Form.useForm()

  const user = useSelector(state => state.user)
  let categories = []

  function onChange(e) {
    setSale(!sale)
  }

  const rules = {
    validateName: [
      {
        required: true,
        message: 'Vui lòng nhập tên sản phẩm'
      }
    ],
    validatePrice: [
      {
        required: true,
        message: 'Không được để trống !'
      },
      ({getFieldValue}) => ({
        validator(rule, value) {
          console.log(parseInt(value));
          let newValue = parseInt(value)
          if (!value || newValue) {
            return Promise.resolve();
          }
          return Promise.reject('Giá phải là số');
        }
      })
    ],
    validateDesc: [
      {
        required: true,
        message: 'Vui lòng nhập mô tả sản phẩm'
      }
    ],
    validateQuantity: [
      {
        required: true,
        message: 'Không được để trống !'
      },
      ({getFieldValue}) => ({
        validator(rule, value) {
          console.log(parseInt(value));
          let newValue = parseInt(value)
          if (!value || newValue) {
            return Promise.resolve();
          }
          return Promise.reject('Số lượng phải là số');
        }
      })
    ],
    priceSale: [
      {
        required: true,
        message: 'Không được để trống !'
      },
      ({getFieldValue}) => ({
        validator(rule, value) {
          console.log(parseInt(value));
          console.log(priceValue)
          if (priceValue>value) {
            return Promise.resolve();

          }
          if (priceValue<value){
            console.log("vvvv",priceValue<value)
            return Promise.reject('giá sale  phải nhỏ hơn giá mặc định');
          }
          let newValue = parseInt(value)
          if (!value || newValue) {
            return Promise.resolve();
          }

            return Promise.reject('giá  phải là số');
        }
      })
    ]
  }


  useEffect(() => {
    getDataCate().then(() => {
    })
    getDataType()
  }, [user])

  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        setCategory(data.categories)
        resolve(data.categories);
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }

  const getDataType = async () => {
    const {data} = await getProperties();
    setDataProperties(data.results)
  }

  const handleChangeImm = ({fileList}) => {
    setImgActive(fileList);
  };


  const updateFirestore = (dlURL) => {
  }
  const getDataCate = async () => {
    const data = await getDataShop();
    for (let item of data) {
      const {data} = await getCategories(item);
      categories.push(data);
    }
    setDataCategory(categories)
  }

  const onChangeImgArr = ({fileList: newFileList}) => {
    console.log(newFileList)
    setFileList(newFileList);

  };
  const handleSubmit = async (values) => {
    let imageArr = []
    setLoading(true)
    console.log(values)
    const description = refEditor.current.getInstance().getMarkdown();
    console.log("dddddd", description)
    const promises = [];

    const test = new Promise(resolve => {
      fileList.map(async (image) => {
        const uploadTask = storage.ref(`images/${image.originFileObj.name}`).put(image.originFileObj);
        promises.push(uploadTask);
        const uploadImage = new Promise((resolve, reject) => {
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setProgress(progress);
            },
            (error) => {
              console.log(error);
            },
            async () => {
              await storage
                .ref("images")
                .child(image.originFileObj.name)
                .getDownloadURL()
                .then((urls) => {
                  resolve(urls)
                  // setUrls((prevState) => [...prevState, urls]);
                });
            }
          );
        })

        const dataImage = await uploadImage.then(res => {
          imageArr.push(res);
          console.log("hhhhhhhhh", imageArr)

          return  imageArr;
        })
        resolve(dataImage);

      });
    });

    test.then(images=>{
      const ref = storage.ref(`images/${imgActive[0].originFileObj.name}`);
      const upload = ref.put(imgActive[0].originFileObj);
      upload.on("state_changed", snapshot => {
      }, error => {
      }, () => {
        storage.ref('images')
          .child(imgActive[0].originFileObj.name)
          .getDownloadURL()
          .then(async (url) => {
            console.log(url)
            console.log("8888888888888", urls)
            const {data} = await addProduct({
              sale: sale, shop: shop.id, img: url, imgDetail: images, description: description, ...values
            });
            if (data) {
              notification.success({message: data.message});
            }
            setLoading(false);
            from.setFieldsValue("")
          }).catch(err=>{
          console.log(err);
          notification.error({message:err})
        })
      })
    });


  }

  const handChangeImg = (e) => {
    setSelectedFile(e.target.files[0])
  }


  const onChangeProperties = () => {
    setProperties(!properties)
  }

  const handleColor = (value) => {
    setColorValue(value)
  }

  const handleChangeSize = (value) => {
    setSizeValue(value)
  }

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined/> : <PlusOutlined/>}
      <div style={{marginTop: 8}}>Upload</div>
    </div>
  );

  const color = ['Đen', 'Trắng', 'Đỏ', 'Vàng', 'Xanh', 'Tím', 'Hồng'];
  const size = ['XL', 'XXL', 'XS', 'S', 'M', 'L'];


  return (<Card style={{backgroundColor: "#fff", borderRadius: "10px", paddingLeft: "20px"}}>
    <Form
      form={from}
      method="m"
      name="basic"
      layout="vertical"
      onFinish={handleSubmit}
      requiredMark={false}
      initialValues={{remember: true}}
      autoComplete="off"
    >
      <h4 className="p-3">THÊM SẢN PHẨM MỚI</h4>
      <Row>
        <Col span={24}>
          <div>Ảnh sản phẩm</div>
          <div className="d-flex">
            <div>
                <Upload
                  action={""}
                  className="avatar-uploader"
                  listType="picture-card"
                  fileList={imgActive}
                  beforeUpload={false}
                  onChange={handleChangeImm}
                >
                  {imgActive.length < 1 &&
                    "+Ảnh bìa"
                  }
                </Upload>

            </div>
            <div>
              <ImgCrop rotate>
                <Upload
                  action={""}
                  className="avatar-uploader"
                  listType="picture-card"
                  fileList={fileList}
                  onChange={onChangeImgArr}
                >
                  {fileList.length < 4 &&
                    "+Ảnh sp"
                  }
                </Upload>
              </ImgCrop>
            </div>
          </div>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>

          <Form.Item
            label="Tên sản phẩm"
            name="name"
            rules={rules.validateName}
          >
            <Input placeholder="Mời nhập"/>
          </Form.Item>


          <div className="d-flex">
            {/*{imgPreview.length > 0 &&*/}
            {/*  <div>*/}
            {/*    {imgPreview.map(item => {*/}
            {/*      return (*/}
            {/*        <img style={{ marginRight: "20px" }} width="70px" id="blah" src={item} alt="your image" />*/}
            {/*      )*/}
            {/*    })}*/}
            {/*  </div>*/}
            {/*}*/}
          </div>
          <Form.Item
            label="Giá"
            name="price"
            rules={rules.validatePrice}
          >
            <Input placeholder="Mời nhập" onChange={(e)=>{
              setPriceValue(e.target.value);
            }}/>
          </Form.Item>
          <Form.Item
            label="Số lượng"
            name="quantity"
            rules={rules.validateQuantity}

          >
            <Input placeholder="Mời nhập"/>
          </Form.Item>
        </Col>

        <Col span={12}>

          <Form.Item
            label="Danh mục"
            name="categories"
            rules={[
              {
                required: true,
                message: 'Bạn chưa chọn danh mục sản phẩm'
              }
            ]}
          >
            <Select>
              {dataCategory && dataCategory.map(item => {
                return (<Option key={item?.id} value={item?.id}>{item?.name}</Option>)
              })}
            </Select>
          </Form.Item>

          <Form.Item label="Type" name="type">
            <Select>
              <Option value="breakfast"> Đồ ăn sáng</Option>
              <Option value="food"> Thực phẩm</Option>
              <Option value="rice"> Cơm</Option>
              <Option value="snacks">Đồ ăn nhanh </Option>
            </Select>
          </Form.Item>


          <div className="mt-5">
            <div>Trạng thái và thuộc tính sản phẩm</div>
            <Checkbox onChange={onChange}>Sale</Checkbox>
            <Checkbox style={{marginBottom: "10px"}} onChange={onChangeProperties}>Thuộc tính</Checkbox>
          </div>

          {properties ? (<div>
            <Form.Item name="color" label="Màu sắc">
              <Select
                mode="multiple"
                allowClear
                style={{width: '100%'}}
                placeholder="Chọn màu"
                onChange={handleColor}
              >
                {color.map(item => {
                  return (<Option key={item}>{item}</Option>)
                })}
              </Select>
            </Form.Item>

            <Form.Item name="size" label="Size">
              <Select
                mode="multiple"
                allowClear
                style={{width: '100%'}}
                placeholder="Chọn size"
                onChange={handleChangeSize}
              >
                {size.map(item => {
                  return (<Option key={item}>{item}</Option>)
                })}
              </Select>
            </Form.Item>
          </div>) : ""}

          {sale ? (<div className="mt-2">
            <Form.Item label="Ngày hết hạn" name="expirationDate">
              <DatePicker style={{width: '100%'}}/>
            </Form.Item>
            <Form.Item rules={rules.priceSale} label="Giá sale" name="priceSale">
              <Input/>
            </Form.Item>
          </div>) : ""}

        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <div>Mô tả sản phẩm</div>
          <Editor
            previewStyle="vertical"
            height="400px"
            initialEditType="wysiwyg"
            useCommandShortcut={true}
            ref={refEditor}
          />
        </Col>
      </Row>
      <Form.Item>
        <Button
          className="mt-2"
          htmlType="submit"
          loading={loading}
          style={{backgroundColor: "rgb(26, 148, 255)", color: "#fff", height: "40px"}}
        >
          Thêm mới
        </Button>
      </Form.Item>
    </Form>
  </Card>);
};

export default AddProduct;
