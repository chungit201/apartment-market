import React, {useEffect, useState} from "react";
import "../cusstom.css";
import moment from "moment";
import {
  getShopFilter,
  getShopOrder, getShopOrderStatus,
  getTimeOrders,
  searchCodeOrder,
  updateOder,
} from "../../service/ApiService/oderUserApi";
import {getShop} from "../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {
  Menu,
  Select,
  Tabs,
  DatePicker,
  Space,
  Input,
  Row,
  Divider,
  Col,
  Button,
  Table,
  Drawer,
  Spin,
  Empty,
  notification, Skeleton, Pagination,
} from "antd";
import {EyeOutlined} from "@ant-design/icons";
import {sendToOne} from "../../service/ApiService/pushNotificationApi";
import {addNotification} from "../../service/ApiService/notificationApi";

const {Option} = Select;

const {TabPane} = Tabs;

function callback(key) {
  console.log(key);
}

const {RangePicker} = DatePicker;
const {Column} = Table;

const menu1 = (
  <Menu>
    <Menu.Item key="1">
      <a href="/order-shop">Tất cả</a>
    </Menu.Item>
    <Menu.Item key="2">
      <a href="#">Đơn hủy</a>
    </Menu.Item>
    <Menu.Divider/>
    <Menu.Item key="3">Trả hàng / Hoàn tiền</Menu.Item>
  </Menu>
);

const menu2 = (
  <Menu>
    <Menu.Item key="4">
      <a href="/list-shop">Tất cả sản phẩm</a>
    </Menu.Item>
    <Menu.Item key="5">
      <a href="/addproduct-shop">Thêm sản phẩm</a>
    </Menu.Item>
  </Menu>
);

const OrderShop = (props) => {
  const user = useSelector((state) => state.user);
  const rootSubmenuKeys = ["sub1", "sub2", "sub4"];
  const [orders, setOrders] = useState([]);
  const [oder, setOder] = useState(null);
  const [searchCode, setSearchCode] = useState("");
  const [openKeys, setOpenKeys] = React.useState(["sub1", "sub2", "sub4"]);
  const [loading, setLoading] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [shop, setShop] = useState({})
  const [date, setDate] = useState([])
  const [page, setPage] = useState(1);
  const publicUrl = new URL(process.env.PUBLIC_URL, window.location);

  let limit = 10;
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  useEffect(() => {
    getOrderShop();
  }, []);

  const getDataShop = async () => {
    try {
      return new Promise(async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        resolve(data);
      });
    } catch (err) {
      window.location.replace("/");
    }
  };

  const getOrderShop = async () => {
    setLoading(true)
    const myShop = await getDataShop();
    const {data} = await getShopOrder(myShop.id, page, limit);
    setTotalPage(data.totalPages)
    setOrders(data.results);
    setLoading(false)
  };
  const handleChange = async (order, value) => {
    console.log(value);
    const {data} = await updateOder(order.id, {
      status: value,
      payment: value === "active" ? true : false
    });
    await sendToOne({
      title: "Apartment market",
      body: value === "active" ? `Đơn hàng của bạn đã được giao thành công` : "Đơn hàng của bạn đã bị từ chối",
      click_action: `${publicUrl.origin}/shop/order-shop`,
      icon: value === "active" ? "https://scontent.xx.fbcdn.net/v/t1.15752-9/275844082_348809860504907_4731915775493283911_n.png?stp=dst-png_s206x206&_nc_cat=104&ccb=1-5&_nc_sid=aee45a&_nc_ohc=5m_Oy5AzJm4AX_pnt2Y&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJ6bYu7ruyeyVePaM28kH9ia-Jg0-LJb2dE4P1-FWKFAw&oe=625728F2" :
        "https://ntlogistics.vn/tin-tuc/wp-content/uploads/2020/07/cancel-order-1-1024x1024.jpg",
      to: order.user.deviceToken,
    });
    await addNotification({
      sender: shop.id,
      recipient: order.user.id,
      title: shop.name,
      img: value === "active" ? "https://scontent.xx.fbcdn.net/v/t1.15752-9/275844082_348809860504907_4731915775493283911_n.png?stp=dst-png_s206x206&_nc_cat=104&ccb=1-5&_nc_sid=aee45a&_nc_ohc=5m_Oy5AzJm4AX_pnt2Y&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJ6bYu7ruyeyVePaM28kH9ia-Jg0-LJb2dE4P1-FWKFAw&oe=625728F2" :
        "https://ntlogistics.vn/tin-tuc/wp-content/uploads/2020/07/cancel-order-1-1024x1024.jpg",
      actionClick: "/purchase",
      content: value === "active" ? `Đơn hàng ${oder.code} của bạn đã được giao thành công` : `Đơn hàng ${oder.code} của bạn đã bị từ chối`,
    })
    console.log(data);
    notification.success({message: "Cập nhật đơn hàng thành công"});
  };

  const [visible, setVisible] = useState(false);
  const showDrawer = (order) => {
    setVisible(true);
    setOder(order);
  };
  const onClose = () => {
    setVisible(false);
  };

  const onSearch = async () => {
    const {data} = await searchCodeOrder(searchCode);
    setOrders(data.results);
  };

  const onChangeDate = async (value, dateString) => {
    console.log("ddddddddd", value)
    const start = moment(value[0]).format("x");
    const end = moment(value[1]).format("x")
    console.log("ddddddddd", start, end);
    const {data} = await getTimeOrders(start, end);
    setOrders(data.results)
  }

  const handleChangePagination = async (value) => {
    setPage(value)
    setLoading(true)
    const myShop = await getDataShop();
    const {data} = await getShopOrder(myShop.id, value, limit,);
    setOrders(data.results);
    setLoading(false)
  }

  const handleSelect = async (value) => {
    console.log(`selected ${value}`);
    if (value === "all") {
      setLoading(true)
      const myShop = await getDataShop();
      const {data} = await getShopOrder(myShop.id, page, limit);
      setTotalPage(data.totalPages)
      setOrders(data.results);
      setLoading(false)
    } else if (value == "-createdAt") {
      setLoading(true)
      const myShop = await getDataShop();
      const {data} = await getShopFilter({shop: myShop.id, page: page, limit: limit, sortBy: "-createdAt"});
      setTotalPage(data.totalPages)
      setOrders(data.results);
      setLoading(false)
    } else if (value == "createdAt") {
      setLoading(true)
      const myShop = await getDataShop();
      const {data} = await getShopFilter({shop: myShop.id, page: page, limit: limit, sortBy: "createdAt"});
      setTotalPage(data.totalPages)
      setOrders(data.results);
      setLoading(false)
    } else {
      setLoading(true)
      const myShop = await getDataShop();
      const {data} = await getShopOrderStatus(myShop.id, page, 1000, value);
      setOrders(data.results);
      setLoading(false)
    }
  }

  return (
    <div>
      <Drawer
        size="large"
        title="Chi tiết"
        placement="right"
        onClose={onClose}
        visible={visible}
      >
        {oder !== null && (
          <div>
            <h6>Thông tin khách hàng</h6>
            <Row gutter={16}>
              <Col span={12}>
                <p style={{color: "#72849a"}}>Địa chỉ: {oder.adress}</p>
              </Col>
              <Col span={12}>
                <p style={{color: "#72849a"}}>
                  Khách hàng: {oder.user.fullName}
                </p>
              </Col>
              <Col span={12}>
                <p style={{color: "#72849a"}}>phone: {oder.user.phone}</p>
              </Col>
            </Row>
            <Divider/>

            <h6>Đơn hàng</h6>
            <Row gutter={16}>
              <Col span={12}>
                <p style={{color: "#72849a"}}>Mã đơn hàng: {oder.code}</p>
              </Col>
              <Col span={12}>
                <p style={{color: "#72849a"}}>
                  Thời gian:{" "}
                  {moment(oder.createdAt).format("L")}
                </p>
              </Col>
            </Row>
            <Divider/>

            <h6>Sản phẩm</h6>
            <Row gutter={16}>
              {oder.products.map((item) => {
                return (
                  <Col span={8}>
                    <div className="d-flex">
                      <img height="70px" src={item.product.img}/>
                      <div style={{marginLeft: "10px"}}>
                        <div className="oder-name">{item.product.name} </div>
                        <span style={{color: "red"}}>
                          {Intl.NumberFormat().format(item.product.price)} đ
                        </span>
                      </div>
                    </div>
                  </Col>
                );
              })}
            </Row>

            <div className="mt-2">Tổng tiền: {Intl.NumberFormat().format(oder.totalPrice)} đ</div>
            <Divider/>
            <div className="d-flex">
              <div>Trạng thái đơn hàng:</div>
              <Select
                style={{marginLeft: "10px"}}
                defaultValue={oder.status}
                onChange={(value) => handleChange(oder, value)}
              >
                <Option value={"pending"}>Chờ giao hàng</Option>
                <Option value={"active"}>Đã giao</Option>
                <Option value={"refuse"}>Từ chối</Option>
              </Select>
            </div>
          </div>
        )}
      </Drawer>
      <p>Bộ lọc</p>
      <Row>
        <Col span={12}>
          <div>
            <Space
              direction="vertical"
              size={12}
              style={{marginBottom: "40px"}}
            >
              <RangePicker onChange={onChangeDate}/>
            </Space>
          </div>
        </Col>
        <Col span={12}>
          <div className="d-flex justify-content-between">
            <div style={{marginBottom: "20px"}} className="d-flex">
              <Input
                placeholder="Nhập mã đơn hàng"
                onChange={(e) => {
                  setSearchCode(e.target.value);
                }}
              />
              <Button
                style={{
                  marginLeft: "10px",
                }}
                onClick={onSearch}
              >
                Tìm kiếm
              </Button>
            </div>
            <Select defaultValue="Tất cả" style={{width: 120}} onChange={handleSelect}>
              <Option value="all">Tất cả</Option>
              <Option value="true">Đã giao</Option>
              <Option value="false">
                Chờ giao hàng
              </Option>
              <Option value="-createdAt">
                Mới nhất
              </Option>
              <Option value="createdAt">
                Cũ nhất
              </Option>
            </Select>
          </div>
        </Col>
      </Row>

      <div>
        <div>
          <div className="font-bold">Tìm thấy: {orders.length}</div>
          <Table loading={loading} dataSource={orders} pagination={false}>
            <Column
              title="Mã đơn hàng"
              render={(record) => {
                return (
                  <div style={{color: "#4ca100"}}>{record.code}</div>
                );
              }}
              key="code"
            />
            <Column
              title="Thời gian"
              render={(record) => {
                return (
                  <div>
                    {moment(record.createdAt).format(
                      "L"
                    )}
                  </div>
                );
              }}
              key="status"
            />

            <Column
              title="Sản phẩm"
              render={(record) => {
                return (
                  <div className="mb-4">
                    {record.products.map((item) => {
                      return (
                        <div className="d-flex justify-content-between mt-2">
                          <img height="70px" src={item.product.img}/>
                          <div style={{width: "100px"}}>
                                  <span className="oder-name">
                                    {item.product.name}
                                  </span>
                            <span>{Intl.NumberFormat().format(item.product.price)} đ</span>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              }}
              key="products"
            />
            <Column
              title="Người đặt hàng"
              render={(record) => {
                return (
                  <div>
                    <span>Tên: {record.user.fullName}</span>
                    <div>Địa chỉ: {record.adress}</div>
                    <div>Phone: {record.user.phone}</div>
                  </div>
                );
              }}
              key="user"
            />
            <Column
              title="Trạng thái"
              render={(record) => {
                return (
                  <div>
                    {record.status === "pending" ? <div>Chờ giao hàng</div> : record.status === "active" ?
                      <div style={{color: "#00ee6f"}}>Đã giao</div> :
                      <div style={{color: "#fd0404"}}>Từ chối</div>}
                  </div>
                );
              }}
              key="status"
            />
            <Column
              title="Action"
              render={(record) => {
                return (
                  <div className="d-flex">
                    <Button onClick={() => showDrawer(record)}>
                      <EyeOutlined/>
                    </Button>
                  </div>
                );
              }}
              key="ship"
            />
          </Table>
          <Pagination onChange={handleChangePagination} className="mt-2" size="small"
                      total={parseInt(`${totalPage}0`)}/>
        </div>

      </div>
    </div>
  );
};

export default OrderShop;
