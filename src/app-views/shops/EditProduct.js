import React, { useEffect, useState } from "react";

import {
  Menu,
  DatePicker,
  Row,
  Col,
  Input,
  Button,
  Upload,
  message, Form, Card, Select, Checkbox, notification,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { Option } from "antd/es/mentions";
import { getShop } from "../../service/ApiService/shopApi";
import { useSelector } from "react-redux";
import { forEach } from "react-bootstrap/ElementChildren";
import { getCategories } from "../../service/ApiService/categoryApi";
import storage from "../../firebase";
import { updateProduct, read, getProduct } from "../../service/ApiService/productsApi";
import { useParams } from "react-router-dom";

const { TextArea } = Input;
const rules = {
  validateName: [
    {
      required: true,
      message: 'Vui lòng nhập tên sản phẩm'
    }
  ],
  validatePrice: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        console.log(parseInt(value));
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Giá phải là số');
      }
    })
  ],
  validateDesc: [
    {
      required: true,
      message: 'Vui lòng nhập mô tả sản phẩm'
    }
  ],
  validateQuantity: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        let newValue = parseInt(value)
        if (!value || newValue || newValue <0) {
          return Promise.resolve();
        }
        return Promise.reject('Số lượng phải là số');
      }
    })
  ]
}
const EditProductShop = () => {
  const [sale, setSale] = useState(false);
  const [shop, setShop] = useState({});
  const [category, setCategory] = useState([]);
  const [dataCategory, setDataCategory] = useState([]);
  const [selectedFile, setSelectedFile] = useState();
  const [products, setProducts] = useState({});
  const user = useSelector(state => state.user)
  const { id } = useParams()
  const [form] = Form.useForm();
  let categories = []

  function onChange(e) {
    setSale(!sale)
  }

  console.log(sale)

  useEffect(() => {
    getDataCate().then(() => {
    })
    getProducts()

  }, [id]);

  const getProducts = async () => {
    try {
      const { data } = await getProduct(id);
      setProducts(data)
      form.setFieldsValue(data)
    } catch (error) {
    }
  };

  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const { data } = await getShop(user.id);
        setShop(data);
        setCategory(data.categories)
        resolve(data.categories);
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }


  const handleSubmit = (values) => {
    console.log(values)
    const ref = storage.ref(`images/${selectedFile.name}`);
    const upload = ref.put(selectedFile);
    upload.on(
      "state_changed",
      snapshot => {
      },
      error => {
        console.log(error);
      }, () => {
        storage.ref('images')
          .child(selectedFile.name)
          .getDownloadURL()
          .then(async (url) => {
            console.log(url)
            const { data } = await updateProduct(id, {
              sale: sale,
              shop: shop.id,
              img: url,
              ...values
            });
            if (data) {
              notification.success({ message: data.message });
            }
          })
      }
    )
  }
  const handChangeImg = (e) => {
    console.log(e.target.files[0])
    setSelectedFile(e.target.files[0])
  }
  const getDataCate = async () => {
    const data = await getDataShop();
    for (let item of data) {
      const { data } = await getCategories(item);
      categories.push(data);
    }
    setDataCategory(categories)
  }


  return (

    <Card style={{ backgroundColor: "#fff", borderRadius: "10px", paddingLeft: "20px" }}>
      <Form
        form={form}
        method="m"
        name="basic"
        layout="vertical"
        onFinish={handleSubmit}
        requiredMark={false}
        initialValues={{ remember: true }}
        autoComplete="off"
      >
        <h4 className="p-3">CẬP NHẬT SẢN PHẨM</h4>
        <Row gutter={16}>
          <Col span={12}>
            <Card>
              <Form.Item
                label="Tên sản phẩm"
                name="name"
                rules={rules.validateName}
              >
                <Input placeholder="Mời nhập" />
              </Form.Item>

              <Form.Item label="Ảnh sp">
                <input type="file" onChange={handChangeImg} />
              </Form.Item>

              <Form.Item
                label="Giá"
                name="price"
                rules={rules.validatePrice}
              >
                <Input placeholder="Mời nhập" />
              </Form.Item>

              <Form.Item
                label="Số lượng"
                name="quantity"
                rules={rules.validateQuantity}
              >
                <Input placeholder="Mời nhập" />
              </Form.Item>

              <Form.Item
                label="Mô tả"
                name="description"
                rules={rules.validateDesc}
              >
                <TextArea rows={4} />
              </Form.Item>

              <Form.Item>
                <Button
                  htmlType="submit"
                  style={{ backgroundColor: "#FD3082", color: "#fff" }}
                >
                  Cập nhật
                </Button>
              </Form.Item>
            </Card>
          </Col>

        </Row>

      </Form>
    </Card>
  );
};

export default EditProductShop;
