import React, {useEffect, useState} from "react"
import {Card, Tabs, Row, Col, Input, Button, Form, notification, Rate, Upload} from "antd"
import {storage} from "../../../firebase";
import {getShop, updateShop} from "../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {LinkOutlined, CameraOutlined, UploadOutlined} from "@ant-design/icons";
import axios from "axios";

const {TabPane} = Tabs;
const {Meta} = Card;
const {TextArea} = Input;

function callback(key) {
  console.log(key);
}

const ProfileShop = () => {
  const [shop, setShop] = useState({});
  const [selectedFile, setSelectedFile] = useState();
  const user = useSelector(state => state.user);
  const [avatar, setAvatar] = useState(user?.avatar)
  const [onUpload, setOnUpLoad] = useState(false)
  const [form] = Form.useForm();

  useEffect(() => {
    getDataShop()
  }, [])

  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        resolve(data);
        form.setFieldsValue(data)
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }

  const handChangeImg = (e) => {
    setSelectedFile(e.target.files[0])
  }
  const handleSubmit = (values) => {
    console.log(values)
    const ref = storage.ref(`images/${selectedFile.name}`);
    const upload = ref.put(selectedFile);
    upload.on(
      "state_changed",
      snapshot => {
      },
      error => {
        console.log(error);
      }, () => {
        storage.ref('images')
          .child(selectedFile.name)
          .getDownloadURL()
          .then(async (url) => {
            console.log(url)
            const {data} = await updateShop(shop.id, {
              ...values,
              avatar: url
            });
            if (data) {
              notification.success({message: data.message});
            }
          })
      }
    )
  }
  

  return (
    <>
      <div className="banner banners" style={{position:"relative"}}>
        <div className="image">
          <img width="100%" height="248"
               src={`https://1.bp.blogspot.com/-WFHKnAAsI3g/WArRBhVNAfI/AAAAAAAAGWo/omG3yNtlQHoTCLepWLQu70k2CexKDeMrgCLcB/s1600/Facebook%2BCover%2BBackgrounds%2B4.jpg`}/>
          
          <div style={{position:"absolute",bottom:"80px",right:"0px"}}>
            <Upload showUploadList={false} beforeUpload={(file, fileList) => {
              const data = new FormData();
              data.append("file", file);
              data.append("user", JSON.stringify(user));
              if (file) {
                let access_token = JSON.parse(localStorage.getItem("ACCESS_TOKEN"));
                axios.post('/api/users/update-self-profile', data,
                  {
                    headers:
                      {
                        'Content-Type': 'multipart/form-data',
                        "Authorization": `Bearer ${access_token.token}`
                      }
                  }
                ).then(res => {
                  let url = URL.createObjectURL(file);
                  setAvatar(url)
                  setOnUpLoad(true)
                  notification.success({
                    message: res.data.message
                  })
                }).catch(err => {
                  console.log(err)
                })
              }
              return false;
            }}
            >
              <Button icon={<CameraOutlined/>}>Cập nhật ảnh bìa</Button>
            </Upload>
          </div>
        </div>
    
        <div className="logo">
          <Card
            className="d-flex"
            bordered={false}
            cover={
              <img
                style={{width: "100px"}}
                alt="example"
                src={shop.avatar}
              />
            }
          >
            <div>
              <div>{shop.name}</div>
              <Rate
                style={{fontSize: "10px"}}
                disabled
                defaultValue={4}
              />
            </div>
      
          </Card>
          <div className="d-flex">
            <div>
              <img width="24" height="24"
                   src="https://salt.tikicdn.com/ts/upload/80/e0/ef/fff5ef4b4a6afe9e1ff8ce3f22dd045a.png"/>
            </div>
            <div style={{padding: "0px 20px"}}>Ngày mai trước 11h00</div>
            <div>Mã giảm giá</div>
          </div>
          <div className="d-flex" style={{marginTop: "16px"}}>
            <div>
              <img width="24" height="24"
                   src="https://salt.tikicdn.com/ts/upload/59/06/35/9b9cbfe33ebd70fb8eede4c073d426db.png"/>
            </div>
            <div style={{padding: "0px 20px"}}>Mã giảm giá</div>
          </div>
        </div>
        <div className="link1">
          <div className="link2">
            <LinkOutlined width="15" height="15"/>
          </div>
        </div>
      </div>
      <Card style={{backgroundColor: "#fff", borderRadius: "10px", paddingLeft: "20px",position:"relative"}}>
        <div>
          <h2>Hồ sơ shop</h2>
          <p>Xem tình trạng shop và cập nhật thông tin của bạn</p>
        </div>

        <div>
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="Thông tin cơ bản" key="1">
              <Form
                style={{width: "100%"}}
                form={form}
                method="m"
                name="basic"
                layout="vertical"
                onFinish={handleSubmit}
                requiredMark={false}
                initialValues={{remember: true}}
                autoComplete="off"
              >
                <Row gutter={16}>
                  <Col className="gutter-row" span={6}>
                    <div>
                      <Card
                        hoverable
                        cover={<img style={{backgroundColor: "#E0E0E0"}} alt="example"
                                    src={shop.avatar}/>}
                      >
                        <p>Giao diện shop trên máy tính</p>
                        <p>Sản phẩm</p>
                      </Card>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={18}>
                    <Form.Item style={{marginBottom: "40px"}} label="Tên shop" name="name">
                      <Input placeholder="Mời nhập"/>
                    </Form.Item>

                    <Form.Item label="Ảnh đại diện shop">
                      <input type="file" onChange={handChangeImg}/>
                    </Form.Item>

                    <Form.Item label="Mô tả" name="description">
                      <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                      >
                        Cập nhật
                      </Button>
                    </Form.Item>
                  </Col>

                </Row>
              </Form>
            </TabPane>
          </Tabs>
        </div>
      </Card>
    </>
  )
}

export default ProfileShop;