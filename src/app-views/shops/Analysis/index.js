import {Card} from "antd";
import ChartWidget from "../../../components/common-component/ChartWidget";
import {VisitorChartData} from './shopData'
import {useEffect, useState} from "react";
import {getShop} from "../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {getShopOrder, getShopOrderSuccess} from "../../../service/ApiService/oderUserApi";

const Analysis = () => {
  const [visitorChartData] = useState(VisitorChartData);
  const [order, setOrder] = useState([]);
  const user = useSelector(state => state.user);

  useEffect(()=>{
    getOrderList()
  },[])

  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        resolve(data);
      }));

    } catch (err) {
      window.location.replace('/')
    }
  }

  const getOrderList = async () => {
    const myShop = await getDataShop();
    const {data} = await getShopOrderSuccess(myShop.id,1,10000);
    console.log(data)
  }

  return (
    <Card title="Phân tích bán hàng">
      <ChartWidget
        title="Biểu đồ thống kê"
        series={visitorChartData.series}
        xAxis={visitorChartData.categories}
        height={'400px'}
        // direction={direction}
      />
    </Card>
  )
}

export default Analysis