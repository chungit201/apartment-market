import {Card, Col, Row} from "antd";
import Parameter from "./ Parameter";
import Analysis from "./Analysis";

const ShopPage = () =>{
  return (
    <div>
      <Row>
        <Col span={24}>
          <Analysis/>
        </Col>
      </Row>
    </div>
  )
}

export default ShopPage