import {Card, Col, Row} from "antd";

const Parameter = () =>{
  return (
    <Card title="Danh sách cần làm">
      <Row >
        <Col span={6}>
          <div className="text-center"  style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}}>0</p>
            <div>Chờ xác nhận</div>
          </div>
        </Col>
        <Col span={6}>
          <div className="text-center"  style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}}>0</p>
            <div>Chờ lấy hàng</div>
          </div>
        </Col>
        <Col span={6}>
          <div className="text-center"  style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}}>0</p>
            <div>Đã lấy hàng</div>
          </div>
        </Col>
        <Col span={6}>
          <div className="text-center" style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}} >0</p>
            <div>Đơn huỷ</div>
          </div>
        </Col>

      </Row>
      <Row style={{marginTop:"20px"}}>
        <Col span={6}>
          <div className="text-center" style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}} >0</p>
            <div>Trả hàng/ chờ xử lý</div>
          </div>
        </Col>

        <Col span={6}>
          <div className="text-center" style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}} >0</p>
            <div>Sản phẩm bị tạm khoá</div>
          </div>
        </Col>

        <Col span={6}>
          <div className="text-center" style={{fontSize:"13px"}}>
            <p style={{color:"#4c6de5"}} >0</p>
            <div>Sản phẩm hết hàng</div>
          </div>
        </Col>
      </Row>
    </Card>
  )
}

export default Parameter