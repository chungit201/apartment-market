import {Button, DatePicker, Empty, Input, Menu, Space, Table, Tabs} from "antd";
import React from "react";

const PortalOrder = () =>{
  const {TabPane} = Tabs;

  function callback(key) {
    console.log(key);
  }

  const {RangePicker} = DatePicker;
  const {Column} = Table;

  const menu1 = (
    <Menu>
      <Menu.Item key="0">
        <a href="/order-shop">Tất cả</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="#">Đơn hủy</a>
      </Menu.Item>
      <Menu.Divider/>
      <Menu.Item key="3">Trả hàng / Hoàn tiền</Menu.Item>
    </Menu>
  );

  const menu2 = (
    <Menu>
      <Menu.Item key="0">
        <a href="/list-shop">Tất cả sản phẩm</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="/addproduct-shop">Thêm sản phẩm</a>
      </Menu.Item>
    </Menu>
  );

  const data = [
    {
      key: '1',
      name: 'Thịt bò',
      img: 'https://cdn.tgdd.vn/2021/01/content/bo%CC%80vai-800x500.jpg',
      price: 150000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '2',
      name: 'Thịt ba chỉ',
      img: 'https://cf.shopee.vn/file/36290b67dd4d8c23998871f8e8c5b35f',
      price: 111000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '3',
      name: 'Rau cải',
      img: 'https://img.dantocmiennui.vn/t620/uploaddtmn//2017/6/19/17c7253a5639582e41ea8f9be4d6dd5b-1.jpg',
      price: 5000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '4',
      name: 'Chân giò',
      img: 'https://img.dantocmiennui.vn/t620/uploaddtmn//2017/6/19/17c7253a5639582e41ea8f9be4d6dd5b-1.jpg',
      price: 100000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '5',
      name: 'Súp lơ',
      img: 'https://img.dantocmiennui.vn/t620/uploaddtmn//2017/6/19/17c7253a5639582e41ea8f9be4d6dd5b-1.jpg',
      price: 10000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '6',
      name: 'Nấm',
      img: 'https://img.dantocmiennui.vn/t620/uploaddtmn//2017/6/19/17c7253a5639582e41ea8f9be4d6dd5b-1.jpg',
      price: 20000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    }

  ];

  const oderData = [
    {
      key: '1',
      name: 'Thịt bò',
      img: 'https://cdn.tgdd.vn/2021/01/content/bo%CC%80vai-800x500.jpg',
      price: 150000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '2',
      name: 'Thịt ba chỉ',
      img: 'https://cf.shopee.vn/file/36290b67dd4d8c23998871f8e8c5b35f',
      price: 111000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    },
    {
      key: '3',
      name: 'Rau cải',
      img: 'https://img.dantocmiennui.vn/t620/uploaddtmn//2017/6/19/17c7253a5639582e41ea8f9be4d6dd5b-1.jpg',
      price: 5000,
      ship: 'Ngay trong ngày',
      status: 'Chờ lấy hàng'
    }
  ];

   return (
     <Tabs
       defaultActiveKey="1"
       onChange={callback}
       style={{backgroundColor: "#fff", borderRadius: "10px"}}
       className="p-3"
     >
       <TabPane tab="Tất cả" key="1">
         <div>
           <p style={{marginLeft: "600px"}}>Ngày đặt hàng</p>
           <Space direction="vertical" size={12} style={{marginLeft: "600px", marginBottom: "40px"}}>
             <RangePicker/>
           </Space>
         </div>

         <div style={{marginBottom: "20px"}}>
           <Input placeholder="Nhập mã đơn hàng" style={{width: 750}}/>
           <Button
             style={{
               backgroundColor: "#FD3082",
               color: "#fff",
               marginLeft: "10px",
             }}
           >
             Tìm kiếm
           </Button>
         </div>
         <div>

         </div>
         <div>
           {oderData.length > 0 ? (
             <Empty/>
           ) : (
             <>
               <h6>6 đơn hàng</h6>
               <Table dataSource={oderData}>
                 <Column title="Sản phẩm" dataIndex="name" key="name"/>
                 <Column title="Tổng đơn hàng" dataIndex="price" key="price"/>
                 <Column title="Trạng thái" dataIndex="status" key="status"/>
                 <Column title="Vận chuyển" dataIndex="ship" key="ship"/>
               </Table>
             </>
           )}

         </div>
       </TabPane>
       <TabPane tab="Chờ xác nhận" key="2">
         <div>
           <p style={{marginLeft: "600px"}}>Ngày đặt hàng</p>
           <Space direction="vertical" size={12} style={{marginLeft: "600px", marginBottom: "40px"}}>
             <RangePicker/>
           </Space>
         </div>

         <div style={{marginBottom: "20px"}}>
           <Input placeholder="Nhập mã đơn hàng" style={{width: 750}}/>
           <Button
             style={{
               backgroundColor: "#FD3082",
               color: "#fff",
               marginLeft: "10px",
             }}
           >
             Tìm kiếm
           </Button>
         </div>
         <div>
           <h2>1 đơn hàng</h2>
         </div>
         <div>
           <Table dataSource={oderData}>
             <Column title="Sản phẩm" dataIndex="name" key="name"/>
             <Column title="Tổng đơn hàng" dataIndex="price" key="price"/>
             <Column title="Trạng thái" dataIndex="status" key="status"/>
             <Column title="Vận chuyển" dataIndex="ship" key="ship"/>
           </Table>

         </div>
       </TabPane>
 
       <TabPane tab="Đang giao" key="4">
         <div>
           <p style={{marginLeft: "600px"}}>Ngày đặt hàng</p>
           <Space direction="vertical" size={12} style={{marginLeft: "600px", marginBottom: "40px"}}>
             <RangePicker/>
           </Space>
         </div>

         <div style={{marginBottom: "20px"}}>
           <Input placeholder="Nhập mã đơn hàng" style={{width: 750}}/>
           <Button
             style={{
               backgroundColor: "#FD3082",
               color: "#fff",
               marginLeft: "10px",
             }}
           >
             Tìm kiếm
           </Button>
         </div>
         <div>
           <h2>3 đơn hàng</h2>
         </div>
         <div>
           <Table dataSource={oderData}>
             <Column title="Sản phẩm" dataIndex="name" key="name"/>
             <Column title="Tổng đơn hàng" dataIndex="price" key="price"/>
             <Column title="Trạng thái" dataIndex="status" key="status"/>
             <Column title="Vận chuyển" dataIndex="ship" key="ship"/>
           </Table>

         </div>
       </TabPane>
       <TabPane tab="Đã giao" key="5">
         <div>
           <p style={{marginLeft: "600px"}}>Ngày đặt hàng</p>
           <Space direction="vertical" size={12} style={{marginLeft: "600px", marginBottom: "40px"}}>
             <RangePicker/>
           </Space>
         </div>

         <div style={{marginBottom: "20px"}}>
           <Input placeholder="Nhập mã đơn hàng" style={{width: 750}}/>
           <Button
             style={{
               backgroundColor: "#FD3082",
               color: "#fff",
               marginLeft: "10px",
             }}
           >
             Tìm kiếm
           </Button>
         </div>
         <div>
           <h2>4 đơn hàng</h2>
         </div>
         <div>
           <Table dataSource={oderData}>
             <Column title="Sản phẩm" dataIndex="name" key="name"/>
             <Column title="Tổng đơn hàng" dataIndex="price" key="price"/>
             <Column title="Trạng thái" dataIndex="status" key="status"/>
             <Column title="Vận chuyển" dataIndex="ship" key="ship"/>
           </Table>

         </div>
       </TabPane>
   
     </Tabs>
   )
}

export default PortalOrder