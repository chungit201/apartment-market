import React, { useState } from 'react';
import {Input, Table, Row, Col, Modal, Button, Tabs, Select, Form} from 'antd';

const columns = [
    {
        title: 'Sản phẩm',
        dataIndex: 'name',
    },
    {
        title: 'Đơn giá',
        dataIndex: 'age',
    },
    {
        title: 'Số lượng',
        dataIndex: 'address',
    },
    {
        title: 'Thành tiền',
        dataIndex: 'sum',
    },
];
const data = [
    {
        key: '1',
        name: 'Bắp cải',
        age: 20000,
        address: 1,
        sum: 20000,
    },
    {
        key: '2',
        name: 'Bắp cải',
        age: 20000,
        address: 1,
        sum: 20000,
    },
    {
        key: '3',
        name: 'Bắp cải',
        age: 20000,
        address: 1,
        sum: 20000,
    },
];

const { TabPane } = Tabs;

const {Option, OptGroup} = Select;

function callback(key) {
}

const Checkout = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (

        <div className="container">
            <div style={{ backgroundColor: "#fff", borderRadius: "10px" }}>
                <div style={{ padding: "20px"}}>
                    <div>
                        <h5 style={{ color: '#FD3082'}}>Địa chỉ nhận hàng</h5>
                    </div>
                    <div>
                        <p>Đào Mạnh Cường (+84) 917454310
                            89, ngõ 70 văn trì, Phường Minh Khai, Quận Bắc Từ Liêm, Hà Nội</p>
                    </div>
                </div>

            </div>

            <div style={{ backgroundColor: "#fff", borderRadius: "10px", marginTop:"20px" }}>
                <div>
                    <Table columns={columns} dataSource={data} size="middle" />
                </div>

                <div>
                    <Row>
                        <Col span={12}>
                            <div className="d-flex" style={{ marginLeft: "10px"}}>
                                <div><span>Lời nhắn</span></div>
                                <Input style={{ width: "40%", marginBottom:"30px", marginLeft:"10px" }} placeholder="Lưu ý cho người bán" />
                            </div>
                        </Col>
                        <Col span={12}><div className="d-flex"><span>Tổng số tiền:</span> <span style={{marginLeft: "5px" ,color:"red"}}>60000đ</span></div></Col>
                    </Row>
                </div>


            </div>

            <div style={{ backgroundColor: "#fff", borderRadius: "10px", marginTop:"20px"  }}>
                <div style={{ marginLeft: "10px", padding:"10px"}}>
                    <div>
                        <h5 style={{ color: '#FD3082'}}>
                            Phương thức thanh toán
                        </h5>
                        <Tabs defaultActiveKey="1" onChange={callback}>
                            <TabPane tab="Thẻ ATM nội địa" key="1">
                                <Form.Item label="Ngân hàng" name="bankCode">
                                    <Select defaultValue="Không chọn">
                                        <OptGroup label="Manager">
                                            <Option value='VNPAYQR'>Ngân hàng VNPAYQR</Option>
                                            <Option value='NCB'>Ngân hàng NCB</Option>
                                            <Option value='SCB'>Ngân hàng SCB</Option>
                                            <Option value='SACOMBANK'>Ngân hàng SACOMBANK</Option>
                                            <Option value='EXIMBANK'>Ngân hàng EXIMBANK</Option>
                                            <Option value='MSBANK'>Ngân hàng MSBANK</Option>
                                            <Option value='NAMABANK'>Ngân hàng NAMABANK</Option>
                                            <Option VISA='VISA'>Ngân hàng VISA</Option>
                                            <Option value='VNMART'>Ngân hàng VNMART</Option>
                                            <Option value='VIETINBANK'>Ngân hàng VIETINBANK</Option>
                                            <Option value='VIETCOMBANK'>Ngân hàng VIETCOMBANK</Option>
                                            <Option value='HDBANK'>Ngân hàng HDBANK</Option>
                                            <Option value='DONGABANK'>Ngân hàng Dong A</Option>
                                            <Option value='TPBANK'>Ngân hàng Tp Bank</Option>
                                            <Option value='OJB'>Ngân hàng OceanBank</Option>
                                            <Option value='BIDV'>Ngân hàng BIDV</Option>
                                            <Option value='TECHCOMBANK'>Ngân hàng Techcombank</Option>
                                            <Option value='VPBANK'>Ngân hàng VPBank</Option>
                                            <Option value='AGRIBANK'>Ngân hàng AGRIBANK</Option>
                                            <Option value='MBBANK'>Ngân hàng MBBank</Option>
                                            <Option value='ACB'>Ngân hàng ACB</Option>
                                            <Option value='OCB'>Ngân hàng OCB</Option>
                                            <Option value='SHB'>Ngân hàng SHB</Option>
                                            <Option value='IVB'>Ngân hàng IVB</Option>
                                        </OptGroup>
                                    </Select>
                                </Form.Item>
                                <hr/>
                                <div>
                                    <h5 style={{ color: '#FD3082'}}>Tổng thanh toán: </h5>
                                    <p>Số tiền phải trả</p>
                                    <h4>60000đ</h4>
                                </div>
                                <hr/>
                                <div>
                                    <Button style={{ backgroundColor: "#FD3082", color: "#fff" }} onClick={showModal}>
                                        Mua hàng
                                    </Button>
                                    <Modal title="Bạn xác nhận mua hàng" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>

                                    </Modal>
                                </div>
                            </TabPane>
                            <TabPane tab="Thanh toán khi nhận hàng" key="2">
                                <span>Thanh toán khi nhận hàng</span>
                                <hr/>
                                <div>
                                    <h5 style={{ color: '#FD3082'}}>Tổng thanh toán: </h5>
                                    <p>Số tiền phải trả</p>
                                    <h4>60000đ</h4>
                                </div>
                                <hr/>
                                <div>
                                    <Button style={{ backgroundColor: "#FD3082", color: "#fff" }} onClick={showModal}>
                                        Mua hàng
                                    </Button>
                                    <Modal title="Bạn xác nhận mua hàng" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>

                                    </Modal>
                                </div>
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Checkout;