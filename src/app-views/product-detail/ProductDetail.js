import React, {createElement, useEffect, useState} from 'react'
import {
  Row,
  Col,
  InputNumber,
  Space,
  Button,
  Card,
  Comment,
  Avatar,
  Radio,
  Form,
  Tooltip, notification, Image, Divider
} from 'antd';
import './productDetail.css';
import {useNavigate, useParams} from "react-router-dom";
import {getCateProducts, getProduct} from "../../service/ApiService/productsApi";
import TextArea from "antd/es/input/TextArea";
import {getCommentsProduct} from "../../service/ApiService/commentApi";
import {DislikeOutlined, LikeOutlined, DislikeFilled, LikeFilled} from '@ant-design/icons';
import {useSelector} from "react-redux";
import {addToCart} from "../../service/ApiService/cartApi";
import ProductPage from "../products/ProductPage";
import ReactMarkdown from "react-markdown";
import moment from 'moment';

const Editor = ({onChange, onSubmit, submitting, value}) => (
  <>
    <Form.Item>
      <TextArea rows={4} onChange={onChange} value={value}/>
    </Form.Item>
    <Form.Item>
      <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
        Add Comment
      </Button>
    </Form.Item>
  </>
);


const ProductDetail = () => {
  const [product, setProduct] = useState(null);
  const [productCategory, setProductCategory] = useState([]);
  const [comments, setComments] = useState([]);
  const [submitting, setSubmitting] = useState(false);
  const [value, setValue] = useState('');
  const [color, setColor] = useState(null);
  const user = useSelector(state => state.user)
  const {id} = useParams();
  const navigate = useNavigate()

  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);

  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction('liked');
  };

  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction('disliked');
  };

  const actions = [
    <Tooltip key="comment-basic-like" title="Like">
      <span onClick={like}>
        {createElement(action === 'liked' ? LikeFilled : LikeOutlined)}
        <span className="comment-action">{likes}</span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title="Dislike">
      <span onClick={dislike}>
        {React.createElement(action === 'disliked' ? DislikeFilled : DislikeOutlined)}
        <span className="comment-action">{dislikes}</span>
      </span>
    </Tooltip>,
    <span key="comment-basic-reply-to">Reply to</span>,
  ];


  const handleChange = (e) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    getProductsCate();
    getDataComment()
  }, [id]);

  const getDataComment = async () => {
    const {data} = await getCommentsProduct(id);
    setComments(data.results)
  }


  const getDataProduct = async () => {
    try {
      return new Promise(async (resolve) => {
        await getProduct(id).then(res => {
          resolve(res.data);
          setProduct(res.data);
        }).catch(err => {
          if (err) {
            window.location.replace('/')
          }
        })
      })
    } catch (err) {
      window.location.replace('/')
    }
  }
  const getProductsCate = async () => {
    const product = await getDataProduct();
    const {data} = await getCateProducts({categories: product.categories, page: 1, limit: 12});
    setProductCategory(data.results)
  }


  const handleAddCart = async () => {
   if(user.id){
     const {data} = await addToCart({
       user: user.id,
       shop: product.shop,
       product: product.id,
       color: color ? color : ""
     });
     navigate('/cart')
     notification.success({message: "Thêm vào giỏ hàng thành công"})
   }else {
     navigate("/login")
   }
  }

  const handleColorChange = (e) => {
    setColor(e.target.value)
  }

  return (
    <div className="container">
      <h4 style={{color: "rgb(253, 48, 130)"}}>Chi tiết sản phẩm</h4>
      {product !== null &&
        <Card>
          <Row gutter={16}>
            <div className="container">
              <Col xxl={24} xl={24} xs={24} sm={24} style={{padding: "20px"}}>
                <Row>
                  <Col span={11}>
                    <img style={{maxWidth: "100%", width: "300px"}} src={product.img}/>
                    <div className="d-flex mt-2 justify-content-center" style={{width:"300px"}}>
                      {product.imgDetail.map(item => {
                        return (
                          <Image
                            width={70}
                            src={item}
                          />
                        )
                      })}
                    </div>
                  </Col>
                  <Col span={13}>
                    <div>
                      <h5>{product.name}</h5>
                      <h4 style={{color: "rgb(253, 48, 130)"}}>{Intl.NumberFormat().format(product.price)} đ</h4>
                      <Space>
                        <p>Số lượng:</p>
                        <input type="number" defaultValue={1} min={1} max={product.quantity}/>
                      </Space>
                      <div>
                        <div>
                          {product.color.length > 0 ? (
                            <div className="mt-3">
                              <p>Màu sắc:</p>
                              <Radio.Group style={{marginLeft: "10px"}} onChange={handleColorChange}>
                                {product.color.map(item => {
                                  return (
                                    <Radio.Button value={item}>{item}</Radio.Button>
                                  )
                                })}
                              </Radio.Group>
                            </div>
                          ) : (null)
                          }
                        </div>

                        <div>
                          {product.size.length > 0 ? (
                            <div className="mt-3" >
                              <p>Size: </p>
                              <Radio.Group style={{marginLeft: "10px"}} onChange={handleColorChange}>
                                {product.size.map(item => {
                                  return (
                                    <Radio.Button value={item}>{item}</Radio.Button>
                                  )
                                })}
                              </Radio.Group>
                            </div>
                          ) : (null)

                          }
                        </div>
                      </div>
                      <div style={{margin: "20px 0"}}>
                        <Button onClick={handleAddCart} style={{marginRight: "20px"}} danger>Thêm vào giỏ hàng</Button>
                        <Button onClick={handleAddCart} type="primary" danger>
                          Mua ngay
                        </Button>
                      </div>
                    </div>
                  </Col>

                </Row>

              </Col>
            </div>
          </Row>
          <Divider/>
          <div className="mt-2">
            <div>
              <div><h5>Mô tả sản phẩm</h5></div>
              <div style={{maxWidth: "100%"}} className={"product-description"}>
                <ReactMarkdown style={{width: "100%"}}>{product.description}</ReactMarkdown>
              </div>
            </div>
          </div>
        </Card>
      }
      {comments.length > 0 ? (
        <Card style={{marginTop: "20px"}} title="Nhận xét về sản phẩm">
          <div>
            {comments && comments.map(item => {
              {
              }
              return (
                <Comment key={item.id}
                         actions={actions}
                         author={<a>{item.author.fullName}</a>}
                         avatar={<Avatar src={item.author.avatar} alt={item.author.fullName}/>}
                         content={
                           <p>
                             {item.content}
                           </p>
                         }
                         datetime={
                           <Tooltip title={moment().format('YYYY-MM-DD HH:mm:ss')}>
                             <span>{moment().fromNow()}</span>
                           </Tooltip>
                         }
                />
              )
            })}
          </div>
        </Card>
      ) : (null)}

      <h4 style={{color: "rgb(253, 48, 130)", marginTop: "20px"}}>Sản phẩm liên quan</h4>
      <Row className="container" gutter={16}
           style={{backgroundColor: "#fff", borderRadius: "5px", marginTop: "20px"}}>
        {productCategory && productCategory.map(item => {
          return (
            <Col key={item.id} xxl={4} xl={4} xs={12} sm={6}>
              <ProductPage product={item}/>
            </Col>
          )
        })}
      </Row>
    </div>

  )
}

export default ProductDetail