import {Button, Card, Tabs} from "antd";
import VoucherList from "./voucher/VoucherList";
import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {getShop} from "../../service/ApiService/shopApi";
import {getVouchers} from "../../service/ApiService/voucherApi";
const { TabPane } = Tabs;

const VoucherPage = () => {
  
  const [shop,setShop] = useState({});
  const [voucher,setVoucher] = useState([]);
  const [loading,setLoading] = useState(false)
  const user = useSelector(state=>state.user)
  useEffect(()=>{
    getDataVoucher().then(()=>{})
  },[])
  
  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        resolve(data);
      }));
      
    } catch (err) {
      window.location.replace('/')
    }
  }
  
  const getDataVoucher = async () =>{
    setLoading(true)
    const myShop = await getDataShop();
    try {
      const {data} = await getVouchers(myShop.id)
      console.log(data.results)
      setVoucher(data.results);
      setLoading(false)
    }catch (err){
    
    }
  }
  
  function callback(key) {
    console.log(key);
  }
  return (
    <Card title="Danh sách mã giảm giá">
      <div className="dlex">
        <span>Tạo Mã giảm giá toàn shop hoặc Mã giảm giá sản phẩm ngay bây giờ để thu hút người mua.</span>
        <Link to='/shop/add-voucher'><Button type="primary" className="float-end">+Tạo</Button></Link>
      </div>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Tất cả" key="1">
          <VoucherList voucher={voucher} loading={loading}/>
        </TabPane>
        <TabPane tab="Đăng diễn ra" key="2">
          {/*<VoucherList/>*/}
        </TabPane>
        <TabPane tab="Sắp diễn ra" key="3">
          {/*<VoucherList/>*/}
        </TabPane>
        <TabPane tab="Kết thúc" key="4">
          {/*<VoucherList/>*/}
        </TabPane>
      </Tabs>
    </Card>
  )
}
export default VoucherPage