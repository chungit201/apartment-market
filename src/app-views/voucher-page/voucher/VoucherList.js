import {Table} from "antd";
import {useEffect, useState} from "react";
import {getShop} from "../../../service/ApiService/shopApi";
import {getVouchers} from "../../../service/ApiService/voucherApi";
import {useSelector} from "react-redux";

const VoucherList = ({voucher,loading}) =>{
  console.log(voucher)
  const columns = [
    {
      title: 'Mã vocher | name',
      render: (record) =>{
        return (
          <div className="d-flex">
            <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAADOxJREFUaAWNmMmPXVcRxs/rwW13t6e4HTsDOAqQkEEJQ4IQOxZZILFAbFkFlkQoi/DPMEiss0FICFYRAoRIEARBkIijkJhMJLbTbfdku90D3++rqvPuex0Sznv3nnOq6vuq6kz3vjf62atXDy6ubbfWDtpoRhX1SNUo6gP3D3q/oWv7aSOx+9IbT52yxGMbmDF/cZcfsNN+y8a69DG0J9byff/CqTb36uqNdC5r+KgOIiArwoVBlks1mpmxzUiAsjTpgcCH8BLIajQj6+Qdybja5QNZlUM6FGDwhv/CwyfYv26ttZnghjgSqL4tPJREJiKCdJ+2LrVtay5skmPfyrQPO2MTY47kCvxITBrejh9i8MmgqfJVMYAhMfzCCAOk2YlAKymJJTcJINpaJZMJ4CR0hQ09DsGOdW6DL5lqPvguH44lfdnOHDIo/zQTD8Z460ZtTqokCgXTyjJwKLamFWTYuukKmQqx9KYC18dYlkp4SzN0FN0JJnu5ZsOmQ9RgvyYvSi9N9hsFimhJESDPiCcmlbH+ZFgjlxl7NkBjpytTdZ82Msu1C6tNXSO4z5Ib9I2XD2RjPK2QlR+0wAKPLnzVciw8ez8DywA4ZNKBdRmAHeAkLwfbl1oEULIIJmwrGTvueMVDXkTY/UcC07IxPvQgCjO0nZs8IWRse7zQjD5QLzn19zU1zGbcJCdR+pSq1Sj7kkVfIyvvPqHSNvB0nFXo6LkbMjjog2NmpvHE46UVEUAWs1OZTvcn5DkbQ2ycMCQrqp5BBQV3zJx5B3g8214R4WN/oLMtKyRj8+DSDidGotOMqJJdzQzZmlgKx0NvSu/RtdNuEbZwDQ6K3PbmK7Z05grQBEPhCaD7t/tuF7GyKgiq8D4bEIDMGqXZw9Bp0dcV4LF9t0ssLCafxqe8nEdUMfoRZnCWr847mEFi5GPdYIagpvTN7mQygDqxGFFfJdeU18xBaMdF3usYiHDovMbPH+NxOx0QDgYydeuUtP9canCaF1tkJKSL9lwfHUKOeYtplzGoWka4R9SLGREQRNxrxMvMA1ebM2JNwwJQh4KlZ3wnoyG18TR05XJCzrvYMKC+R0J44I1qGx5ImAJW8RoXNqZXTqfendKTEzdAtzNH5tuzD19w98evv93euXHDfA6u8HCqneMRfoToQ2T/zJanIHJR8iM/ryRLfD+1GBiuWi6xJslmMOWmx0XK6Nd6hdO+Qj+nAM4dW3AS3FYW5vViFzr6nggaFJZH+i++SAV+bHUb+Ip24IA7medeuKz0yJD+QYvBoU9WXPriXzrL1IrXfUU9oZPFaL89cnqpfe3cifappYU2G0DALjs6V9/eutFe29xqf1271ugHFz7CX8Ux4dOrI2J0HNkftkfPvXBFDPoSaBk4wEjCcpx0PTFlAINEjs629p3PnW33nziGwSeWm3t77fdXVttLq2ttz+9QGah8OYkcxPAP3UA+aJetX+NjmhVVTT2pRezjKdfw9+kXup9s5Kvru58/938nQVhHZ2fbU+fPtsdPnghfTG9e9uNl6vi7vvsnFkh0K1ludgJjYSlr1X3zMeIJoOodEib6+Lavnj/e7tFSGpZLGzfbK2ubbeP2Xjs+P9MuLB9tD51aavP6UXa4hH/LxWn/dNS2T6KlMPxdVk1mitd4zwJoh4y1kwoCw23oU4ttwWzo40MBiJx8YWUxDPP+5vrN9pOL7/elCvWfrq63hdmZ9tgdy+3LZ5T44tG0JjixJReNvrkddfp0MgSNy6gjFmLgOUJNHqatIANsheW62VN2ZB/JhPzOY/Nl5Xprd0/m9jiWayZu6Znw5w/XdV1vj+pQ+MbdZ0VE4GHmwAiSTwTkIK2tPh38V9Ay1EODdy29YNPR2YltkIUxCJ/XbEbPxoDXgdI/aHsZCD4oD59ebE+sLLe/XN2AwTY8oAISEf3j2nZ7c+OtdmphzoGPkxkEKd5KDl6vAtUeRC9RZ+QY51BGdlkbIWfOOGQmGIuwsJ74cfTu5k777KlaKnrvkexb951pT91zqr2+fqO9of3y+vp2u35718kEvrWt3f22vbdDhJMBO2Nn4djwQfp9FuiRQ/pHPhe/BzJbNnCWmDk8hABjjw5nPzLfRKaR/sN/tiYSKY6l+dn2+JllX/wOuXh9u714eb29sal/bga8Ze9avI4Cf6VQg3jwGa8sqbE8YtYRgrAufiViDEPIIODymBiT8oHNP1dvtd++uwHofxZmiVPr6Qfuat974O52xzzbs/yOa/vT4JQujvnUd//EpI77uNT2oO9pUqMSiHUZCfhUyw1pW4GGdeF/fWm9Pf/aalvf2YP5Y8t9Ooq//9C97ZGTS8E1xc/PYnjDj6hoa+CGfgne+09Y9BoWsg1jms5UXZIx0lMtiIzj+aIGBbmYbUdf4pev3Giv6A+/J+5cbI/pSL5w/Ij3C+rpwvPk2xfOtss3d9qVW7eNdyiaDWKL3/g4FRKXlmVM0cllhk7r5dlfXZMZAXGPuumdCbztZ1BL3l9fygaDakfNMTh+V2ttaX7UPnNqQU/8hfbQ6WPtxBGf9gB7uaT98tPX3uv8k3EkP37K/6CNrUdYsonfIxG9lDEkNDyltdQMzNFhNlj32OTXs4O6ytbuQfv7Vc3Sh9vtl5fW2hPnlts3P326zZJtlnsWF+R2anZLqTpmnYYu+7NQNxW7103f/Ms0pEGoNrHlFdayHPRZr2VL+5EzR5veGWWT++cQfkbPmlF76YPN9scPJg8FltjK0SPGgvcfD4fwwVvLntiIIGKk481OWhKymXR5NtSPUDWfFRwytZHbHmfZfvL8YvvBl1bavcvzmumxDcmOBwDu1q5/xGGgF4E+cMaYNyIY4+Eac9t39cXr49cJCFyBOWMCVWOcvQTEUkcjXRLPcn5pvj3zxZX29KN3tPtPapNL7sFJZ/As67nylTuXC+J6Y2e3rbHZ8R8uohYu8JhFe1j3BLETzk92TL0WPRKSamd541ffHgSAUuv0gPnXOq8tAr7Kg6cXGteO3lve2thRkLtaVgd6A57VQ3PBL45lS/3i5XiNcTRw4wviqjHCMZVk4T9qhxeq+F+LdhyjDjUAANNoqC8yHOELp5s7g6lJzJHZkQNvbfL1fkDZeEv+3XvrXjH4Z+G4wK0G/YrB8anjASZhNKWUnV5RBNDoeiQAiiTex5I42UwAdfcmY+hE9vyrm+1vl2+1r19Y1HGrjfsJBR8vX9lqv3hzVb8OGYvwD5eajqco7E4KYq5khhk6LoHigVgMJqqk6AidMupKpupIyop2cfW2rmvt3uOz7cEzR7xPLpyc70tpV3vr8vbt9m8ttxff34yHIFFTRNG51KhBtQ4Vdk6GJaWmuAzteJ4jKuRBiZHJ1/mcpcwlyMLMjmOM8C/H+jgk3d7Z2NXb8G77zWi7rSzOtB8+qd8cKj965cP2lt6S4w+Mcpg+Oz6SgLOCB+s8eyBIiDk4SpzPEY5ZtJgM3nPcl6lqgGDrIgUfzUBIBlufUImnazx62oVPWz+qs2188HlgDvkprrAPXqcrXuqZNnrm51t2X4/6mDIxibdGL2QOp78qTOrCXu4CIyx8MfO1DMhq3D6kAzPET9gO+T+ae6af1eFHcAqjo4oUnWaNnLo8R2zkaP3PJKMS9lQhD2xySFaj6HqKN2TY6pv+1IuScUzipbId9jHT47dfYAyTFQon34dY/3wnTgx42HBpA7SXJC6d13LHu2HT+Gktf+Q9UeTRmzkUQqgwkxFbTrMTYFEVPl8aHWsErOACExRl6YREGQdCBhTDlK5cWU8Injke7xSZ21TE1mVg9gCVPk7YceGffj6bAKBnJaSeWGxjbrVV+xXFFn2qCCLB9hTLgkAcBrLgDLtaClnXUo3gwkUtF+PtHA6CCy77R07A4u6+BrLwniODL+Mxxod/igW4SGxAsOUkiUvvkcVhJms5o5VBVG07xHmRjIOcGqSPw8feI+jgOYwnsYhlziHoRq354q5SteSgvdZS3G3DytPsJNLO+PFtvGSQBa+TlMdYQklvofRj1ybp/sdoDwhKb1FmRF+fWm4hqFEj2LzQ1ZKKAGJkY+nIrmam8OoXtuz9szW5wxfrmufNYFTAS1pL0zOY3N1/zjyj3v2bI19RwmFsGjuC07S62VeMnqctdQ5CdHwn8Lmhx3jmDHxwIacjWgFV53/IFufNgRsT5PDXKWggYuN1Uxvq2OyWMtKIwsqJ5ugiI3DMfDRSu0MjL9uoTVeyCTwB5+gaV7bGhr/wARpfU3hkQ3zxJR57zW8EHw6CwMkInI9xCZHjMGzj52i0w3aMq0BjyMY2zHcN1BAfox/BVxLmTF/BHzzFXTW6ws88ele8GzEWBcJwvFYjSAOcUMCxNmHKCovdUG4u2RzCJ670Ha8EPgpvfenEFoMbMTxwYrH9F3TfbEPDytIDAAAAAElFTkSuQmCC"}/>
            <div>
              <span style={{marginLeft:"10px"}}>{record.code}</span>
              <div style={{marginLeft:"10px"}}>{record.name}</div>
            </div>
          </div>
        )
      }
    },
    {
      title: 'Loại mã',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: 'Giảm giá',
      dataIndex: 'priceSale',
      key: 'priceSale',
    },
    {
      title: 'Tổng số mã giảm giá có thể sử dụng',
      dataIndex: 'usages',
      key: 'usages',
    },
    {
      title: 'Đã dùng',
      dataIndex: 'used',
      key: 'used',
    },
    {
      title: 'Thời gian lưu Mã giảm giá',
      render: (record) =>{
        return (
          <div className="d-flex">
            <div>{record.start} - {record.end}</div>
          </div>
        )
      }
    },
    {
      title: 'Thao tác',
      dataIndex: 'action',
      key: 'action',
    },
    
  ];
  return (
    <Table loading={loading} dataSource={voucher} columns={columns} />
  )
}

export default VoucherList