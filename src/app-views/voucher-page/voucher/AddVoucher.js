import {Tabs, Radio, Card, Form, Input, Row, Col, DatePicker, Button, notification} from 'antd';
import {useState} from "react";
import {addVoucher} from "../../../service/ApiService/voucherApi";
import {getShop} from "../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import moment from 'moment'

const {RangePicker} = DatePicker;
const {TabPane} = Tabs;
const rules = {
  validateNameVoucher: [
    {
      required: true,
      message: 'Vui lòng nhập tên chương trình giảm giá'
    }
  ],
  validateTypeVoucher: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Yêu cầu nhập số');
      }
    })
  ],
  validateMinValue: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        console.log(parseInt(value));
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Yêu cầu nhập số');
      }
    })
  ],
  validateMaxValue: [
    {
      required: true,
      message: 'Không được để trống !'
    },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        console.log(parseInt(value));
        let newValue = parseInt(value)
        if (!value || newValue) {
          return Promise.resolve();
        }
        return Promise.reject('Yêu cầu nhập số');
      }
    })
  ]
}

const AddVoucher = () => {
  const [size, setSize] = useState('small');
  const [shop, setShop] = useState({});
  const [start, setStart] = useState("");
  const [end, setEnd] = useState("");
  const [code, setCode] = useState("");
  const [key, setKey] = useState("shop");
  const [voucher,setVoucher] = useState({});
  const user = useSelector(state => state.user)
  
  const onChange = (e) => {
    setSize(e.target.value);
  };
  
  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        setShop(data);
        resolve(data);
      }));
      
    } catch (err) {
      window.location.replace('/')
    }
    
  }
  
  function makeid(length) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    for (let i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
  }
  
  
  const handleSubmit = async (values) => {
    console.log(values)
    const myShop = await getDataShop();
    const {data} = await addVoucher({
      start: start,
      shop: myShop.id,
      end: end,
      type: key,
      ...values
    }).then(res => {
      if (res) {
        notification.success({message: res.data.message})
      }
    })
  }
  
  function onChangeDate(dates, dateStrings) {
    console.log(dateStrings)
    setStart(dateStrings[0]);
    setEnd(dateStrings[1])
  }
  
  const [form] = Form.useForm();
  form.setFieldsValue({
    code: makeid(8)
  })
  
  const handleChangTab = (key) => {
    setKey(key)
    form.setFieldsValue({
      code: makeid(8)
    })
  }
  
  console.log(code.toUpperCase())
  
  return (
    <div className="container mt-4">
      <h5>Tạo mã giảm giá mới</h5>
      <Form form={form} onFinish={handleSubmit}>
        
        <Card>
          
          <Row>
            <Col span={12}>
              <h6>Thông tin cơ bản</h6>
              <Tabs className="mt-2" defaultActiveKey="1" type="card" onChange={handleChangTab} size={size}>
                <TabPane tab="Voucher shop" key="shop">
                  <Form.Item
                   label="Tên trương trình giảm giá"
                    name="name"
                    rules={rules.validateNameVoucher}
                    >
                    <Input/>
                  </Form.Item>
                  <Form.Item
                   label="Mã voucher"
                    name="code"
                    >
                    <Input/>
                  </Form.Item>
                  
                  <RangePicker
                    ranges={{
                      Today: [moment(), moment()],
                      'This Month': [moment().startOf('month'), moment().endOf('month')],
                    }}
                    onChange={onChangeDate}
                  />
                  
                  
                  <h6 className="mt-4">Thiết lập mã giảm giá</h6>
                  
                  <Form.Item 
                  label="Loại giảm giá | Mức giảm" 
                  name="priceSale"
                  rules={rules.validateTypeVoucher}
                  >
                    <Input type="number"/>
                  </Form.Item>
                  
                  <Form.Item 
                  label="Giá trị đơn hàng tối thiểu" 
                  name="minimum"
                  rules={rules.validateMinValue}
                  >
                    <Input type="number"/>
                  </Form.Item>
                  
                  <Form.Item 
                  label="Lượt sử dụng tối đa" 
                  name="usages"
                  rules={rules.validateMaxValue}
                  
                  >
                    <Input type="number"/>
                  </Form.Item>
                  
                  <div>
                    Sản phẩm được áp dụng : Tất cả sản phẩm
                  </div>
                
                </TabPane>
              </Tabs>
            </Col>
            
            <Col span={12}>
              <div className="d-flex justify-content-center">
                <img
                  src={"https://deo.shopeemobile.com/shopee/shopee-seller-live-sg/rootpages/static/modules/vouchers/image/multilang_voucher_illustration_vn.29df4f1.png"}/>
              
              </div>
              <div className="text-center">
                <span>Lưu ý mỗi mã giảm giá chỉ đc sử dụng 1 lần</span>
              </div>
            </Col>
          </Row>
        </Card>
        <div className="d-flex justify-content-end">
          <Button className="mt-2">Hủy</Button>
          <Button style={{marginLeft: "10px"}} htmlType="submit" className="mt-2" type="primary">Xác nhận</Button>
        </div>
      </Form>
    
    </div>
  )
}

export default AddVoucher