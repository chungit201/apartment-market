import React, {useEffect, useState} from "react";
import {
  Card, Row, Col, Button, List, InputNumber, Space, notification, Tabs, Modal, Form, Input, Checkbox,
} from "antd";
import {ExclamationCircleOutlined, TagOutlined} from "@ant-design/icons";
import {deleteCarts, getCarts} from "../../service/ApiService/cartApi";
import {useSelector} from "react-redux";
import {addMyOder} from "../../service/ApiService/oderUserApi";
import {CloseOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router-dom";
import {sendToOne} from "../../service/ApiService/pushNotificationApi";
import {getShop, getShopId} from "../../service/ApiService/shopApi";
import {addVoucherUser, getVoucher, updateVoucher} from "../../service/ApiService/voucherApi";
import {addSingle} from "../../service/ApiService/singleApi";
import {addNotification} from "../../service/ApiService/notificationApi";

const {confirm} = Modal
const {TabPane} = Tabs;

const Cart = () => {
  const [carts, setCarts] = useState([]);
  const user = useSelector((state) => state.user);
  const [key, setKey] = useState(1);
  const [currenPrice, setCurrenPrice] = useState(0);
  const [productCarts, setProductCarts] = useState([]);
  const [voucher, setVoucher] = useState({});
  const [useVoucher, setUseVoucher] = useState(false);
  const [code, setCode] = useState("");
  const [receiver,setReceiver] = useState(user.fullName)
  const publicUrl = new URL(process.env.PUBLIC_URL, window.location);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [address, setAddress] = useState(user.address)
  let total = 0;
  const [form] = Form.useForm();
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    setIsModalVisible(false);
    await getVoucher({
      code: code, currenPrice: currenPrice, user: user.id
    })
      .then(async (res) => {
        setVoucher(res.data);
        setUseVoucher(true);
        if (res.status === 200) {
          if (res.data.minimum < currenPrice) {
            notification.success({message: "Áp dụng thành công"});
            setCurrenPrice(currenPrice - res.data.priceSale);
          } else {
            notification.error({
              message: `Đơn hàng tối thiểu ${res.data.minimum} đ`,
            });
          }
        }
      })
      .catch((err) => {
        console.log(err.response.data.message);
        notification.error({message: err.response.data.message});
      });
  };
  console.log("cccccc", carts)
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const navigate = useNavigate();

  useEffect(() => {
    filterProductShop();
  }, []);

  const getDataCart = async () => {
    try {
      return new Promise(async (resolve) => {
        const {data} = await getCarts(user.id);
        setCarts(data.results);
        data.results.map((item) => {
          total += item.totalPrice;
          setCurrenPrice(total);
        });
        resolve(data.results);
        console.log("oooooooo", user)
        form.setFieldsValue({adress: user.address,receiver:receiver})
      });
    } catch (err) {
      console.log(err);
    }
  };

  function randomString(len, an) {
    an = an && an.toLowerCase();
    let str = "", i = 0, min = an == "a" ? 10 : 0, max = an == "n" ? 10 : 62;
    for (; i++ < len;) {
      let r = (Math.random() * (max - min) + min) << 0;
      str += String.fromCharCode((r += r > 9 ? (r < 36 ? 55 : 61) : 48)).toUpperCase();
    }
    return str;
  }

  let listShopId = [];
  let productShopInCarts = [];
  const filterProductShop = async () => {
    const dataCarts = await getDataCart();
    dataCarts.forEach((value) => {
      const shopId = value.shop.id;
      if (!listShopId.includes(shopId)) {
        listShopId.push(shopId);
      }
    });
    console.log(listShopId);
    listShopId.forEach((shopId) => {
      let productShop = {
        shop: shopId, data: dataCarts.filter((item) => {
          if (item.shop.id == shopId) {
            return item;
          }
        }),
      };
      productShopInCarts.push(productShop);
      setProductCarts(productShopInCarts);
    });
  };

  const handleRemove = async (id) => {
    try {
      const {data} = await deleteCarts(id);
      if (data) {
        const newCart = carts.filter((x) => x.id !== data.cartDelete.id);
        console.log(newCart);
        setCarts(newCart);
        notification.success({message: data.message});
      }
    } catch (err) {
    }
  };

  function callback(value) {
    console.log(value);
    setKey(value);
  }

  function showConfirm() {
    confirm({
      title: 'Xác nhận thông tin',
      icon: <ExclamationCircleOutlined/>,
      content: <div>
          <div>Người nhận: {receiver}</div>
          <div>Địa chỉ: {address}</div>
        <div>Tổng tiền: {Intl.NumberFormat().format(currenPrice)} đ</div>
      </div>,
      onOk() {
        handleSubmit().then();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
  }

  console.log("nnnn", voucher);
  const handleSubmit = async () => {
    if (key == 1) {
      if (useVoucher) {
        await addVoucherUser({
          user: user.id,
          voucher: voucher.id,
          code: voucher.code,
          shop: voucher.shop,
        })
        await updateVoucher(voucher.id, {
          usages: voucher.usages - 1,
          used: voucher.used + 1
        });
      }
      const {data} = await addSingle({
        user: user.id, total: currenPrice, single: carts
      });
      console.log(data)

      navigate(`/payment?amount=${currenPrice}&description=${data.single.id}&mode=product`);
    }
    if (key == 2) {
      try {
        if (productCarts.length > 0) {

          productCarts.map(async (item) => {
            console.log("iiiiii", item);
            await addMyOder({
              user: user.id,
              code: randomString(10),
              products: item.data,
              shop: item.shop,
              totalPrice: currenPrice,
              adress: address
            })
            if(useVoucher) {
              await addVoucherUser({
                user: user.id,
                voucher: voucher.id,
                code: voucher.code,
                shop: voucher.shop,
              })
            }
            await getShopId(item.shop).then(async (res) => {
              console.log("rrrrrrrrrr", res);
              await sendToOne({
                title: "Apartment market",
                body: `Bạn có một đơn hàng mới`,
                click_action: `${publicUrl.origin}/shop/order-shop`,
                icon: "https://scontent.xx.fbcdn.net/v/t1.15752-9/275844082_348809860504907_4731915775493283911_n.png?stp=dst-png_s206x206&_nc_cat=104&ccb=1-5&_nc_sid=aee45a&_nc_ohc=5m_Oy5AzJm4AX_pnt2Y&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJ6bYu7ruyeyVePaM28kH9ia-Jg0-LJb2dE4P1-FWKFAw&oe=625728F2",
                to: res.data.deviceToken,
              });

              await addNotification({
                sender: user.id,
                title: "Đơn hàng mới ",
                shop: item.shop,
                img: "https://e-invoiced.vn/data/news/2165/formICON-300x300.png",
                actionClick: "/shop/order-shop",
                content: `${user.fullName} bạn đã đặt đơn hàng mới`
              })
            });
          });
          if (useVoucher) {
            await updateVoucher(voucher.id, {
              usages: voucher.usages - 1,
            });
          }
        }
        await notification.success({message: "Đặt hàng thành công"});


        // navigate("/my-order");
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleChangeAddress = (e) => {
    setAddress(e.target.value)
  }

  return (<div className="container">
    <Modal
      title="Chọn voucher"
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Input
        onChange={(e) => {
          setCode(e.target.value);
        }}
      />
    </Modal>
    <Card title="Giỏ hàng">
      <Row gutter={16}>
        <div className="container">
          <Col xxl={24} xl={24} xs={24} sm={24} style={{padding: "20px"}}>
            <Row>
              <Col span={24} style={{borderBottom: "1px solid #ccc"}}>
                <div style={{maxHeight: "500px", overflowY: "scroll"}}>
                  <List
                    itemLayout="vertical"
                    size="large"
                    dataSource={carts}
                    renderItem={(item) => (<div>
                      <List.Item
                        style={{position: "relative"}}
                        key={item.id}
                        extra={<img
                          width={100}
                          alt="logo"
                          src={item.product?.img}
                        />}
                      >
                        <List.Item.Meta
                          title={<div>
                            <div>
                              <h5 style={{fontSize: "14px"}}>
                                {" "}
                                {item.product?.name}
                              </h5>
                              <div style={{fontSize: "13px"}}>Cửa hàng: {item.shop.name}</div>
                              <div
                                style={{
                                  fontSize: "13px", color: "red",
                                }}
                              >
                                giá: {item.totalPrice} đ
                              </div>
                            </div>
                            {/* <span
                              style={{fontSize: "13px"}}>Shop : {item.shop.name}</span> */}
                          </div>}
                        />

                        <Space>
                          Số lượng:
                          <InputNumber
                            style={{width: "50px"}}
                            size="large"
                            min={1}
                            max={100000}
                            defaultValue={item.quantity}
                          />
                        </Space>
                        <div
                          onClick={() => handleRemove(item.id)}
                          style={{
                            position: "absolute", top: "10px", right: "0", cursor: "pointer",
                          }}
                        >
                          <CloseOutlined/>
                        </div>
                      </List.Item>
                    </div>)}
                  />
                </div>
              </Col>

              <div style={{display: carts.length > 0 ? "block" : "none"}}>
                <Col span={24}>
                  <div className="d-flex mt-4">
                    <TagOutlined style={{marginTop: "3px"}}/>
                    <span style={{marginLeft: "10px"}}>
                        Apartment Voucher
                      </span>
                    <span
                      onClick={showModal}
                      style={{
                        marginLeft: "20px", color: "#ff7875", cursor: "pointer",
                      }}
                    >
                        Áp dụng mã giảm giá
                      </span>
                  </div>

                  <div className="voucher-list mt-2">
                    {useVoucher ? (<div>
                      <div>Voucher:</div>
                      <div className="d-flex mt-2">
                        <img
                          src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAADOxJREFUaAWNmMmPXVcRxs/rwW13t6e4HTsDOAqQkEEJQ4IQOxZZILFAbFkFlkQoi/DPMEiss0FICFYRAoRIEARBkIijkJhMJLbTbfdku90D3++rqvPuex0Sznv3nnOq6vuq6kz3vjf62atXDy6ubbfWDtpoRhX1SNUo6gP3D3q/oWv7aSOx+9IbT52yxGMbmDF/cZcfsNN+y8a69DG0J9byff/CqTb36uqNdC5r+KgOIiArwoVBlks1mpmxzUiAsjTpgcCH8BLIajQj6+Qdybja5QNZlUM6FGDwhv/CwyfYv26ttZnghjgSqL4tPJREJiKCdJ+2LrVtay5skmPfyrQPO2MTY47kCvxITBrejh9i8MmgqfJVMYAhMfzCCAOk2YlAKymJJTcJINpaJZMJ4CR0hQ09DsGOdW6DL5lqPvguH44lfdnOHDIo/zQTD8Z460ZtTqokCgXTyjJwKLamFWTYuukKmQqx9KYC18dYlkp4SzN0FN0JJnu5ZsOmQ9RgvyYvSi9N9hsFimhJESDPiCcmlbH+ZFgjlxl7NkBjpytTdZ82Msu1C6tNXSO4z5Ib9I2XD2RjPK2QlR+0wAKPLnzVciw8ez8DywA4ZNKBdRmAHeAkLwfbl1oEULIIJmwrGTvueMVDXkTY/UcC07IxPvQgCjO0nZs8IWRse7zQjD5QLzn19zU1zGbcJCdR+pSq1Sj7kkVfIyvvPqHSNvB0nFXo6LkbMjjog2NmpvHE46UVEUAWs1OZTvcn5DkbQ2ycMCQrqp5BBQV3zJx5B3g8214R4WN/oLMtKyRj8+DSDidGotOMqJJdzQzZmlgKx0NvSu/RtdNuEbZwDQ6K3PbmK7Z05grQBEPhCaD7t/tuF7GyKgiq8D4bEIDMGqXZw9Bp0dcV4LF9t0ssLCafxqe8nEdUMfoRZnCWr847mEFi5GPdYIagpvTN7mQygDqxGFFfJdeU18xBaMdF3usYiHDovMbPH+NxOx0QDgYydeuUtP9canCaF1tkJKSL9lwfHUKOeYtplzGoWka4R9SLGREQRNxrxMvMA1ebM2JNwwJQh4KlZ3wnoyG18TR05XJCzrvYMKC+R0J44I1qGx5ImAJW8RoXNqZXTqfendKTEzdAtzNH5tuzD19w98evv93euXHDfA6u8HCqneMRfoToQ2T/zJanIHJR8iM/ryRLfD+1GBiuWi6xJslmMOWmx0XK6Nd6hdO+Qj+nAM4dW3AS3FYW5vViFzr6nggaFJZH+i++SAV+bHUb+Ip24IA7medeuKz0yJD+QYvBoU9WXPriXzrL1IrXfUU9oZPFaL89cnqpfe3cifappYU2G0DALjs6V9/eutFe29xqf1271ugHFz7CX8Ux4dOrI2J0HNkftkfPvXBFDPoSaBk4wEjCcpx0PTFlAINEjs629p3PnW33nziGwSeWm3t77fdXVttLq2ttz+9QGah8OYkcxPAP3UA+aJetX+NjmhVVTT2pRezjKdfw9+kXup9s5Kvru58/938nQVhHZ2fbU+fPtsdPnghfTG9e9uNl6vi7vvsnFkh0K1ludgJjYSlr1X3zMeIJoOodEib6+Lavnj/e7tFSGpZLGzfbK2ubbeP2Xjs+P9MuLB9tD51aavP6UXa4hH/LxWn/dNS2T6KlMPxdVk1mitd4zwJoh4y1kwoCw23oU4ttwWzo40MBiJx8YWUxDPP+5vrN9pOL7/elCvWfrq63hdmZ9tgdy+3LZ5T44tG0JjixJReNvrkddfp0MgSNy6gjFmLgOUJNHqatIANsheW62VN2ZB/JhPzOY/Nl5Xprd0/m9jiWayZu6Znw5w/XdV1vj+pQ+MbdZ0VE4GHmwAiSTwTkIK2tPh38V9Ay1EODdy29YNPR2YltkIUxCJ/XbEbPxoDXgdI/aHsZCD4oD59ebE+sLLe/XN2AwTY8oAISEf3j2nZ7c+OtdmphzoGPkxkEKd5KDl6vAtUeRC9RZ+QY51BGdlkbIWfOOGQmGIuwsJ74cfTu5k777KlaKnrvkexb951pT91zqr2+fqO9of3y+vp2u35718kEvrWt3f22vbdDhJMBO2Nn4djwQfp9FuiRQ/pHPhe/BzJbNnCWmDk8hABjjw5nPzLfRKaR/sN/tiYSKY6l+dn2+JllX/wOuXh9u714eb29sal/bga8Ze9avI4Cf6VQg3jwGa8sqbE8YtYRgrAufiViDEPIIODymBiT8oHNP1dvtd++uwHofxZmiVPr6Qfuat974O52xzzbs/yOa/vT4JQujvnUd//EpI77uNT2oO9pUqMSiHUZCfhUyw1pW4GGdeF/fWm9Pf/aalvf2YP5Y8t9Ooq//9C97ZGTS8E1xc/PYnjDj6hoa+CGfgne+09Y9BoWsg1jms5UXZIx0lMtiIzj+aIGBbmYbUdf4pev3Giv6A+/J+5cbI/pSL5w/Ij3C+rpwvPk2xfOtss3d9qVW7eNdyiaDWKL3/g4FRKXlmVM0cllhk7r5dlfXZMZAXGPuumdCbztZ1BL3l9fygaDakfNMTh+V2ttaX7UPnNqQU/8hfbQ6WPtxBGf9gB7uaT98tPX3uv8k3EkP37K/6CNrUdYsonfIxG9lDEkNDyltdQMzNFhNlj32OTXs4O6ytbuQfv7Vc3Sh9vtl5fW2hPnlts3P326zZJtlnsWF+R2anZLqTpmnYYu+7NQNxW7103f/Ms0pEGoNrHlFdayHPRZr2VL+5EzR5veGWWT++cQfkbPmlF76YPN9scPJg8FltjK0SPGgvcfD4fwwVvLntiIIGKk481OWhKymXR5NtSPUDWfFRwytZHbHmfZfvL8YvvBl1bavcvzmumxDcmOBwDu1q5/xGGgF4E+cMaYNyIY4+Eac9t39cXr49cJCFyBOWMCVWOcvQTEUkcjXRLPcn5pvj3zxZX29KN3tPtPapNL7sFJZ/As67nylTuXC+J6Y2e3rbHZ8R8uohYu8JhFe1j3BLETzk92TL0WPRKSamd541ffHgSAUuv0gPnXOq8tAr7Kg6cXGteO3lve2thRkLtaVgd6A57VQ3PBL45lS/3i5XiNcTRw4wviqjHCMZVk4T9qhxeq+F+LdhyjDjUAANNoqC8yHOELp5s7g6lJzJHZkQNvbfL1fkDZeEv+3XvrXjH4Z+G4wK0G/YrB8anjASZhNKWUnV5RBNDoeiQAiiTex5I42UwAdfcmY+hE9vyrm+1vl2+1r19Y1HGrjfsJBR8vX9lqv3hzVb8OGYvwD5eajqco7E4KYq5khhk6LoHigVgMJqqk6AidMupKpupIyop2cfW2rmvt3uOz7cEzR7xPLpyc70tpV3vr8vbt9m8ttxff34yHIFFTRNG51KhBtQ4Vdk6GJaWmuAzteJ4jKuRBiZHJ1/mcpcwlyMLMjmOM8C/H+jgk3d7Z2NXb8G77zWi7rSzOtB8+qd8cKj965cP2lt6S4w+Mcpg+Oz6SgLOCB+s8eyBIiDk4SpzPEY5ZtJgM3nPcl6lqgGDrIgUfzUBIBlufUImnazx62oVPWz+qs2188HlgDvkprrAPXqcrXuqZNnrm51t2X4/6mDIxibdGL2QOp78qTOrCXu4CIyx8MfO1DMhq3D6kAzPET9gO+T+ae6af1eFHcAqjo4oUnWaNnLo8R2zkaP3PJKMS9lQhD2xySFaj6HqKN2TY6pv+1IuScUzipbId9jHT47dfYAyTFQon34dY/3wnTgx42HBpA7SXJC6d13LHu2HT+Gktf+Q9UeTRmzkUQqgwkxFbTrMTYFEVPl8aHWsErOACExRl6YREGQdCBhTDlK5cWU8Injke7xSZ21TE1mVg9gCVPk7YceGffj6bAKBnJaSeWGxjbrVV+xXFFn2qCCLB9hTLgkAcBrLgDLtaClnXUo3gwkUtF+PtHA6CCy77R07A4u6+BrLwniODL+Mxxod/igW4SGxAsOUkiUvvkcVhJms5o5VBVG07xHmRjIOcGqSPw8feI+jgOYwnsYhlziHoRq354q5SteSgvdZS3G3DytPsJNLO+PFtvGSQBa+TlMdYQklvofRj1ybp/sdoDwhKb1FmRF+fWm4hqFEj2LzQ1ZKKAGJkY+nIrmam8OoXtuz9szW5wxfrmufNYFTAS1pL0zOY3N1/zjyj3v2bI19RwmFsGjuC07S62VeMnqctdQ5CdHwn8Lmhx3jmDHxwIacjWgFV53/IFufNgRsT5PDXKWggYuN1Uxvq2OyWMtKIwsqJ5ugiI3DMfDRSu0MjL9uoTVeyCTwB5+gaV7bGhr/wARpfU3hkQ3zxJR57zW8EHw6CwMkInI9xCZHjMGzj52i0w3aMq0BjyMY2zHcN1BAfox/BVxLmTF/BHzzFXTW6ws88ele8GzEWBcJwvFYjSAOcUMCxNmHKCovdUG4u2RzCJ670Ha8EPgpvfenEFoMbMTxwYrH9F3TfbEPDytIDAAAAAElFTkSuQmCC"}
                        />
                        <div>
                              <span style={{marginLeft: "10px"}}>
                                {voucher.code}
                              </span>
                          <div style={{marginLeft: "10px"}}>
                            Giảm{" "}
                            {Intl.NumberFormat().format(voucher.priceSale)}{" "}
                            đ cho đơn hàng{" "}
                            {Intl.NumberFormat().format(voucher.minimum)}
                          </div>
                        </div>
                      </div>
                    </div>) : null}
                  </div>

                  <Row gutter={64}>
                    <Col span={12}>
                      <div className="payment" style={{padding: "20px"}}>
                        <h6 className="mt-2">Phương thức thanh toán</h6>

                        <Tabs onChange={callback} type="card">
                          <TabPane tab="Thanh toán vnpay" key="1">
                            <img
                              style={{maxWidth: "100%"}}
                              src="https://phongvu.vn/cong-nghe/wp-content/uploads/2020/08/Banner-cong-thanh-toan-VNPAYQR.png"
                            />
                          </TabPane>
                          <TabPane tab="Thanh toán khi nhận hàng" key="2">
                            <div>
                              {" "}
                              Thanh toán khi nhận hàng - Phí thu hộ: ₫0 VNĐ.
                              Ưu đãi về phí vận chuyển (nếu có) áp dụng cả với
                              phí thu hộ.
                            </div>
                          </TabPane>
                        </Tabs>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div style={{marginLeft: "40px"}}>
                        <div style={{marginBottom: "20px"}}>
                          <Form form={form} layout="vertical">
                            <Form.Item label={"Địa chỉ nhận hàng"} name="adress">
                              <Input placeholder="địa chỉ nhận hàng" onChange={handleChangeAddress}/>
                            </Form.Item>
                            <Form.Item label={"Người nhận"} name="receiver">
                              <Input placeholder="Người nhận ..." onChange={handleChangeAddress}/>
                            </Form.Item>
                          </Form>
                        </div>
                        <Card
                          style={{
                            width: "100%",
                          }}
                        >

                          <div style={{padding: "20px"}}>
                            <h6>Tóm tắt đơn hàng</h6>
                            <p>Số lượng sản phẩm: {carts?.length}</p>
                            <p>Giao hàng: miễn phí</p>
                            <p>
                              Tổng tiền:{" "}
                              <b>
                                {Intl.NumberFormat().format(currenPrice)} vnđ
                              </b>
                            </p>
                          </div>
                        </Card>
                        <Button
                          onClick={showConfirm}
                          type="primary"
                          danger
                          style={{
                            width: "250px", height: "50px", margin: "20px",
                          }}
                        >
                          Mua hàng
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </div>
            </Row>
          </Col>
        </div>
      </Row>
    </Card>
  </div>);
};
export default Cart;
