import {Button, Result} from "antd";
import {Link} from "react-router-dom";
import {getShop} from "../../service/ApiService/shopApi";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const ExpireShop = () => {
  const [shop,setShop] = useState(null);
  const user = useSelector(state=> state.user);
  useEffect(()=>{
    getDataShop().then(()=>{})
  },[])
  const getDataShop = async () => {
    try {
      return new Promise(async (resolve) => {
        await getShop(user.id).then(res => {
          resolve(res.data);
          setShop(res.data);
        }).catch(err => {
          if (err) {
            window.location.replace('/')
          }
        })
      })
    } catch (err) {
      window.location.replace('/')
    }
  }

  return (
    <div>
      <Result
        status="403"
        title="403"
        subTitle="Xin lỗi , Shop của bạn đã hết hạn vui lòng gia hạn để tiếp tục"
        extra={
       <div>
         {shop !== null &&  <Link to={`/payment?amount=${shop.package}&description=gia hạn shop&mode=shop`}><Button type="primary">Gia hạn ngay</Button></Link>}
       </div>
      }
      />
    </div>
  )
}

export default ExpireShop