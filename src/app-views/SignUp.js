import { Form,Button,Input,Typography,Radio,Select } from "antd";
const { Option } = Select;
const{Title} = Typography;

const SignUp = () => {
    
    return (
        
        <div className="formsignin">
{/* Usenamer */}
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 8,
                }}
                >
                <Title  style={{textAlign:'center',marginTop:"30px"}} level={1} >Đăng Ký Tài Khoản</Title>
                <Form.Item
                    label="Họ tên"
                    name="username"
                    rules={[
                    {
                        required: true,
                        message: 'Bạn chưa nhập họ tên',
                    },
                    ]}
                >
                    <Input />
                </Form.Item>
{/* Password */}
                <Form.Item
                    label="Mật khẩu"
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Bạn chưa nhập mật khẩu',
                    },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                
{/* Floor */}
                <Form.Item
                    name="select"
                    label="Tầng"
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: 'Bạn chưa chọn tầng nào',
                      },
                    ]}
                >
                    <Select  placeholder="Chọn tầng">
                        <Option value="floor1">Tầng 1</Option>
                        <Option value="floor2">Tầng 2</Option>
                        <Option value="floor3">Tầng 3</Option>
                        <Option value="floor4">Tầng 4</Option>
                        <Option value="floor5">Tầng 5</Option>
                        <Option value="floor6">Tầng 6</Option>
                        <Option value="floor7">Tầng 7</Option>
                        <Option value="floor8">Tầng 8</Option>
                        <Option value="floor9">Tầng 9</Option>
                        <Option value="floor10">Tầng 10</Option>
                    </Select>
                </Form.Item>
{/* Room */}
                <Form.Item 
                name="radio-group" 
                label="Số Phòng"
                rules={[
                    {
                      required: true,
                      message: 'Bạn chưa chọn phòng nào',
                    },
                  ]}
                >

                    <Radio.Group>
                    <Radio value="room1">Phòng 1</Radio>
                    <Radio value="room2">Phòng 2</Radio>
                    <Radio value="room3">Phòng 3</Radio>
                    <Radio value="room4">Phòng 4</Radio>
                    <Radio value="room5">Phòng 5</Radio>
                    <Radio value="room6">Phòng 6</Radio>
                    <Radio value="room7">Phòng 7</Radio>
                    <Radio value="room8">Phòng 8</Radio>
                    
                    </Radio.Group>
                </Form.Item>
{/* Phone */}
                <Form.Item
                    label="Số điện thoại"
                    name="phone"
                    type="number"
                    rules={[
                    {
                        required: true,
                        message: 'Bạn chưa nhập Số điện thoại',
                    },
                    ]}
                >
                    <Input />
                    
                </Form.Item>
{/* Email */}
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                    {
                        required: true,
                        message: 'Bạn chưa nhập Email',
                    },
                    ]}
                >
                    <Input />
                </Form.Item>
{/* SignUp */}
                <Form.Item
                    wrapperCol={{
                    offset: 8,
                    span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                    Đăng Ký
                    </Button>
                </Form.Item>
    </Form>
        
        </div>
    )
}

export default SignUp