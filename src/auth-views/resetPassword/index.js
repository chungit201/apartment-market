import {Avatar, Button, Card, Checkbox, Col, Form, Input, Modal, notification, Row} from "antd";
import {Link} from "react-router-dom";
import {LockOutlined, PhoneOutlined} from "@ant-design/icons";
import {useState} from "react";
import Timer from "../../components/common-component/Timer";
import {checkCodePhone, getUserPhone, register, resetPassword, verifyPhone} from "../../service/ApiService/userApi";

const backgroundStyle = {
  backgroundImage: "url(/img/img-18.jpg)",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

const rules = {
  password: [
    {
      required: true,
      message: 'Vui lòng nhập mật khẩu'
    }
  ],
  confirm: [
    {
      required: true,
      message: 'Please confirm your password!'
    },
    ({getFieldValue}) => ({
      validator(rule, value) {
        if (!value || getFieldValue('password') === value) {
          return Promise.resolve();
        }
        return Promise.reject('Mật khẩu không khớp');
      },
    })
  ]
}

const ResetPassword = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [showTime, setShowTime] = useState(false);
  const [loading, setLoading] = useState(false);
  const [numberPhone, setNumberPhone] = useState();
  const [user, setUser] = useState(null);
  const [userId, setUserId] = useState("");
  const [time, setTime] = useState(1)
  const [code, setCode] = useState()
  const [valueForm, setValueForm] = useState();


  const handleResetPass = async () => {
    setIsModalVisible(true)
  }

  const handleOk = async () => {
    setLoading(true)
    const {data} = await checkCodePhone({
      to: numberPhone, code: code
    });
    console.log(data)
    if (data.status === "approved") {
      try {
        const {data} = await resetPassword({
          user:userId,
          password:valueForm.password
        });
        console.log("sssssssssssss",data);
        notification.success({message: "Thay đổi mật khẩu thành công!"});
        setLoading(false)
        setIsModalVisible(false);

      } catch (err) {
        console.log(err)
      }
    }
  };

  const handleCheckCode = (values) => {
    console.log(values)
  }

  const onFinish = async (values) => {
    const {data} = await getUserPhone({phone: values.phone});
    console.log(data)
    setUser(data)
    setUserId(data.id)
    setValueForm(values);
    setIsModalVisible(true);
    const phoneNumber = `+84${(values.phone * 1)}`
    setNumberPhone(phoneNumber);
    const res = await verifyPhone({
      phone: phoneNumber
    });
    console.log(res)
  };


  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <Row>
      <Modal title="Xác minh tài khoản" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        {user !== null &&  <div className="d-flex justify-content-center">
          <div className="d-flex">
            <Avatar src={user.avatar}/>
            <div className="mt-1" style={{marginLeft: "10px"}}>{user.fullName}</div>
          </div>
        </div>}

        <Form
          layout="vertical"
          requiredMark={false}
          onFinish={handleCheckCode}
        >
          <Form.Item label="Mã xác thực OTP" name="code">
            <Input type="number"
                   onChange={e => {
                     setCode(e.target.value)
                   }}
                   placeholder="Nhập mã xác thực ..."/>
          </Form.Item>
          <div className="d-flex">
            <div className="d-flex m-auto">
              <Timer initialMinute={time}/>
              {showTime ? (
                <Timer initialMinute={time}/>
              ) : ("")}
              <span style={{marginLeft: "5px", cursor: "pointer", color: "#3670ee"}}
                    onClick={() => {
                      setShowTime(true)
                      setTime(1)
                    }}
              >Gửi lại mã</span>

            </div>
          </div>
          <div className="text-center"><Link to={""}>Đây không phải tôi.</Link></div>

        </Form>
      </Modal>
      <div className="h-100 w-100" style={backgroundStyle}>
        <div
          className="d-flex from-body"
          style={{width: "100%", height: "100vh"}}
        >
          <Col xxl={5} xl={6} md={10} sm={8} xs={16} className="m-auto">
            <Card
              className=" card_form"
              style={{
                backgroundColor: "rgb(255,255,255)",
                border: "1px solid #ccc",
                borderRadius: "10px",
                maxWidth: "100%",
              }}
            >
              <h5
                className="form-title text-center"
                style={{fontWeight: 500}}
              >
                Quên mật khẩu
              </h5>
              <div className="register-account mt-2 text-center">
                Đã có đăng nhập và mật khẩu?{" "}
                <Link to="/login" style={{color: "rgb(24, 144, 255)"}}>
                  Đăng nhập
                </Link>
              </div>
              <Form
                name="normal_login"
                className="login-form mt-4"
                initialValues={{remember: false}}
                onFinish={onFinish}
              >
                <Form.Item
                  name="phone"
                  rules={[
                    {required: true, message: "Please input your phone!"},
                  ]}
                >
                  <Input
                    prefix={<PhoneOutlined className="site-form-item-icon"/>}
                    placeholder="Nhập số điện thoại"
                  />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={rules.password}
                >
                  <Input
                    name="password"
                    prefix={<LockOutlined className="site-form-item-icon"/>}
                    type="password"
                    placeholder="Nhập mật khẩu ..."
                  />
                </Form.Item>
                <Form.Item
                  name="confirm"
                  rules={rules.confirm}
                >
                  <Input
                    name="confirm"
                    prefix={<LockOutlined className="site-form-item-icon"/>}
                    type="password"
                    placeholder="Nhập lại mật khẩu ..."
                  />
                </Form.Item>

                <Form.Item className="mt-4">
                  <div className="btn-login text-center">
                    <Button
                      style={{
                        width: "100%",
                        border: 0,
                        backgroundColor: "rgb(24, 144, 255)",
                        color: "#fff",
                      }}
                      htmlType="submit"
                      className="login-form-button "
                    >
                      Reset password
                    </Button>
                  </div>
                </Form.Item>
              </Form>
              <br/>
              <br/>
            </Card>
          </Col>
        </div>
      </div>
    </Row>
  )
}

export default ResetPassword