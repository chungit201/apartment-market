import {
  Form,
  Input,
  Button,
  Checkbox,
  Card,
  Col,
  Row,
  notification, Divider,
} from "antd";
import {UserOutlined, LockOutlined} from "@ant-design/icons";
import "../custom.css";
import {Link} from "react-router-dom";
import {login} from "../../service/ApiService/userApi";
import {setData} from "../../service/storageService";
import {setUserData} from "../../redux/actions/User";
import {useState} from "react";

const backgroundStyle = {

  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

const LoginPage = () => {
  const [loading, setLoading] = useState(false);
  const handleLogin = async (values) => {
    setLoading(true);
    await login(values)
      .then(async (res) => {
        console.log(res);
        if (res.status === 200) {
          const {user, tokens} = res.data;
          window.location.replace('/')
          await setData("ACCESS_TOKEN", tokens.access);
          await setData("REFRESH_TOKEN", tokens.refresh);
          setUserData({...user});
        }
      })
      .catch((err) => {
        console.log(err)
        setLoading(false);
        console.log(err.response.data.message);
        notification.error({message: err.response.data.message});
      });
  };
  return (
    <div className="h-100 w-100">
      <div className="d-flex from-body" style={{width: "100%", height: "100vh"}}>
        <Row justify="center" className="m-auto">
          <Col span={8}>
            <div>
              <h2 style={{color: "#ee4d2d"}}>
                Bán hàng chuyên nghiệp
              </h2>
              <p style={{fontSize: "18px", color: "#666666"}}>Quản lý shop của bạn một cách hiệu quả hơn trên apartment
                với apartment - Đăng kí shop để nhận ưu đãi tốt nhất cho bạn.</p>
              <img width="100%"
                   src="https://deo.shopeemobile.com/shopee/shopee-seller-live-sg/rootpages/static/modules/account/image/login-img.9347138.png"
                   alt=""/>
            </div>
          </Col>
          <Col span={4}></Col>
          <Col span={7}>
            <Card
              className=" card_form"
              style={{
                backgroundColor: "rgb(255,255,255)",
                border: "1px solid #ccc",
                borderRadius: "10px",
                width: "100%",
              }}
            >
              <h5
                className="form-title text-center"
                style={{fontWeight: 500,}}
              >
                Đăng nhập vào Apartment
              </h5>

              <Form
                name="normal_login"
                className="login-form mt-4"
                initialValues={{remember: false}}
                onFinish={handleLogin}
              >
                <Form.Item
                  name="username"
                  rules={[
                    {required: true, message: "Please input your Username!"},
                  ]}
                >
                  <Input
                    prefix={<UserOutlined className="site-form-item-icon"/>}
                    placeholder="Username"
                  />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[
                    {required: true, message: "Please input your Password!"},
                  ]}
                >
                  <Input
                    name="confirm"
                    prefix={<LockOutlined className="site-form-item-icon"/>}
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>
                <Form.Item>
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Nhớ tôi</Checkbox>
                  </Form.Item>

                  <Link
                    style={{color: "rgb(24, 144, 255)"}}
                    className="login-form-forgot"
                    to="/reset-password"
                  >
                    Quên mật khẩu
                  </Link>
                </Form.Item>
                <Form.Item className="mt-4">
                  <div className="btn-login text-center">
                    <Button
                      loading={loading}
                      style={{
                        width: "100%",
                        border: 0,
                        height: "40px",
                        backgroundColor: "#ee4d2d",
                        color: "#fff",
                      }}
                      htmlType="submit"
                      className="login-form-button "
                    >
                      Đăng nhập
                    </Button>
                  </div>
                </Form.Item>

                <div className="register-account mt-2 text-center">
                  Bạn chưa có tài khoản?
                  <Link to="/register" style={{color: "rgb(24, 144, 255)"}}>
                    Tạo tài khoản
                  </Link>
                </div>
                <Divider plain>OR</Divider>
                <Row justify={"center"} gutter={18}>
                  <Col span={12}>
                    <Button className="w-100">
                      <div className="d-flex justify-content-center">
                        <img width="20px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/768px-Facebook_Logo_%282019%29.png" alt=""/>
                        <div className="mx-1">facebook</div>
                      </div>
                    </Button>
                  </Col>
                  <Col span={12}>
                    <Button className="w-100">
                      <div className="d-flex justify-content-center">
                        <img width="20px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABd1BMVEX////t7e3u7u7tQS0tqk9Agvr8vQDr6+srePouevvm7//V3+42fvozfPry8Oz8ugCZufzukYqov/PtLhCwyPuOrvPz8PLtOiTt+Pnt8vL8vwAgp0fsNBvtOSP3m5QAqFMAojvtJwCnw/z49ewtqVHt1dP4urXttbD86OT84N3uioDtfXPtg3rtZ1fs8ff+5Knk9ejC0vErrTlunvY9h+pilvev1blmv33+9fTvysfuwLrtnpfyYVbtTz73sqv6zcfvWEXub2Ts4d/4wLn4pQD+2YTxXiXtNi/2gxrzeB/9xy73nRP+9truTyn10HX4kxTy4LH5rwj3zF3xZSP+45/1xkP82Kbx6tr11YxPjfv9/e7x5L69z/H/9tXx3av4yk7g469drDtAtm3cuxaqtSl9x426tiKBsDdIrEjLuRySsTKAqPWJyJrD58xsvoAVnWg8jNY3l6g5lLUzn4OVyqM6kcE0m5EwpWE7js81mZ09ieJ4tLnP4tVXN4zsAAANLUlEQVR4nO2daWPbxhGGKYgiSJuHZVhLgCBj0IRMUwdNSTElWxJt03HSuonbpnGTOHba2FGOSmTahml66Md3QfAmFpg9cFDWfBp9wHIfzezMu7ugFItZtiRJ0tIF9WIRmccl4SWhJyE2+YJ6fdeGlS+oJ4WeRv560iXhwnvSxJKUL6Q3KKjRmIw/nm3RSChf+2Ho87gk5CYMXXn4511qmoX33oWOf/EJx0sy/OZ8qWmYPNuCSBlJw9b3LEcK7HMDILS5DtbvP6hWb1tWrT64v34wYF14QlVarx5uHS0rZaU4NkXBPy8fbR1W7yM1GELhigKHLhar3d44VjBZKb/sZPm8xXq81apZ0Vw4TVNrbRcVEtsMp6JsW5QLo2k0VXq4kVeK3nATmEWltPVwyYdlKb7ja9qDTTxdCrqhlRRlszootxEmrK2WFJrgzYRSKW7URBOK0zR41CdHZZboTUWy/Oh2TOysRAxje4c84ZsIZLG4eiBuVjEhqYC9g/eUogA824rljYNIdXxNO3ivLI7PshJm1CJDqEmHTMXTg1FZRSKax4CQQzNIarUkNn5DKxZbQ4UepqZR7xwpvvD1GR/VVClcTaNpG2UR9ZNk+fKWxJeqnB1fvb8sfgFOW6n0QOXZTPIR4gD6zGdZeZOPkEM93Dn2p8LMWmm5xjQ/Xk2jtoIIoG3lQzVwTSNpm/6V0HlTHiPWTQcrYfvY7xIzbaXl9WAJa0I0No3llYdchJRK4UlwS3Bs5RbDTGNMmkY9DAMQI1a1YDSNuhFkjRmbsqoG1PEDLaLTgMEQbgbT5ucA31MD0jShAQakadSwAFfVYDTNYhUZlo5/uFBFhoEwlEbPXmToNU0tLMCAzmmkdmhFhkXJjD1wP9SOgxbbQ8CAzmnUTb7tUr7Uv/q1jXhp6ggYEGGLvYxiOKW0vdF68rC23m6312sPn7RWt/OQ+1O+IkOlae6wVpl8UTlerbbV2S26prYfrD7yuEdlVjL0mkY7ZuMrKtuttkocWW1XH7tAcigZWk2jbrDU0bzyCOO5H+hqqnz7EeHSkbfIUHR87T5DjpbKm7UYYJlLuNFuOTFyFxkaQnq+vLJ1QDGPg4252yv+IgPXNOoqbaPIlzfbGmDksae1t6YvQDiVDJWmkajrqHLE8nbMne2JhsStZMae90JRj+jETF5pMSZUtThMFjFFBtjxtSpdr1e226zH05r8WBkBBkco0UWw3OL57av9X6eoIgPTNNohTSvML69r7M1ZXpK19nFJgJKh0TQHNDla3OafFnosQslQaJpVihAqW1zXtbYnCX59z4vw5QcUgIeiCmCQhE8TiQ8/ggHiGrMUGS6oplmK5RKJwm9AiOXbmhANItrz0DRfYMJE4QMAYrmlEUcJ13PvhxYgRsx94sWI12BU0pKq49+1CTHjb90Ri1tRLDIAwmeJoRV+54ZY2hamssQTummaj3OJMaJrpsqSKA0i2nPVNPXnE4SJAjlTy+uhg7Bpmnpi2gq/d0ZUWqJfsA+o49f/kJtF/KMTYH47omXUm/DTxKwVCn+aD6PSjgwNpaaRZkPonKk4RyOjXyg1zVyS2ogzUjx/5PRslDxyP3zuSJgoTEvxsuDvuATZ8R35+otxQornN0NnYCWUPnYOYZ9xLHCUdugMrJqm/hmZcCxwSluaw7NR8lw0zQsyYGIkxcsHEQFh0TQuIRxlqrUKo5GMDB3/tQchFjiffKTUosDASHjXi9ASOMeRYGDUNIRuOM34Z+dnvb4pNfp00R6NpqkXvAETuQq9yqhfvXrFsquWifSI348i9UNACBMv6tTJk72eTCUtS1km0EsS5kLs+F8BCHN36ZdH9np6xRdL7WTpCL+GEH4eJcIbJEJnTVP/wpswV6jTN2LfCNNrWTpN8ymA8DkdnO3dyPhEuEf43BghoZ55AiZyX9MlqO35RbjyJeFzSYSAZpH7PFKEaUpCSL+vR4ow6U44qwVeAgif1Z2fdff8I7xCpWlctr+jJH3OcnKSve4XYWpHotE0gIaf+4xFEPvWLVZSr7I0Hd/5nG2a8G60CDNUhHWApMl9FTHCGwRCR00DInzNcrjgI+H1LI2m8d7/4nbIdHLiWy3N3KLSNCBCpl23r4QUHf+ScPEIZ7UAxTqMiqbpE4I1TR1C+DpamgbXUgpNs5j9EF10TUPq+Bdflzqf08D3FpHRNKkdKk0D2R++qNPBBbA/pDmngRwIR2uPnyJ8Lonw4p/TXLiztjktADkvfUp4NhxNs0f4XMI5DeTMO/GsHiFNY515U2gaCSLbonVvkSHeWzgTAu+eWG7X/Ioh7d1TFtIu/sJAeCuVpjYIYVJ2/lzi+zT1AiSIWXpNs7O2ds2yNcuAHgixQvs+DaCYJnJ/pYEbeVnLKLydFCBJ90ifFiMlFKDUFL55Q5OgzN4eIIbW/peu43u/T5N7G48bjQAIrwBCiHU3NaGnMv32phnXTwIgvAapviniKANCBy3g+l5bLvHNzTg2Q/X/PV9ICPuajUrTeLybmHgbNy3AuH6GHJ4V6WVBIcxcyxJHIfRDCZEXYi7x3U0bEAdRFpWMJA/UKjK435P6IYmQ/I5wIvH9zfjQrJXoKyEohHgZkkZxIyR1xMIPY0C7nPpIWE9CAHE3JBMSNA32COdtP44y1LZ9p2eFeZBeaN0dEkchaxrsORL+7WZ82owm8gcOe1mInMGWrJNHiZETxeE7M1jGzALipSj71w9BfCvpexXyKC6Ec++25UZNYpqw63jYLMCrnMK2WoTDYG9CaRbw2/kADvLUF8LsK1iOrqTcvkM+IHRUFPWZ94QdMnSAeO4yCrsnA48D0qdZl1Fcv8s91fTfEvAsMyUfNE32HpAwueM6HrkfxqbOFL8jBXCwFIUXGegiHGhSlo4fm9wkfu8G2Nenggkr14GLENcZDsLhFurZD6YrIF6KPaEFVa5AqwwmdB3P6+/TPM3ZMsapS8wgNhFxFIaDjh2QWrMsveY6nqumwda/g5qTMc6IHSRK08jZq+AIYj3jHqSYR8o8z00LbfcoCioyWKyBz43Tpx7jeRG+/BGG10fEa1EAIaq8Aqdo/9qQjzB2olMgnokoN/AqaocQRkjWFhUDTmj1Ra/xPL3KKQUg8c1goKaxPHRGEcS4/kZGXEomK9+judhIn1YE/M09GsK4rjd5ev/BqwzV3VSGcFsB7/jYQ00qxLjRlVi3Gkg6+YmGbyVzKyuAUEJvqAitMDISdvCz+t8psvRL0gkbXNNYHmrQFBu80Ygb+w2X8Uie2jVM/PjuP8CIyZ2s7DWyh6axPUTTMewwGmcynYhD8pkx+JRdaKam9yqAkWOgNKIEtBmpEvRMH/0WTd38LyiMaRnyF/5AhKhDl6dDRujNVGMUv4Ht/hOASHiRjYlQos9Tm3G/IyH35oEQ6uwbujn9pLn7s+fdNs5RUHYMCL30BkOe2pB6t6mRdQ6Smie64fTb0833PcKIcxSkkTw1zcCjrKdTkPu9jozQzKchtHTew9EjJsfuL66I/cMZkFry7oe212NGtCANs9trdhoNeUmW5Uaj0+l1TcMxeBOI/3JBTK2Blhes4w9+5/ssS3GSUjfGppNjNzJT/5WcqfcqwgklyeQiZDHd+DcJEdQooJpm4EnnHHnKZmZ89z+OiMmrElQtgTTN0GPpirxm/LQy3zaSr8BzHhpQeTRDQHSQ4iniuzPsHX+krfraOGCbleKZU6o50xGyaRteM36ezNTMXpaFEHqagnuGGTzipBRP3xuqUeCcoZpm5HG2RSYzx1I8cw88U1pNM/JCiWIcS/ERINXpAcv/sAwjijhTf30/PQD0nRB1Q4mivvtLkrLIUGqaiXJzFkJfxLb7v4lvGPqiacZeGK0/bvQYZjo06oO/EASc0WGaKSth7NwMuN7o54wzZSWUpEBLqnUfwkcI1TQT5Qbx7Popzbq2Y73LotY0k2c38WDCqOsdpvmxappJr+t/GK07Apl1fowdf8JDTcB5C5/1r+vCI5TQ0om/YTS6Mt+7SCyaZsqTUMPHvqGbHcT5XxcYNc3UrRHq+ZSqut4bJCjP/GIcCTBKVW32ZkUIn3HCmaB8HX/aQ40TwYy60W0gzlmJJMSeUEYcv4Z1RCuQkF7TzHtzt4B8fIJmxaVpZjwsHJsmN6RuxHtWCETNilPTzHvnXa7Cipcf4ybJr44/76Fsc9/r3oxApxtvRtepESa0Lq7lTpcWUjeM/abscSUeiqYh6ohO743hcsM7HTvD7HWEzkCopiF41rV2p7cfty5DddJL1Na1qXUJ3r/0Fg4nTtO4Lku50zzbN3X73nds1s/m/lmzI3zh+dbxiR7qv6SgNs6bzWav2bMMe+cN6z+uIx8WXvCEQ89CtXU0Qgj5HTlfNE1EPYGaJqqe+H4YLc+Pjh8t710g9EPTRMkbFNRoTGYxNU343iXh4nsDwtCVh3/epaZZeO9d6PgXn3C8JMNvzpeahsmzLRoJ5Ws/DH0el4S8hOOCevG8/wP97KrmCqVwwAAAAABJRU5ErkJggg==" alt=""/>
                        <div className="mx-1">Google</div>
                      </div>
                    </Button>
                  </Col>
                </Row>
              </Form>
              <br/>
              <br/>
            </Card>
          </Col>


        </Row>
      </div>
    </div>
  );
};

export default LoginPage;
