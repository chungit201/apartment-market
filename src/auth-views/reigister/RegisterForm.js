import {Alert, Button, Card, Checkbox, Col, Form, Input, Modal, notification, Row, Select} from "antd";
import {Link} from "react-router-dom";
import {checkCodePhone, getUserPhone, register, verifyPhone} from "../../service/ApiService/userApi";
import {useState} from "react";
import Timer from "../../components/common-component/Timer";
import logo from "../../assets/logoAP1.png";
import {LockOutlined, UserOutlined} from "@ant-design/icons";

const {Option} = Select;

const backgroundStyle = {
  backgroundImage: "url(/img/img-18.jpg)",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  height: "100%"
};


const rules = {
  password: [
    {
      required: true,
      message: 'Vui lòng nhập mật khẩu'
    }
  ],
}

const RegisterForm = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [numberPhone, setNumberPhone] = useState();
  const [valueForm, setValueForm] = useState();
  const [showTime, setShowTime] = useState(false);
  const [loading, setLoading] = useState(false)
  const [time, setTime] = useState(1)
  const [code, setCode] = useState();
  const [disable,setDisable] = useState(false)


  const onFinish = async (values) => {
    console.log(values)
    setValueForm(values);
    setIsModalVisible(true);
    const phoneNumber = `+84${(values.phone * 1)}`
    setNumberPhone(phoneNumber);
    const res = await verifyPhone({
      phone: phoneNumber
    });
    console.log(res)
    // await checkCodePhone(values)
  };
  const handleCheckCode = (values) => {
    console.log(values)
  }


  const handleOk = async () => {
    setLoading(true)
    const {data} = await checkCodePhone({
      to: numberPhone, code: code
    });
    if (data.status === "approved") {
      try {
        setIsModalVisible(false);
        const {data} = await register(valueForm);
        console.log(data);
        notification.success({message: "Đăng kí thành công!"});
        setLoading(false)
      } catch (err) {
        console.log(err)
        console.log(err.response.data.message)
      }
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);

  };

  return (

    <div className="h-100 w-100" style={backgroundStyle}>
      <Modal title="Xác minh tài khoản" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Form
          layout="vertical"
          requiredMark={false}
          onFinish={handleCheckCode}
        >
          <Form.Item label="Mã xác thực OTP" name="code">
            <Input type="number"
                   onChange={e => {
                     setCode(e.target.value)
                   }}
                   placeholder="Nhập mã xác thực ..."/>
          </Form.Item>
          <div className="d-flex">
            <div className="d-flex m-auto">
              <Timer initialMinute={time}/>
              {showTime ? (<Timer initialMinute={time}/>) : ("")}
              <span style={{marginLeft: "5px", cursor: "pointer", color: "#3670ee"}}
                    onClick={() => {
                      setShowTime(true)
                      setTime(1)
                    }}
              >Gửi lại mã</span>
            </div>
          </div>
        </Form>
      </Modal>
      <div className="from-body" style={{width: "100%", height: "100vh"}}>
        <Row justify="center" className="m-auto">
          <Col span={12}>
            <div className="d-flex " style={{height: "100vh"}}>
              <div className="m-auto">
                <div className="d-flex justify-content-center">
                  <img width="100px" src={logo} alt=""/>
                </div>
                <h2 className="text-center">Apartment Market</h2>
                <p className="text-center" style={{width: "350px"}}>Nền thương mại điện tử được yêu thích nhất ở Việt
                  Nam phổ biến trong các tòa chung cư.</p>
              </div>
            </div>
          </Col>
          <Col span={7} className="m-auto">
            <Card className=" card_form"
                  style={{
                    backgroundColor: "rgb(255,255,255)",
                    border: "1px solid #ccc",
                    borderRadius: "10px",
                    maxWidth: "100%"
                  }}>
              <h4 className="form-title text-center" style={{fontWeight: 500}}>Đăng kí APMK</h4>
              <div className="register-account mt-2 text-center">
                Bạn đã có tài khoản? <Link to="/login" style={{color: "rgb(24, 144, 255)"}}> Đăng nhập</Link>
              </div>
              <Form
                name="normal_signup"
                className="signup-form text-light mt-4"
                initialValues={{remember: true}}
                onFinish={onFinish}
                layout="vertical"
                requiredMark={false}
              >
                <Form.Item
                  className="text-light"
                  label="Họ và tên:"
                  name="fullName"
                  rules={[{required: true, message: 'Please input your full name!'}]}
                >
                  <Input placeholder="Nhập họ tên ..."/>
                </Form.Item>
                <Form.Item
                  label="Tên tài khoản:"
                  name="username"
                  rules={[{required: true, message: 'Please input your username!'}]}
                >
                  <Input placeholder="Nhập tên tài khoản ..."/>
                </Form.Item>
                <Form.Item
                  label="Password"
                  name="password"
                  rules={[{required: true, message: 'Please input your Password!'}]}
                >
                  <Input.Password placeholder="Nhập mật khẩu ..."/>
                </Form.Item>
                <Form.Item
                  name="phone"
                  label="Số điện thoại"
                  rules={rules.phone}
                >
                  <Input
                    style={{width: '100%'}}
                    onChange={(e) => {
                      getUserPhone({phone:e.target.value}).then(async (res)=>{
                        if(res){
                          setDisable(false)
                        }
                      }).catch((err) => {
                        setDisable(true)
                        notification.error({message:err.response.data.message})

                      })
                    }} placeholder="Nhập mật số điện thoại ..."/>
                </Form.Item>

                <Form.Item
                  label="CCCD:"
                  name="cardId"
                  rules={[{required: true, message: 'Please input your address!'}]}
                >
                  <Input placeholder="Nhập cccd ..."/>
                </Form.Item>

                <Form.Item
                  label="Địa chỉ nhà:"
                  name="address"
                  rules={[{required: true, message: 'Please input your address!'}]}
                >
                  <Input placeholder="Nhập địa chỉ ..."/>
                </Form.Item>
                <Form.Item className="mt-4">
                  <div className="btn-login text-center">
                    <Button style={{backgroundColor: "#ee4d2d", border: 0, width: "100%", height: "40px"}}
                            type="primary"
                            disabled={disable}
                            htmlType="submit" className="login-form-button ">
                      Tiếp theo
                    </Button>
                  </div>
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
      </div>
    </div>


  )
}
export default RegisterForm;