import {Button, Card, Form, Input, Steps, message, Select, Row, Col, notification} from 'antd';
import '../custom.css';
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {checkCodePhone, verifyPhone} from "../../service/ApiService/userApi";
import {getCategory} from "../../service/ApiService/categoryApi";
import {addShop} from "../../service/ApiService/shopApi";
import {Link, useNavigate} from "react-router-dom";
const {Option, OptGroup} = Select;

const RegisterShop = () => {
  const user = useSelector(state => state.user);
  const [phone, setPhone] = useState();
  const [category, setCategory] = useState([]);
  const [select, setSelect] = useState([]);
  const [countCategory, setCountCategory] = useState();
  const [numberPhone, setNumberPhone] = useState();
  const [name,setName] = useState();
  const [code,setCode] = useState();
  const [adress,setAdress] = useState();
  const [money,setMoney] = useState();
  const [loading,setLoading] = useState(false)

  const navigate = useNavigate()

  useEffect(() => {
    getDataCategory()
  }, [])

  let total
  const handleChangeCate = (value) => {
    setCountCategory(value);
    if(value === "3"){
      total = 50000
    } else if(value === "6"){
      total = 100000
    }else {
      total=200000
    }
    setMoney(total)
    setSelect([]);
    console.log(select)
  }

  const getDataCategory = async () => {
    try {
      const {data} = await getCategory(1,100);
      setCategory(data.results);
    } catch (e) {
      console.log(e)
    }
  }
  console.log(countCategory)

  const handleOTP = async () => {
    const phoneNumber = `+84${phone * 1}`
    setNumberPhone(phoneNumber)
    const res = await verifyPhone({
      phone: phoneNumber
    });
    console.log(res)
  }

  const onFinish = async (values) => {
    setLoading(true)
    const {data} = await checkCodePhone({
      to: numberPhone,
      code: code
    })
    if (data.status === "approved"){
     const res = await addShop({
        name:name,
        author:user.id,
        categories:select,
        adress:adress,
        package:money,
        duration: new Date()
      })
      if(res.status === 200){
        navigate(`/payment?amount=${money}&description=đăng kí shop&mode=shop`)
      }
    }
    setLoading(false)
  }
  return (
    <div>
      <div className="container mt-2">
        <h4 className="text-danger">Đăng kí trở thành shop Apartment market</h4>
      </div>
      <Form
        name="basic"
        onFinish={onFinish}
        layout="vertical"
        requiredMark={false}
        initialValues={{remember: true}}
        autoComplete="off"
      >
      <Row gutter={16}>
        <Col span={18}>
       <Card>
           <div className="d-flex">
             <div className="m-auto" style={{width: "500px"}}>
               <Form.Item label="Tên shop" name="name"
                          rules={[{required: true, message: 'Vui lòng nhập tên shop!'}]}
               >
                 <Input placeholder="Nhập vào" onChange={e=>{setName(e.target.value)}}/>
               </Form.Item>
               <Form.Item label="Địa chỉ lấy hàng" name="adress"
                          rules={[{required: true, message: 'Vui lòng nhập tên địa chỉ!'}]}
               >
                 <Input placeholder="Nhập vào" onChange={e=>{setAdress(e.target.value)}}/>
               </Form.Item>

               <Form.Item label="Chọn gói">
                 <Select style={{width: 200}} onChange={handleChangeCate}>
                   <OptGroup label="Manager">
                     <Option value="3">50k/1 tháng (3 danh mục)</Option>
                     <Option value="6">100k/1 tháng (6 danh mục)</Option>
                     <Option value="1000">200k/1 tháng (tất cả danh mục)</Option>
                   </OptGroup>
                 </Select>
               </Form.Item>
               <div style={{display: countCategory ? 'block' : 'none'}}>
                 <Form.Item label="Danh mục">
                   <Select mode="tags" value={select} style={{width: '100%'}}
                           onSelect={(x, y) => {
                             setSelect((state) => {
                               return [...state, x]
                             })
                           }}
                           onDeselect={
                             (x, y) => {
                               setSelect((state) => {
                                 return state.filter(item => item !== x)
                               })
                             }
                           }
                           placeholder="Tags Mode">
                     {category.map(item => {
                       return (
                         <Option disabled={!select.includes(item.id) && select.length === parseInt(countCategory)} key={item.id}
                                 value={item.id}>{item.name}</Option>
                       )
                     })}
                   </Select>
                 </Form.Item>
               </div>

               <Form.Item label="Số điện thoại" name="phone"
                          rules={[{required: true, message: 'Vui lòng nhập tên số điện thoại'}]}
               >
                 <Input placeholder="Nhập vào"
                        onChange={e => {
                          setPhone(e.target.value)
                        }}
                 />
               </Form.Item>
               <div className="d-flex">
                 <Form.Item label="Mã OTP" name="opt"
                            rules={[{required: true, message: 'Vui lòng nhập tên mã opt'}]}
                 >
                   <div className="d-flex">
                     <Input placeholder="Nhập vào" onChange={e=>{setCode(e.target.value)}}/>
                     <Button onClick={handleOTP}> Gửi</Button>
                   </div>
                 </Form.Item>
               </div>

             </div>
           </div>

       </Card>
        </Col>
        <Col span={6}>
          <Card title="Thông tin">
            <div className="mt-1">
              <span>Tên shop: {name} </span>
            </div>
            <div className="mt-1">
              <span>Địa chỉ cửa hàng: {adress} </span>
            </div>
            <div className="mt-1">
              <span>Số lượng danh mục: {countCategory} </span>
            </div>
            <div className="mt-1">
              <span>Số tiền thanh toán: {money} đ</span>
            </div>
            <Form.Item>
               <Button loading={loading} htmlType="submit" className="mt-2">Tiếp theo</Button>
            </Form.Item>
          </Card>
        </Col>
      </Row>
    </Form>
    </div>
  )
}
export default RegisterShop