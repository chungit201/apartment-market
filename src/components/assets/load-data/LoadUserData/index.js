import useSWR from 'swr';
import {refreshTokenApi} from "../../../../service/ApiService/userApi";
import {authDataStorage} from "../../../../service/storageService";
import {setUserData} from "../../../../redux/actions/User";

const loadUserData = () => {
  return new Promise(async resolve => {
    try {
      const tokens = await JSON.parse(localStorage.getItem('REFRESH_TOKEN'));
      const currentTime = new Date().getTime();
      if(!tokens || tokens.expires > currentTime) resolve(null);
      const res = await refreshTokenApi({refreshToken:tokens.token});
      console.log(res.data)
      const { user, access, refresh } = res.data;
      setUserData({...user});
      // dispatch(action);
      await authDataStorage({access, refresh});

      resolve(user);
    }
    catch(err) {
      resolve(null);
    }
  })
}

export const loadData = (props) => {
  console.log(props)
  return new Promise(async (resolve, reject) => {
    const user = await loadUserData();
    resolve({
      userData: user,
    })
  })
}

export default function useLoadUserData () {
  const { data, error } = useSWR("/", loadData);

  const loading = !data && !error;

  return {
    user: data?.userData,
    loading,
  }
}
