import React, {useEffect, useState} from 'react';
import useLoadUserData from "../LoadUserData";
import {LoadingOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router-dom";
import {updateUser} from "../../../../service/ApiService/userApi";

const LoadData = ({children}) => {
  const navigate =  useNavigate()
  const {user, loading} = useLoadUserData();
  const [deviceToken, setDeviceToken] = useState()
  useEffect(() => {
    setDeviceToken(localStorage.getItem('deviceToken'))
    authentication()
  }, [user, loading]);
  const authentication = async () =>{
    // if (!loading && user === null) {
    //   await navigate('/login',{replace:true});
    // }

    if (user) {
      checkDeviceToken(user)
    }

  }

  const checkDeviceToken = async (user) => {
    if (deviceToken) {
      if (user.deviceToken !== deviceToken) {
        await updateUser(user.id,{
          deviceToken: deviceToken
        });
      }
    }
  }

  return (<>
    {loading ? <div style={{height: '100vh', display: 'flex'}}>
      <LoadingOutlined style={{fontSize: 30, margin: 'auto'}}/>
    </div> : children}
  </>);
}

export default LoadData;
