import React from "react";
import dynamic from "next/dynamic"

const Editor = dynamic(() => import('../../WapperEditor'))

const EditorWithForwardedRef = React.forwardRef((props, ref) => (
  <Editor {...props} forwardedRef={ref} />
))


export default EditorWithForwardedRef;