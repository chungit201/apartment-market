import "../../../cusstom.css";
import { Card, Row, Col, Select } from "antd";
import { NavLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import Meta from "antd/es/card/Meta";
import {
  getCategoryProduct,
  getCategory,
  getCategories,
} from "../../../../service/ApiService/categoryApi";
import ProductPage from "../../../../app-views/products/ProductPage";
import { getCateProducts ,getProductSortBy} from "../../../../service/ApiService/productsApi";
const { Option } = Select;
const Category = () => {
  const { id } = useParams();
  console.log(id);
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState({});
  const [page, setPage] = useState(1);
  const [totalResults, setTotalResults] = useState(0);

  const limit = 24;

  useEffect(() => {
    getDataCategory();
    getProducts();
    getCate();
  }, [id]);

  let categoryByShop = [];
  const getDataCategory = async () => {
    const { data } = await getCategory();
    const newCategory = data.results.filter((x) => x.id !== id);
    console.log(newCategory);
    setCategories(newCategory);
  };

  const getProducts = async () => {
    const { data } = await getCategoryProduct(id, page, limit);
    setProducts(data.results);
    setTotalResults(data.totalResults);
  };

  const getCate = async () => {
    const { data } = await getCategories(id);
    setCategory(data);
  };

  const handleChang = async (value) => {
    const { data } = await getCateProducts({categories:value.id});
    setProducts(products.concat(data.results).reverse());
  };

  const handleChangeOption = async (value) => {
    const { data } = await getProductSortBy(id,1,24,value);
    setProducts(data.results);
  }
  
  return (
    <div className="container">
      <div className="row">
        <div className="col-2">
          <span>Danh Mục Liên Quan </span>
          <div className="form-check mt-2">
            {categories.map((category) => {
              return (
                <div className="form-check">
                  <input
                    onChange={() => handleChang(category)}
                    className="form-check-input"
                    type="checkbox"
                    value={category.id}
                    id="flexCheckDefault"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    {category.name}
                  </label>
                </div>
              );
            })}
          </div>
        </div>
        <div className="col-10">
          <h3>Trang chủ - {category.name} </h3>
          <div className="d-flex">
            <span>
              {totalResults} sản phẩm tìm thấy trong {category.name}
            </span>
            <div className="d-flex" style={{width: '100%', justifyContent: "end"}}>
              <span className="mt-1">Sắp xếp:</span>
              <Select
                defaultValue="Sắp xếp theo"
                style={{marginLeft:"10px",width: '150px'}}
                onChange={handleChangeOption}
              >
                <Option  value="price" style={{width: '100%'}}>Từ thấp đến cao</Option>
                <Option value="-price" style={{width: '100%'}}>Từ cao xuống thấp</Option>
               
              </Select>
            </div>
          </div>

          <Row gutter={16}>
            {products &&
              products.map((item) => {
                return (
                  <Col xxl={4} xl={4} xs={12} sm={6}>
                    <ProductPage key={item.id} product={item} />
                  </Col>
                );
              })}
          </Row>
          <Row>
            <Col span={24}>
              <div style={{ float: "right" }}>
                <div className="mt-3">
                  <nav aria-label="Page navigation example ">
                    <ul className="pagination text-center">
                      <li className="page-item">
                        <a className="page-link" href="#" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                          <span className="sr-only">Previous</span>
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          1
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          2
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          3
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                          <span className="sr-only">Next</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};
export default Category;
