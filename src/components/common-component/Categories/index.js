import {Card, ListGroup, ListGroupItem} from "react-bootstrap";
import {Col, Row} from "antd";
import {Link} from "react-router-dom";

const CategoriesLayOut = ({category}) => {
  return (
    <Col xxl={3} xl={3} xs={6} sm={6}>
      <Link to={`category/${category.id}`}>
        <Card style={{maxWidth: "100%", border: "0"}} className="d-flex align-items-center">
          <Card.Img alt={category.name} className='mt-2' style={{width: "50px", height: "50px"}} variant="top"
                    src={category.img}/>
          <Card.Body>
            <Card.Title className="text-center title_category"
                        style={{fontSize: "12px", color: "#000"}}>{category.name}</Card.Title>
          </Card.Body>
        </Card>
      </Link>
    </Col>
  )
}

export default CategoriesLayOut