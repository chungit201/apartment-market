import {Link, useLocation} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {checkPayment} from "../../../service/ApiService/vnPayApi";
import {useSelector} from "react-redux";
import {Button, notification, Result} from "antd";
import {getSingle} from "../../../service/ApiService/singleApi";
import {addMyOder} from "../../../service/ApiService/oderUserApi";
import {getShopId} from "../../../service/ApiService/shopApi";
import {sendToOne} from "../../../service/ApiService/pushNotificationApi";

const publicUrl = new URL(process.env.PUBLIC_URL, window.location);

function useQuery() {
  const {search} = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
}

function randomString(len, an) {
  an = an && an.toLowerCase();
  let str = "",
    i = 0,
    min = an === "a" ? 10 : 0,
    max = an === "n" ? 10 : 62;
  for (; i++ < len;) {
    let r = (Math.random() * (max - min) + min) << 0;
    str += String.fromCharCode(
      (r += r > 9 ? (r < 36 ? 55 : 61) : 48)
    ).toUpperCase();
  }
  return str;
}

const CheckPaymentProduct = () => {
  let query = useQuery();
  const search = useLocation().search;
  const user = useSelector(state => state.user);
  const [status, setStatus] = useState(false);
  const [oderId, setOderId] = useState("")
  const amount = query.get("vnp_Amount")
  const singleId = query.get("vnp_OrderInfo")
  const load = 1
  console.log(search)
  useEffect(() => {
    checkShop().then(() =>{});
  }, [])

  let listShopId = [];
  let productShopInCarts = [];
  const checkShop = async () => {
    const {data} = await checkPayment(search);
    console.log(data)
    if (data.message === "success") {
      setStatus(true)
      setOderId(singleId)
      const {data} = await getSingle(singleId);
      console.log("dddddddd", data)

      data.single.forEach((value) => {
        const shopId = value.shop.id;
        if (!listShopId.includes(shopId)) {
          listShopId.push(shopId);
        }
      });
      console.log(listShopId);
      listShopId.forEach((shopId) => {
        let productShop = {
          shop: shopId,
          data: data.single.filter((item) => {
            if (item.shop.id == shopId) {
              return item;
            }
          }),
        };
        productShopInCarts.push(productShop);
      });

      console.log(productShopInCarts.length)
      try {
        if (productShopInCarts.length > 0) {
          productShopInCarts.map(async (item) => {
            const res = await addMyOder({
              user: user.id,
              code: randomString(10),
              products: item.data,
              shop: item.shop,
              totalPrice: (parseInt(amount) / 100),
              payment: true
            });

            if (res.status === 200) {
              notification.success({message: "Đặt hàng thành công"});
            }

            await getShopId(item.shop).then(async (res) => {
              console.log("rrrrrrrrrr", res);
              await sendToOne({
                title: "Apartment market",
                body: `Bạn có một đơn hàng mới`,
                click_action: `${publicUrl.origin}/shop/order-shop`,
                icon: "https://scontent.xx.fbcdn.net/v/t1.15752-9/275844082_348809860504907_4731915775493283911_n.png?stp=dst-png_s206x206&_nc_cat=104&ccb=1-5&_nc_sid=aee45a&_nc_ohc=5m_Oy5AzJm4AX_pnt2Y&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJ6bYu7ruyeyVePaM28kH9ia-Jg0-LJb2dE4P1-FWKFAw&oe=625728F2",
                to: res.data.deviceToken,
              });
            });
          });
        }

      } catch (err) {
        console.log(err);
      }
    }


  }
  console.log("ppppp", productShopInCarts)
  return (
    <div className="container">
      {status ? (
        <Result
          status="success"
          title="Chúc mừng bạn đã thanh toán thành công"
          subTitle={`Order number: ${oderId}  Cloud server configuration takes 1-5 minutes, please wait.`}
          extra={[
            <Link to="/">
              <Button type="primary" key="console">
                Quay lại trang chủ
              </Button>
            </Link>,
            <Link to="/purchase"><Button key="buy">Đơn hàng của bạn</Button></Link>
          ]}
        />
      ) : (
        <>ERROR</>
      )}
    </div>
  )
}

export default CheckPaymentProduct