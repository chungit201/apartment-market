import React, {useEffect, useState} from "react"
import {Button, Row, Col, DatePicker, Tabs, Empty, Modal, Form, Input, Select, notification} from 'antd';
import {CreditCardOutlined, MenuOutlined} from "@ant-design/icons"
import {getShop, sendPaymentShop, updateShop} from "../../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";
import {getShopPayment, updateOder} from "../../../../service/ApiService/oderUserApi";

const {Option, OptGroup} = Select;

const {RangePicker} = DatePicker;
const {TabPane} = Tabs;
const PortalBalance = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const user = useSelector(state => state.user);
  const [shop, setShop] = useState(null);
  const [surplus,setSurplus] = useState(0);
  const [paymentOrder,setPaymentOrder] = useState([]);
  let total=0;
  const handleOk = () => {
    setIsModalVisible(false);
  };
  
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleClick = async () => {
    setIsModalVisible(true)
  }
  useEffect(()=>{
    getDataShop();
    getShopPayments();
  },[])
  const getDataShop = async () => {
    try {
      return new Promise((async (resolve) => {
        const {data} = await getShop(user.id);
        console.log(data)
        setShop(data)
        resolve(data);
      }));
      
    } catch (err) {
      window.location.replace('/')
    }
  }
  
  const getShopPayments = async ()=>{
    const myShop = await getDataShop();
    const { data } = await getShopPayment({
      payment:true,
      shop:myShop.id,
      shopPayment: "unpaid"
    });
    setPaymentOrder(data);
    console.log(data);
    data.map(item=>{
      total += parseInt(item.totalPrice);
      setSurplus(total)
    })
  }
  
  const handleBank = async (bank) => {
    console.log("bbbbbbb", bank)
    const {data} = await updateShop(shop.id, bank)
      .then(res => {
        if (res) {
          notification.success({message: "Cập nhật tài khoản ngân hàng thành công"})
        }
      })
  }

  function randomString(len, an) {
    an = an && an.toLowerCase();
    let str = "", i = 0, min = an == "a" ? 10 : 0, max = an == "n" ? 10 : 62;
    for (; i++ < len;) {
      let r = (Math.random() * (max - min) + min) << 0;
      str += String.fromCharCode((r += r > 9 ? (r < 36 ? 55 : 61) : 48)).toUpperCase();
    }
    return str;
  }

  const handleSendPayment = async () =>{
    const myShop = await getDataShop();
    const {data} = await sendPaymentShop({
      code: randomString(10),
      shop:myShop.id,
      total: surplus,
      order: paymentOrder
    }).then(async (res)=>{
      if(res.status === 200) {
        paymentOrder.map(async (item)=>{
          const {data} = await updateOder(item.id,{shopPayment:"pending"});
        })
        notification.success({message:"Gửi yêu cầu thanh toán thành công"});
      }
    })
  }
  console.log("ssss",surplus)
  return (
    <>
      <Modal footer={null} title="Thêm tài khoản ngân hàng" visible={isModalVisible} onOk={handleOk}
             onCancel={handleCancel}>
        <Form onFinish={handleBank}>
          <Form.Item name="bankNumber" label="Số tài khoản">
            <Input/>
          </Form.Item>
          <Form.Item name="bank" label="Ngân hàng">
            <Select defaultValue="Không chọn">
              <OptGroup label="Manager">
                <Option value='VNPAYQR'>Ngân hàng VNPAYQR</Option>
                <Option value='NCB'>Ngân hàng NCB</Option>
                <Option value='SCB'>Ngân hàng SCB</Option>
                <Option value='SACOMBANK'>Ngân hàng SACOMBANK</Option>
                <Option value='EXIMBANK'>Ngân hàng EXIMBANK</Option>
                <Option value='MSBANK'>Ngân hàng MSBANK</Option>
                <Option value='NAMABANK'>Ngân hàng NAMABANK</Option>
                <Option VISA='VISA'>Ngân hàng VISA</Option>
                <Option value='VNMART'>Ngân hàng VNMART</Option>
                <Option value='VIETINBANK'>Ngân hàng VIETINBANK</Option>
                <Option value='VIETCOMBANK'>Ngân hàng VIETCOMBANK</Option>
                <Option value='HDBANK'>Ngân hàng HDBANK</Option>
                <Option value='DONGABANK'>Ngân hàng Dong A</Option>
                <Option value='TPBANK'>Ngân hàng Tp Bank</Option>
                <Option value='OJB'>Ngân hàng OceanBank</Option>
                <Option value='BIDV'>Ngân hàng BIDV</Option>
                <Option value='TECHCOMBANK'>Ngân hàng Techcombank</Option>
                <Option value='VPBANK'>Ngân hàng VPBank</Option>
                <Option value='AGRIBANK'>Ngân hàng AGRIBANK</Option>
                <Option value='MBBANK'>Ngân hàng MBBank</Option>
                <Option value='ACB'>Ngân hàng ACB</Option>
                <Option value='OCB'>Ngân hàng OCB</Option>
                <Option value='SHB'>Ngân hàng SHB</Option>
                <Option value='IVB'>Ngân hàng IVB</Option>
              </OptGroup>
            </Select>
          </Form.Item>
          <Form.Item name="bankName" label="Chủ khoản">
            <Input/>
          </Form.Item>
          <div className="d-flex justify-content-end">
            <Button>Cancel</Button>
            <Button style={{marginLeft: "10px"}} htmlType="submit" type="primary">Submit</Button>
          </div>
        
        </Form>
      </Modal>
      <div>
        <h4>Tổng quan</h4>
        <div style={{border: "1px solid #e9e9e9", boxShadow: "0 2px 5px 0 #e9e9e9", borderRadius: "4px"}}>
          <Row className="p-4">
            <Col style={{borderRight: "1px solid #e9e9e9"}} span={12}>
              <div>
                <p>Số dư</p>
                <div className="d-flex" >
                  <div className="d-flex" style={{marginRight: "30px"}}>
                    <h4 >{Intl.NumberFormat().format(surplus)} </h4><span style={{textDecoration:"underline",marginLeft:"10px"}}>đ</span>
                  </div>
                  <Button type="primary" danger onClick={handleSendPayment}>
                    Yêu cầu thanh toán
                  </Button>
                </div>
              
              </div>
            </Col>
            <Col style={{paddingLeft: "20px"}} span={12}>
              <div>
                <h5>Tài khoản ngân hàng</h5>
                <div className="d-flex p-2 justify-content-between" style={{backgroundColor: "#fafafa", alignItems: "center"}}>
                  {shop !==null && (
                    <div>
                      {shop && shop.bank ? (
                      <div>
                       <div>
                         <div>STK: {shop.bankNumber}</div>
                         <div>Ngân hàng: {shop.bank}</div>
                         <div>CTK: {shop.bankName}</div>
                       </div>
                        <Button className="mt-2">Thay đổi</Button>
                      </div>
                      ) : (
                        <span
                          style={{cursor: "pointer", color: "blue"}}
                          onClick={handleClick}>Thêm tài khoản ngân hàng mới</span>)}
                    </div>
                  )}
                 
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <h4 style={{marginTop: "30px"}}>Các giao dịch gần đây</h4>
        <RangePicker/>
        <div className="float-end">
          <Button style={{marginRight: "20px"}}>Xuất</Button>
          <Button><MenuOutlined/></Button>
        </div>
        <div style={{marginTop: "20px"}}>
          <Tabs defaultActiveKey="1" type="card">
            <TabPane tab="Tất cả" key="1">
              <Empty/>
            </TabPane>
            <TabPane tab="Doanh thu" key="2">
              <Empty/>
            </TabPane>
            <TabPane tab="Rút tiền" key="3">
              <Empty/>
            </TabPane>
            <TabPane tab="Giá trị hoàn được ghi nhận" key="4">
              <Empty/>
            </TabPane>
            <TabPane tab="Điều chỉnh" key="5">
              <Empty/>
            </TabPane>
            <TabPane tab="Số dư TK" key="6">
              <Empty/>
            </TabPane>
          </Tabs>
        </div>
      
      </div>
    </>
  )
}

export default PortalBalance