import React, {useEffect, useState} from "react";
import {Card, Row, Col, Tabs, Table, Empty, Spin} from "antd";
import {Link} from "react-router-dom";
import moment from "moment";
import {RightOutlined} from "@ant-design/icons"
import Search from "antd/es/input/Search";
import {getShopPayment} from "../../../../service/ApiService/oderUserApi";
import {getShop} from "../../../../service/ApiService/shopApi";
import {useSelector} from "react-redux";

const {TabPane} = Tabs;

const PortalIncome = () => {
  const [selectedDate, setSelectedDate] = useState(moment())
  const user = useSelector(state => state.user);
  const [loading, setLoading] = useState(false)
  const [orders, setOrders] = useState([]);
  const [willPay, setWillPay] = useState([]);
  const [paymentWill, setPaymentWill] = useState(0);
  const [paid, setPaid] = useState([]);
  const [key, setKey] = useState(false);
  let start = moment().clone().startOf('weeks');
  let end = moment().clone().endOf('weeks');
  console.log("sssss", end)
  let total = 0;
  const dataSource = [];

  const columns = [
    {
      title: 'Đơn hàng',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Ngày thanh toán dự kiến',
      render: () => {
        return (
          <div>{moment().add(30, 'days').calendar()}</div>
        )
      },
      key: 'date',
    },
    {
      title: 'Trạng thái',
      render: (record) => {
        return (
          <div>{record.shopPayment === "unpaid" ? "sẽ thanh toán" : record.shopPayment === "pending" ? "Đang sử lý" : "Đã thanh toán"} </div>
        )
      },
      key: 'status',
    },
    {
      title: 'Số tiền',
      render: (record) => {
        return (
          <div>{Intl.NumberFormat().format(record.totalPrice)} đ</div>
        )
      },
      key: 'totalPrice',
    },
  ];
  const onSearch = value => console.log(value);

  useEffect(() => {
    getShopPayments()
  }, [])

  const getDataShop = async () => {
    try {
      return new Promise(async (resolve) => {
        const {data} = await getShop(user.id);
        resolve(data);
      });
    } catch (err) {
      window.location.replace("/");
    }
  };

  const getShopPayments = async () => {
    setLoading(true)
    const myShop = await getDataShop();
    const {data} = await getShopPayment({
      payment: true,
      shop: myShop.id,
      shopPayment: "unpaid"
    });
    if (data) {
      data.map(item => {
        total += parseInt(item.totalPrice);
        setPaymentWill(total)
      })
      console.log("000000", data);
      setPaid(data);
    }
    setLoading(false)

  }


  const callback = async (value) => {
    setLoading(true)
    const myShop = await getDataShop();
    const {data} = await getShopPayment({
      payment: true,
      shop: myShop.id,
      shopPayment: "unpaid"
    });
    setPaid(data);
    setLoading(false)
  }

  return (
    <div className="portal">
      <div className="mt-4">
        <Row gutter={16}>
          <Col xs={24} sm={24} lg={16} xl={16}>
            <Card className="mt-2" title="Tổng quan">
              <div className="d-flex">
                <div className="d-flex">
                  <div style={{borderRight: "1px solid #ccc"}}>
                    <div style={{marginRight: "40px"}}>
                      <h6>Sẽ thanh toán</h6>
                      <span>Tổng cộng</span>
                      <div className="d-flex">
                        <div
                          style={{fontSize: "20px", fontWeight: "500"}}>{Intl.NumberFormat().format(paymentWill)}</div>
                        <span style={{textDecoration: "underline", marginLeft: "5px"}}>đ</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div style={{width: "100%", marginLeft: "20px"}}>
                  <h6>Đã thanh toán</h6>
                  <div className="d-flex justify-content-between">
                    <div>
                      <span>Tuần này</span>
                      <div className="d-flex">
                        <div style={{fontSize: "20px", fontWeight: "500"}}>0</div>
                        <span style={{textDecoration: "underline", marginLeft: "5px"}}>đ</span>
                      </div>
                    </div>
                    <div>
                      <span>Tháng này</span>
                      <div className="d-flex">
                        <div style={{fontSize: "20px", fontWeight: "500"}}>0</div>
                        <span style={{textDecoration: "underline", marginLeft: "5px"}}>đ</span>
                      </div>
                    </div>
                    <div>
                      <span>Tổng cộng</span>
                      <div className="d-flex">
                        <div style={{fontSize: "20px", fontWeight: "500"}}>0</div>
                        <span style={{textDecoration: "underline", marginLeft: "5px"}}>đ</span>
                      </div>
                    </div>
                  </div>
                  <div className="float-end mt-2">
                    <Link to={""}>
                      <span>Số dư tài khoản apartment <RightOutlined style={{marginBottom: "4px"}}/></span>
                    </Link>
                  </div>
                </div>
              </div>
            </Card>
          </Col>
          <Col xs={24} sm={24} lg={8} xl={8}>
            <Card className="mt-2" title="Báo cáo thu nhập">
              <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                  height: 60,
                }}
                description={
                  <span style={{color: "#999"}}>
                      No Income Statment
                  </span>
                }
              >
              </Empty>
            </Card>
          </Col>
        </Row>
      </div>
      <div className="mt-4">
        <Row gutter={16}>
          <Col xs={24} sm={24} lg={18} xl={16}>
            <Card style={{position: "relative"}} title="Chi tiết">
              <Search style={{width: "300px", position: "absolute", top: "15px", right: "20px"}}
                      placeholder="Tìm kiếm đơn hàng  " onSearch={onSearch}/>
              <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Sẽ thanh toán" key="false">
                  <Spin spinning={loading}>
                    <Table dataSource={paid} columns={columns}/>
                  </Spin>
                </TabPane>
                <TabPane tab="Đã thanh toán" key="true">
                  <Spin spinning={loading}>
                    <Table dataSource={paid} columns={columns}/>
                  </Spin>
                </TabPane>
              </Tabs>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default PortalIncome;
