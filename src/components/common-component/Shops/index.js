import { Col, Rate, Row, Card } from "antd";
import { Link, NavLink } from "react-router-dom";
import "./shop.css";
import {useEffect, useState} from "react";
import {getProductShop} from "../../../service/ApiService/productsApi";

const ShopsLayOut = ({shop}) => {
  const [products,setProducts] = useState([]);
  useEffect(()=>{
    getDataShop();
  },[])
  const getDataShop = async () => {
    const {data} = await getProductShop({
      shop:shop.id,
      page:1,
      limit:3
    });
    setProducts(data.results)
  }
  return (
  
      <Col xxl={8} xl={8} xs={8} sm={0}>
        <Link to={`/shop-detail/${shop.id}`}>
          <div className="mb-2">
            <div>
              <Card
                className="d-flex "
                bordered={false}
                cover={
                  <img
                    className="mt-3"
                    style={{ width: "100px",padding:"10px", marginLeft: "14px" }}
                    alt="example"
                    src={shop.avatar}
                  />
                }
              >
                <div>
                  <div>{shop.name}</div>
                  <Rate
                    style={{ fontSize: "10px" }}
                    disabled
                    defaultValue={4}
                  />
                  <h6 style={{ color: "#0EAF5F", marginTop: "10px" }}>
                    GIAO NHANH
                  </h6>
                </div>
              </Card>
            </div>
            <div>
              <Card bordered={false}>
                <Row gutter={8}>
                  {products && products.map(item=>{
                    return (
                      <Col span={8}>
                        <Link to={`product/${item.id}`}>
                        <Card
                          hoverable
                          className="shop-cart"
                          bordered={false}
                          cover={
                            <img
                              style={{ height:"99.5px"}}
                              alt="example"
                              src={item.img}
                            />
                          }
                        >
                          <p className="ant-card-meta-title" style={{ fontSize: "13px" }}>{item.name}</p>
                          <span style={{ color: "rgb(255, 66, 78)" }}>{`${Intl.NumberFormat().format(item.price)} đ`}</span>
                        </Card>
                          </Link>
                      </Col>
                    )
                  })}
                </Row>
              </Card>
            </div>
          </div>
        </Link>
      </Col>
    
  );
};

export default ShopsLayOut;
