import React, {useState, useEffect} from "react"
import ShopsLayOut from "./index";
import {Card, Row} from "antd";
import CategoriesLayOut from "../Categories";
import { getCategory} from "../../../service/ApiService/categoryApi";
import { getShops } from "../../../service/ApiService/shopApi";

const ShopList = () => {
  const [listShop,setListShop] = useState([]);
  const [category, setCategory] = useState([]);

  useEffect( () => {
    getDataCategories();
    getListShop();
  }, [])

  const getListShop = async () =>{
    const {data} = await getShops({
      page:1,
      sortBy:"-order"
    })
    setListShop(data.results);
  }

  const getDataCategories = async () => {
    const {data} = await getCategory(1, 16);
    setCategory(data.results);
  };
  return (
    <>


      <div className="container mt-2">
        <h5 className="mt-4">Danh sách cửa hàng</h5>
        <Row gutter={8}>
          {listShop && listShop.map(item=>{
            return (
              <ShopsLayOut shop={item}/>
            )
          })}
        </Row>

      </div>
    </>
  )
}

export default ShopList