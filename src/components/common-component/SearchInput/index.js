import React, { useState } from "react";

import { SearchOutlined } from "@ant-design/icons";
import { Select,Button } from "antd";
import {
  searchProducts,
} from "../../../service/ApiService/productsApi";
import { Link } from "react-router-dom";

export const NavSearch = () => {
  const [value, setValue] = useState(null);
  const [options, setOptions] = useState([]);

  const onSelect = () => {
    setValue(null);
    setOptions([]);
  };

  const handleSearchProduct = async (value) => {
    try {
        await searchProducts(value).then((res) => {
          console.log(res);
          let options = [...res.data.results].map((product) => {
            return {
              value: product.id,
              name: product.name,
              data: product,
              label: (
                <Link to={`/product/${product.id}`}>
                  <div
                    className="search-list-item"
                    style={{
                      display: "flex",
                    }}
                  >
                    <div className="mr-3">
                      <img alt={product.name} width={"50px"} src={product.img} />
                    </div>
                    <span style={{ color: "#000" }} className="product-search">
                      {product.name}
                    </span>
                  </div>
                </Link>
              ),
            };
          });
          setOptions(value ? options : []);
        });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="d-flex" style={{ marginRight: "50px", marginTop: "5px" }}>
      <Select
        allowClear
        showSearch
        labelInValue
        showArrow={false}
        value={value}
        dropdownClassName="nav-search-dropdown"
        placeholder="Nhập nội dung tìm kiếm..."
        filterOption={false}
        onSearch={handleSearchProduct}
        onSelect={onSelect}
        style={{ width: "300px" }}
      >
        {options.map((option) => (
          <Select.Option
            key={option.value}
            value={option.label}
          >
          
          </Select.Option>
        ))}
      </Select>
      <Button style={{ borderRadius: "0", height: "32px" }}>
        <SearchOutlined />
      </Button>
    </div>
  );
};

export default NavSearch;
