import {Carousel} from "react-bootstrap"

const SliderPage = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          style={{maxHeight:"100%"}}
          className="d-block w-100"
          src="https://icms-image.slatic.net/images/ims-web/cc03b905-64ad-4840-b754-119e27a61f50.jpg"
          alt="First slide"
        />

      </Carousel.Item>
      <Carousel.Item>
        <img style={{maxHeight:"100%"}}
          className="d-block w-100"
          src="https://icms-image.slatic.net/images/ims-web/69670bf3-1c5e-410a-a2c9-de5fbe57b084.jpg"
          alt="Second slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          style={{minHeight:"100%"}}
          className="d-block w-100"
          src="https://icms-image.slatic.net/images/ims-web/5633ae78-7387-49d6-aa28-90d65d884ef8.jpg"
          alt="Second slide"
        />
      </Carousel.Item>
</Carousel>
  )
}
export default SliderPage