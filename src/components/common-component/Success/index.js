import {Link, useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import {checkPayment} from "../../../service/ApiService/vnPayApi";
import {updateUser} from "../../../service/ApiService/userApi";
import {useSelector} from "react-redux";
import {Button} from "antd";
import {getShop, updateShop} from "../../../service/ApiService/shopApi";

const PaymentSuccess = () => {
  const search = useLocation().search;
  const user = useSelector(state => state.user);
  const [status, setStatus] = useState(false);
  const [shop, setShop] = useState({})
  console.log(search)
  useEffect(() => {
    checkShop()
  }, [])

  const getDataShop = async () => {
    try {
      return new Promise(async (resolve) => {
        await getShop(user.id).then(res => {
          resolve(res.data);
          setShop(res.data);
        }).catch(err => {
          if (err) {
            window.location.replace('/')
          }
        })
      })
    } catch (err) {
      window.location.replace('/')
    }
  }

  const checkShop = async () => {
    console.log(Date.now())
    const myShop = await getDataShop()
    const {data} = await checkPayment(search);
    console.log(data)
    if (data.message === "success") {
      setStatus(true)
      await updateUser(user.id, {
        shop: true
      })
      await updateShop(myShop.id, {
        status: false,
        startDay:Date.now()
      });
    }
  }
  return (
    <div className="container">
      {status ? (
        <div className="text-center">
          <img style={{margin: "0 auto"}}
               src="https://cdn.dribbble.com/users/2185205/screenshots/7886140/02-lottie-tick-01-instant-2.gif"/>
          <div>Chúc mừng bạn đã thanh toán thành công của Apartment</div>
          <div className="mt-2">
            <Link to='/shop'><Button style={{backgroundColor: "#e03e3e", color: "#fff"}}>Cửa hàng của
              tôi</Button></Link>
          </div>
        </div>
      ) : (
        <></>
      )}
    </div>
  )
}

export default PaymentSuccess