import React from "react";
import {
  makeStyles,
  createMuiTheme,
} from "@material-ui/core/styles";

import "suneditor/dist/css/suneditor.min.css";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import ControlledEditor from "./controlledEditor";

const MyBlock = (props) => {
  return (
    <div
      style={{
        padding: 10,
        backgroundColor: "#ebebeb"
      }}
    >
      My Block content is:
      {props.children}
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "100ch"
    }
  }
}));
const defaultTheme = createMuiTheme();

Object.assign(defaultTheme, {
  overrides: {
    MUIRichTextEditor: {
      root: {
        marginTop: 2,
        width: "80%",
        marginBottom: 20,
        backgroundColor: "white",
        paddingBottom: 24
      },
      editor: {
        borderBottom: "1px solid white"
      }
    }
  }
});

export default function BasicTextFields() {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <ControlledEditor />
    </form>
  );
}
