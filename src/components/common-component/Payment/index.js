import {createPayment} from "../../../service/ApiService/vnPayApi";
import {Button, Card, Form, Input, Select} from "antd";
import {useLocation} from "react-router-dom";
import React, {useEffect} from "react";

const {Option, OptGroup} = Select;

function useQuery() {
  const {search} = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
}


const Payment = () => {
  let query = useQuery();
  const mode = query.get("mode")
  const publicUrl = new URL(process.env.PUBLIC_URL, window.location);
  console.log(publicUrl)
  const handleSubmit = async (values) => {
    console.log("vvvvvvv",values)
    if(mode === "shop"){
      const {data} = await createPayment({
        vnp_ReturnUrl: `${publicUrl.origin}/success`,
        language: 'vn',
        ...values

      });
      window.location.href = data;
    }else if (mode === "product"){
      const {data} = await createPayment({
        vnp_ReturnUrl: `${publicUrl.origin}/success-product`,
        language: 'vn',
        ...values
      });
      window.location.href = data;
    }


  }

  const valueForm = {
    amount: parseInt(query.get("amount")),
    orderDescription: query.get("description")
  }
  return (
    <div className="container">
      <Card title="Thanh toán">
        <Form
          name="vertical"
          layout="vertical"
          requiredMark={false}
          initialValues={valueForm}
          onFinish={handleSubmit}
        >
          <Form.Item label="Mã đơn hàng" name="orderDescription">
            <Input readOnly/>
          </Form.Item>

          <Form.Item label="Tổng tiền" name="amount">
            <Input readOnly/>
          </Form.Item>

          <Form.Item className={"mt-3"} label="Ngân hàng" name="bankCode">
            <Select defaultValue="Không chọn">
              <OptGroup label="Manager">
                <Option value='VNPAYQR'>Ngân hàng VNPAYQR</Option>
                <Option value='NCB'>Ngân hàng NCB</Option>
                <Option value='SCB'>Ngân hàng SCB</Option>
                <Option value='SACOMBANK'>Ngân hàng SACOMBANK</Option>
                <Option value='EXIMBANK'>Ngân hàng EXIMBANK</Option>
                <Option value='MSBANK'>Ngân hàng MSBANK</Option>
                <Option value='NAMABANK'>Ngân hàng NAMABANK</Option>
                <Option VISA='VISA'>Ngân hàng VISA</Option>
                <Option value='VNMART'>Ngân hàng VNMART</Option>
                <Option value='VIETINBANK'>Ngân hàng VIETINBANK</Option>
                <Option value='VIETCOMBANK'>Ngân hàng VIETCOMBANK</Option>
                <Option value='HDBANK'>Ngân hàng HDBANK</Option>
                <Option value='DONGABANK'>Ngân hàng Dong A</Option>
                <Option value='TPBANK'>Ngân hàng Tp Bank</Option>
                <Option value='OJB'>Ngân hàng OceanBank</Option>
                <Option value='BIDV'>Ngân hàng BIDV</Option>
                <Option value='TECHCOMBANK'>Ngân hàng Techcombank</Option>
                <Option value='VPBANK'>Ngân hàng VPBank</Option>
                <Option value='AGRIBANK'>Ngân hàng AGRIBANK</Option>
                <Option value='MBBANK'>Ngân hàng MBBank</Option>
                <Option value='ACB'>Ngân hàng ACB</Option>
                <Option value='OCB'>Ngân hàng OCB</Option>
                <Option value='SHB'>Ngân hàng SHB</Option>
                <Option value='IVB'>Ngân hàng IVB</Option>
              </OptGroup>
            </Select>
          </Form.Item>

          <Form.Item>
            <Button htmlType="submit">Thanh toán</Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}

export default Payment