import { Col, Row } from "antd";
import {
  FacebookOutlined,
  InstagramOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";

const FooterPage = () => {
  return (
    <div className="container">
      <Row>
        <Col xxl={6} xl={6} xs={12} lg={12}>
          <div style={{ fontSize: "12px" ,color:"rgb(234 234 234)"}}>
            <ul className="title-footer">
              <span style={{ fontWeight: 600 }}>
                APARTMENT - NỀN TẢNG MUA SẮM TRỰC TUYẾN HÀNG ĐẦU VIỆT NAM
              </span>
              <div className="mt-2">
                <p style={{ fontSize: "12px" ,color:"rgb(234 234 234)"}}>
                  Thành lập từ năm 2012,  là nền tảng thương mại điện tử
                  hàng đầu Đông Nam Á, tiên phong thúc đẩy sự phát triển tại khu
                  vực thông qua Thương mại & Công nghệ. Hiện nay, với nền tảng
                  logistics và hệ thống thanh toán lớn nhất khu vực,  trở
                  thành một phần trong đời sống của người tiêu dùng và hướng đến
                  mục tiêu phục vụ cho 300 triệu khách hàng trên toàn khu vực
                  Đông Nam Á vào năm 2030.
                </p>
                <p style={{ fontSize: "12px" }}>
                  Tại Việt Nam,  là nền tảng Thương mại điện tử quen thuộc
                  của hàng triệu người tiêu dùng bởi sự đa dạng hàng đầu về
                  chủng loại sản phẩm, ứng dụng công nghệ mua sắm và giải trí
                  thông minh cùng khả năng logistics
                </p>
              </div>
            </ul>
          </div>
        </Col>

        <Col xxl={6} xl={6} xs={12} lg={12}>
          <div style={{ fontSize: "12px" ,color:"rgb(234 234 234)"}}>
            <ul className="title-footer">
              <span style={{ fontWeight: 600 }}>Về Apartment</span>
              <div className="mt-2 my-off" >
                <li>Quy chế hoạt động Sàn GDTMĐT Bán hàng cùng Tiki</li>
                <li>Tuyển dụng</li>
                <li>Chính sách bảo mật thanh toán</li>
                <li>Chính sách bảo mật thông tin cá nhân</li>
                <li>Chính sách giải quyết khiếu nại</li>
                <li>Điều khoản sử dụng</li>
                <li>Bán hàng doanh nghiệp</li>
              </div>
            </ul>
          </div>
        </Col>

        <Col xxl={6} xl={6} xs={12} lg={12}>
          <div style={{ fontSize: "12px" ,color:"rgb(234 234 234)"}}>
            <ul className="title-footer">
              <span style={{ fontWeight: 600 }}>Hợp tác và liên kết</span>
              <div className="mt-2 my-off">
                <li>Quy chế hoạt động Sàn GDTMĐT Bán hàng cùng APARTMENT</li>
              </div>
            </ul>
            <ul style={{ fontWeight: 600 }}>
              Chứng nhận bởi
              <div className="d-flex mt-2">
                <img
                  width="50px"
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/bo-cong-thuong-2.png"
                />
                <img src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/bo-cong-thuong.svg" />
              </div>
            </ul>
          </div>
        </Col>
        <Col xxl={6} xl={6} xs={12} lg={12}>
          <div style={{ fontSize: "12px" ,color:"rgb(234 234 234)"}}>
            <ul className="title-footer">
              <span style={{ fontWeight: 600 }}>Kết nối với chúng tôi</span>
              <div className="mt-2">
                <div className="d-flex">
                  <FacebookOutlined
                    style={{ fontSize: "24px", marginRight: "10px" }}
                  />
                  <YoutubeOutlined
                    style={{ fontSize: "24px", marginRight: "10px" }}
                  />
                  <InstagramOutlined
                    style={{ fontSize: "24px", marginRight: "10px" }}
                  />
                </div>
              </div>
            </ul>
            <ul style={{ fontWeight: 600 }}>
              Tải ứng dụng trên điện thoại
              <div className="d-flex mt-2">
                <Row>
                  <Col xxl={12} xl={12} xs={12} lg={24}>
                    <img
                      style={{ marginRight: "10px" }}
                      width="100px"
                      src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/qrcode.png"
                    />
                  </Col>
                  <Col xxl={12} xl={12} xs={12} lg={24}>
                    <div style={{ flexDirection: "column" }}>
                      <img
                        width="100px"
                        src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/appstore.png"
                      />
                      <img
                        className="mt-2"
                        width="100px"
                        src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/playstore.png"
                      />
                    </div>
                  </Col>
                </Row>
              </div>
            </ul>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default FooterPage;
