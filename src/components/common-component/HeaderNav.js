import '../cusstom.css'
import {
  ShoppingOutlined,
  BellOutlined,
  SettingOutlined,
  LogoutOutlined,
  EyeOutlined,

} from '@ant-design/icons'
import {Menu, Dropdown, Avatar, Badge, Select, Divider, Empty, Button,} from "antd";
import {useEffect, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {logout} from "../../service/ApiService/userApi";
import {Nav, Navbar, NavDropdown} from "react-bootstrap";
import {useSelector} from "react-redux";
import {getCarts} from "../../service/ApiService/cartApi";
import logo from '../../assets/logoAP1.png'
import SearchInput from "./SearchInput";
import {getCategory} from "../../service/ApiService/categoryApi";
import {getMyNotifications} from "../../service/ApiService/notificationApi";
import moment from "moment";
import 'moment/locale/es'  // without this line it didn't work

const HeaderNav = () => {
  const [subNav, setShowNav] = useState(false);
  const [carts, setCarts] = useState([]);
  const [category, setCategory] = useState([]);
  const navigate = useNavigate();
  const [notifications, setNotifications] = useState([]);
  const user = useSelector(state => state.user)
  const ShowNavProfile = () => {
    setShowNav(!subNav);
  }


  const handleLogout = async (e) => {
    const refreshToken = JSON.parse(localStorage.getItem('REFRESH_TOKEN'));
    e.preventDefault()
    await logout({
      refreshToken: refreshToken.token
    })
      .then(() => {
        navigate('/login', {replace: true});
        localStorage.clear();
      }).catch(err => {
        notification.error({message: "Đã có lỗi sảy ra!"})
      })
  }

  useEffect(() => {
    getDataCart().then(() => {
    })
    getNotification();
  }, [user.id]);

  useEffect(() => {
    getCategoryPage();
  }, [])

  const getDataCart = async () => {
    if (user.id) {
      const {data} = await getCarts(user.id);
      console.log("ddddfaaaavvv", data)
      setCarts(data.results);
    }
  }

  const getCategoryPage = async () => {
    const {data} = await getCategory(1, 16);
    setCategory(data.results);
  }

  const getNotification = async () => {
    if (user.id) {
      const {data} = await getMyNotifications({recipient: user.id,sortBy:"-createdAt"});
      setNotifications(data.results);
    }
  }

  const menu = (<Menu>
    <Menu.Item key="0">
      <div className="nav-profile-header">
        <div className="d-flex">
          <Avatar size={40}
                  src={user.avatar}/>
          <div className="sub-nav-user" style={{paddingLeft: "1rem"}}>
            <h6>{user.fullName}</h6>
            <a href=""><span>@{user.username}</span></a>
          </div>
        </div>
      </div>
    </Menu.Item>
    <Menu.Item key="1">
      <Link to="purchase">
        <EyeOutlined style={{fontSize: "17px"}}/>
        <span style={{marginLeft: "10px", marginTop: "5px"}}>View profile</span>
      </Link>
    </Menu.Item>
    <Menu.Item key="2">
      <Link to="account-setting">
        <SettingOutlined style={{fontSize: "17px"}}/>
        <span style={{marginLeft: "10px"}}>Account setting</span>
      </Link>
    </Menu.Item>
    <Menu.Divider/>
    <Menu.Item key="3">
      <a href="" onClick={handleLogout}>
        <LogoutOutlined style={{fontSize: "17px"}}/>
        <span style={{marginLeft: "10px"}}>Logout</span>
      </a>
    </Menu.Item>
  </Menu>);

  const notification = (
    <Menu>
      <div style={{padding: "0px 20px 20px", overflowY: "auto", maxHeight: "400px"}}>
        <div className="mt-2" style={{borderBottom: "1px solid rgb(120 120 120 / 25%)", paddingBottom: "10px"}}>Thông
          Báo
        </div>
        {notifications.length > 0 ? (
          <div>
            {notifications.map(item => {
              return (
                <Link to={""} key={item.id}>
                  <div className="d-flex mt-3 w-100">
                    <Avatar size={40} src={item.img}/>
                    <div className="notification-content flex-column" style={{marginLeft: "10px", maxWidth: "300px"}}>
                      <div className="title d-flex justify-content-between w-100" style={{color: "rgba(0,0,0,.8)"}}>
                       <div> {item.title}</div>
                        <div style={{color:"rgb(120 120 120 / 85%)",fontSize:"12px"}}>{moment(item.createdAt).format('L')}</div>
                      </div>
                      <span style={{fontSize: "12px", color: "rgb(120 120 120 / 85%)"}}>{item.content}</span>
                    </div>
                  </div>

                </Link>
              )
            })}
          </div>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}/>
        )}


      </div>
    </Menu>
  );
  return (

    <Navbar style={{backgroundColor: "#00c2b4"}} expand="lg">
      <div className="container">
        <Navbar.Brand href="#">
          <div className="d-flex justify-content-center">
            <img width="50px" src={logo}/>
          </div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll"/>
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{maxHeight: '100px', backgroundColor: "#00c2b4"}}
            navbarScroll
          >
            <Nav.Link href="/">Trang chủ</Nav.Link>
            <Nav.Link href="/shop-list">Cửa hàng</Nav.Link>
            <NavDropdown title="Danh mục">
              {category && category.map(item => {
                return (
                  <div key={item.id}>
                    <NavDropdown.Item href={`/category/${item.id}`}
                                      style={{fontSize: "13px"}}>{item.name}</NavDropdown.Item>
                  </div>
                )
              })}
            </NavDropdown>
            {user.shop ? (<Nav.Link href="/shop">
              Cửa hàng của tôi
            </Nav.Link>) : (<Nav.Link href="/register-shop">
              Kênh bán hàng
            </Nav.Link>)}
          </Nav>
          <div className="d-flex" style={{maxWidth:"100%"}}>
            <SearchInput/>

          </div>
          <div className="mt-3">
            <div className="d-flex">
              <Dropdown className="mt-2" overlay={notification} trigger={['hover']}>
                <Badge count={notifications.length}>
                  <BellOutlined
                    style={{fontSize: "26px", color: "#fff", cursor: "pointer"}}/>
                </Badge>
              </Dropdown>
              <Nav.Link href="/cart">
                <Badge count={carts.length}>
                  <ShoppingOutlined style={{fontSize: "26px", color: "#fff"}}/>
                </Badge>
              </Nav.Link>

              {user.fullName ? (
                <Dropdown className="profile" overlay={menu} trigger={['click']}>
            <span style={{cursor: "pointer"}} className="ant-dropdown-link"
                  onClick={e => e.preventDefault()}>
              <Avatar
                size={40}
                onClick={ShowNavProfile}
                style={{position: "relative"}}
                src={user.avatar}/>
            </span>
                </Dropdown>
              ) : (<Link to="/login"><Button className="mt-1">Đăng nhập</Button></Link>)}
            </div>
          </div>
        </Navbar.Collapse>
      </div>
    </Navbar>


    //   <div className="top_header " style={{backgroundColor: "rgb(255 255 255)", height: "80px"}}>
    //   <Row>
    //     <Col span={24}>
    //
    //       <Row>
    //         <Col span={3}>
    //           <Navbar.Brand href="/">
    //             <div className="logo d-flex justify-content-center">
    //               <img width="50px" src={logo}/>
    //             </div>
    //           </Navbar.Brand>
    //         </Col>
    //
    //         <Col span={6}>
    //           <div className="search " style={{marginTop: "20px"}}>
    //             <SearchInput/>
    //           </div>
    //         </Col>
    //
    //         <Col span={12}>
    //           <div className="d-flex justify-content-center mt-2">
    //             <NavLink to="/" style={{marginRight: "20px", fontSize: "13px", color: "black"}}>
    //               <div className="d-flex">
    //                 <HomeOutlined style={{fontSize: "13px", marginRight: "5px", marginTop: "18px"}}/>
    //                 <span className="mt-3">Trang chủ</span>
    //               </div>
    //             </NavLink>
    //
    //             {user.shop ? (<NavLink to="/shop" style={{marginRight: "20px", fontSize: "13px", color: "black"}}>
    //               <div className="d-flex">
    //                 <ShopOutlined style={{fontSize: "13px", marginRight: "5px", marginTop: "18px"}}/>
    //                 <span className="mt-3">My shop</span>
    //               </div>
    //             </NavLink>) : (<NavLink to="/register-shop"
    //                                     style={{marginRight: "20px", fontSize: "13px", color: "black"}}>
    //               <div className="d-flex">
    //                 <ShopOutlined style={{fontSize: "13px", marginRight: "5px", marginTop: "18px"}}/>
    //                 <span className="mt-3">Đăng kí shop</span>
    //               </div>
    //             </NavLink>)}
    //
    //             <NavLink to="/posts" style={{marginRight: "20px", fontSize: "13px", color: "black"}}>
    //               <div className="d-flex">
    //                 <FormOutlined style={{fontSize: "13px", marginRight: "5px", marginTop: "18px"}}/>
    //                 <span className="mt-3">Bài viết</span>
    //               </div>
    //             </NavLink>
    //
    //             <NavLink to="" style={{marginRight: "20px", fontSize: "13px", color: "black"}}>
    //               <div className="d-flex">
    //                 <BulbOutlined style={{fontSize: "13px", marginRight: "5px", marginTop: "18px"}}/>
    //                 <span className="mt-3">Góp ý</span>
    //               </div>
    //             </NavLink>
    //           </div>
    //         </Col>
    //         <Col span={3}>
    //
    //           <div className="d-flex"
    //                style={{
    //                  marginLeft: "30px", paddingTop: "18px", justifyContent: "flex-end", marginRight: "20px"
    //                }}>
    //
    //             <Dropdown className="mt-2" overlay={notification} trigger={['click']}>
    //               <BellOutlined
    //                 style={{fontSize: "20px", marginRight: "20px"}}/>
    //             </Dropdown>
    //
    //             <NavLink className="mt-2" to="cart" style={{marginRight: "20px"}}>
    //               <Badge count={carts.length}>
    //                 <ShoppingOutlined style={{fontSize: "20px",}}/>
    //               </Badge>
    //             </NavLink>
    //
    //             <Dropdown className="profile" overlay={menu} trigger={['click']}>
    //                       <span style={{cursor: "pointer"}} className="ant-dropdown-link"
    //                             onClick={e => e.preventDefault()}>
    //                         <Avatar
    //                           size={40}
    //                           onClick={ShowNavProfile}
    //                           style={{position: "relative"}}
    //                           src={user.avatar}/>
    //                       </span>
    //             </Dropdown>
    //           </div>
    //         </Col>
    //       </Row>
    //
    //
    //    </Col>
    //  </Row>
    // </div>
  )
}
export default HeaderNav