import { Avatar, Badge, Layout, Menu, notification,  Modal,
  Row, Dropdown, Empty } from "antd";
import { Link, Outlet, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { getShop, updateShop } from "../service/ApiService/shopApi";
import { useSelector } from "react-redux";
import { logout } from "../service/ApiService/userApi";
import logo from '../assets/logoAP1.png'
import {
  InboxOutlined,
  BorderRightOutlined,
  SnippetsOutlined,
  BellOutlined,
  TagOutlined,
  BarChartOutlined,
  FundOutlined, LogoutOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { getMyNotifications } from "../service/ApiService/notificationApi";

const { Content, Sider } = Layout;

const { SubMenu } = Menu;
const LayoutShop = () => {
  const rootSubmenuKeys = ['sub1', 'sub2', 'sub3'];
  const [openKeys, setOpenKeys] = React.useState(['sub1', 'sub2', 'sub4']);
  const [deviceToken, setDeviceToken] = React.useState(localStorage.getItem('deviceToken'))
  const [shop, setShop] = useState({});
  const [notifications, setNotifications] = useState([]);

  const user = useSelector(state => state.user)
  const navigate = useNavigate();

  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  useEffect(() => {
    checkDeviceToken()
    checkPaymentShop()
    getNotification()
  }, [])



  const getDataShop = async () => {
    try {
      return new Promise(async (resolve) => {
        await getShop(user.id).then(res => {
          resolve(res.data);
          setShop(res.data);
        }).catch(err => {
          if (err) {
            window.location.replace('/')
          }
        })
      })
    } catch (err) {
      window.location.replace('/')
    }
  }

  const checkDeviceToken = async () => {
    const myShop = await getDataShop()
    if (myShop.deviceToken !== deviceToken || myShop.deviceToken == null) {
      const { data } = await updateShop(myShop.id, {
        deviceToken: deviceToken
      })
    }
  }

  const getNotification = async () => {
    const myShop = await getDataShop();
    const { data } = await getMyNotifications({ shop: myShop.id });
    setNotifications(data.results);
  }

  const checkPaymentShop = async () => {
    const myShop = await getDataShop();
    const startDay = moment(myShop.startDay).format();
    const startFormat = moment(myShop.startDay).format("x")
    const endDay = moment(startDay).add(30, 'days').format("x")

    const today = moment().format('x')
    console.log("eeeeeeeeee", endDay)
    if (today >= endDay) {
      console.log("Đã hết hạn");
      await updateShop(myShop.id, {
        status: true
      });
      await navigate('/expire-shop', { replace: true })
    } else {
      console.log("Chưa hết hạn");
    }
    if(myShop.lock === true){
      console.log("ssssssss", myShop.lock)

        Modal.error({
          title: 'Cửa hàng của bạn đã bị khóa!',
          content: <div className="text-center">
            <div>Cửa hàng của bạn đã vi phạm chính sách hệ thống</div>
            <div>Chi tiết vui lòng liên hệ email: chungcubb1@gmail.com để được giải đáp.</div>
          </div>,
          onOk: ()=>{
            window.location.replace('/')
          },
          okText: <div className="d-flex justify-content-center">Quay về trang chủ</div>
        });
    }

  }

  const menu = (
    <Menu>
      <Menu.Item icon={<FundOutlined />}>
        <Link to="profile-shop">
          Hồ sơ shop
        </Link>
      </Menu.Item>
      <Menu.Item icon={<LogoutOutlined />}>
        <Link to={""}>
          Đăng xuất
        </Link>
      </Menu.Item>
    </Menu>
  );

  const notification = (
    <Menu>
      <div style={{ padding: "0px 20px 20px", overflowY: "auto", maxHeight: "400px" }}>
        <div className="mt-2" style={{ borderBottom: "1px solid rgb(120 120 120 / 25%)", paddingBottom: "10px" }}>Thông
          Báo
        </div>
        {notifications.length > 0 ? (
          <div>
            {notifications.map(item => {
              return (
                <Link to={""}>
                  <div className="d-flex mt-3">
                    <Avatar size={40} src={item.img} />
                    <div className="notification-content flex-column" style={{ marginLeft: "10px", maxWidth: "300px" }}>
                      <div className="title" style={{ color: "rgba(0,0,0,.8)" }}>{item.title}</div>
                      <span style={{ fontSize: "12px", color: "rgb(120 120 120 / 85%)" }}>{item.content}</span>
                    </div>
                  </div>
                </Link>
              )
            })}
          </div>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}


      </div>
    </Menu>
  );

  return (
    <Layout>
      <Sider
        style={{ background: "#fff" }}
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div style={{ backgroundColor: "rgb(26, 148, 255)", height: "70px" }}>
          <Link to={"/shop"}>
            <div style={{ padding: "15px" }} className="d-flex justify-content-center">
              <img style={{ height: "40px" }} src={logo} />
              <Link to="/"><h5 style={{ marginLeft: "20px", color: "antiquewhite", fontSize: "18px" }}>Apartment Shop</h5>
              </Link>
            </div>
          </Link>
        </div>
        <Menu style={{ paddingTop: "20px" }} mode="inline" openKeys={openKeys} onOpenChange={onOpenChange}>
          <SubMenu key="sub1" icon={<InboxOutlined />} title="Quản lý sản phẩm">
            <Menu.Item key="1"><Link to="/shop/addproduct-shop">Thêm sản phẩm</Link></Menu.Item>
            <Menu.Item key="2"><Link to="/shop/product-shop">Tất cả sản phẩm</Link></Menu.Item>
            <Menu.Item key="3"><Link to="">Sản phẩm vi phạm</Link></Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<SnippetsOutlined />} title="Quản lý đơn hàng">
            <Menu.Item key="5"><Link to="/shop/order-shop">Tất cả</Link></Menu.Item>

          </SubMenu>
          <SubMenu key="sub3" icon={<TagOutlined />} title="Kênh bán hàng">
            <Menu.Item key="8"><Link to="/shop/voucher-shop">Mã giảm giá của tôi</Link></Menu.Item>
          </SubMenu>
          <SubMenu key="sub4" icon={<BarChartOutlined />} title="Tài chính">
            <Menu.Item key="9"><Link to="/shop/portal/income">Doanh thu</Link></Menu.Item>
            <Menu.Item key="10"><Link to="/shop/portal/balance">Số dư tài khoản</Link></Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout>
        <div className="header-shop">
          <Row style={{ paddingTop: "20px" }}>
            <div className="container">
              <div className="d-flex float-end" style={{ marginRight: "20px" }}>
                <Dropdown overlay={menu}>
                  <div className="d-flex" style={{ marginRight: "20px" }}>
                    <Avatar src={shop.avatar} />
                    <Link to="profile-shop">
                      <div className="mt-2" style={{ marginLeft: "10px", color: "#fff" }}>{shop.name}</div>
                    </Link>
                  </div>
                </Dropdown>

                <BorderRightOutlined className="mt-2" style={{ fontSize: "20px", marginRight: "20px", color: "#fff" }} />
                {/*<Badge className="mt-2" count={3}>*/}
                {/*  <BellOutlined style={{fontSize: "20px", color: "#fff"}}/>*/}
                {/*</Badge>*/}
                <Dropdown className="mt-2" overlay={notification} trigger={['hover']}>
                  <Badge count={notifications.length}>
                    <BellOutlined
                      style={{ fontSize: "20px", color: "#fff", cursor: "pointer" }} />
                  </Badge>
                </Dropdown>
              </div>
            </div>
          </Row>
        </div>
        <Content style={{ margin: '24px 16px 0' }}>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            <Outlet />
          </div>
        </Content>
      </Layout>
    </Layout>
  )


}

export default LayoutShop