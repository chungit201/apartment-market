import './cusstom.css'
import {Col, Layout, Menu, Row} from 'antd';
import {Link, Outlet} from "react-router-dom";
import HeaderNav from "../components/common-component/HeaderNav";
import {useSelector} from "react-redux";
import FooterPage from "../components/common-component/FooterPage";

const {SubMenu} = Menu;
const {Content, Footer, Sider} = Layout;

const LayoutWeb = () => {
  const user = useSelector(state => state.user);
  return (
    <Layout style={{position: "relative"}}>
      <HeaderNav/>
      <Content>
        <Layout style={{background: "#ffffff"}}>
          <Content style={{paddingTop: "30px"}}>
            <Row>
              <Col span={24}>
                <div className="content" style={{minHeight: 360}} >
                  <Outlet/>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Content>
      <Footer className="mt-2" style={{backgroundColor:"#00c2b4"}}>
        <FooterPage/>
      </Footer>
    </Layout>
  )
}
export default LayoutWeb