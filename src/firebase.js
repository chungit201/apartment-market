import firebase from 'firebase/app';
import "firebase/messaging";
import "firebase/storage"
const firebaseConfig = {
  apiKey: "AIzaSyAkqTJNjeSSL6NJQL7vvDZQ_TJF8d5U0Po",
  authDomain: "apartment-2a243.firebaseapp.com",
  projectId: "apartment-2a243",
  storageBucket: "apartment-2a243.appspot.com",
  messagingSenderId: "112905749027",
  appId: "1:112905749027:web:4fe411f1e1425a84336796",
  measurementId: "G-YXQ3PGM3DD"
};

firebase.initializeApp(firebaseConfig)
export const storage = firebase.storage()
export { firebase as default};