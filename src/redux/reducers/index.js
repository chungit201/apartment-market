import { combineReducers } from 'redux';
import User from './User'

const reducers = combineReducers({
  user: User,
});

export default reducers;