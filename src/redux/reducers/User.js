import { SET_USER_DATA } from "../constants/User";

const initialState = {
  id: "",
  address: "",
  username: "",
  fullName: "",
  role: "",
  phone: ""
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_USER_DATA:
      state = {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
}

export default reducer;
