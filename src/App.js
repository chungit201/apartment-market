import "bootstrap/dist/css/bootstrap.min.css";
import "antd/dist/antd.min.css";
import "./App.css";
import LayoutWeb from "./layout/LayoutWeb";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import HomePage from "./app-views/home/HomePage";
import "./App.css";
import MarketPage from "./app-views/markets/MarketPage";
import AccountSetting from "./app-views/profile/accuont/AccountSetting";
import LoginPage from "./auth-views/login";
import RegisterForm from "./auth-views/reigister/RegisterForm";
import index from "./redux/store";
import {Provider} from "react-redux";
import { createWrapper } from "next-redux-wrapper";
import RegisterShop from "./auth-views/registerShop";
import OrderShop from "./app-views/shops/order";
import ProductShop from "./app-views/shops/ListProduct";
import ProductDetail from "./app-views/product-detail/ProductDetail";
import Cart from "./app-views/cart/Cart";
import LoadData from "./components/assets/load-data/LoadData";
import AddProduct from "./app-views/shops/AddProduct";
import EditProductShop from "./app-views/shops/EditProduct";
import {useEffect} from "react";
import firebase from "./firebase";
import Checkout from "./app-views/checkout/checkout";
import PostPage from "./app-views/posts";
import Detail from "./components/common-component/Categories/CategoryDetail/Detail1.js";
import MyOder from "./app-views/my-oder";
import Payment from "./components/common-component/Payment";
import PaymentSuccess from "./components/common-component/Success";
import LayoutShop from "./layout/LayoutShop";
import ShopPage from "./app-views/shops";
import Purchase from "./app-views/profile/accuont/Purchase";
import Voucher from "./app-views/profile/accuont/Voucher";
import NotificationActivity from "./app-views/profile/accuont/Notification/Activity";
import NotificationOrder from "./app-views/profile/accuont/Notification/Order";
import NotificationPromotion from "./app-views/profile/accuont/Notification/Promotion";
import NotificationRating from "./app-views/profile/accuont/Notification/Rating";
import NotificationWallet from "./app-views/profile/accuont/Notification/Wallet";
import ProfileShop from "./app-views/shops/Profile/ProfileShop";
import VoucherPage from "./app-views/voucher-page";
import AddVoucher from "./app-views/voucher-page/voucher/AddVoucher";
import CheckPaymentProduct from "./components/common-component/CheckPaymentProduct";
import ShopList from "./components/common-component/Shops/ShopList";
import PortalIncome from "./components/common-component/portal/Income/PortalIncome";
import ShopDetail from "./app-views/shops/ShopDetail";
import PortalBalance from "./components/common-component/portal/Balance/PortalBalance";
import ExpireShop from "./app-views/expire-shop";
import ResetPassword from "./auth-views/resetPassword";
import ScrollToTop from "./components/util-components/ScrollTop";
function PrivateRoute({ children, isAuthenticated }) {
  return isAuthenticated ? children : <Navigate to="/login" />;
}
function App() {
  const token = localStorage.getItem("ACCESS_TOKEN");
   useEffect(()=>{
     getTokens()
   },[])
  const getTokens = async () => {
    try {
      const messaging = firebase.messaging();
      await messaging.requestPermission();
      const token = await messaging.getToken();
      console.log('tokens:', token);
      localStorage.setItem('deviceToken', token)
      return token;
    } catch (error) {
      console.error(error);
    }
  }
  
  return (
    <Provider store={index}>
      <div className="App">
        <BrowserRouter>
          {/*------LayOut ----------------*/}
         <LoadData>
           <ScrollToTop />
           <Routes>
             <Route path="/*" element={<LayoutWeb />}>
               <Route index element={<HomePage />}/>
               <Route path="expire-shop" element={<ExpireShop/>}/>
               <Route path="success" element={<PrivateRoute isAuthenticated={token}><PaymentSuccess /></PrivateRoute>}/>
               <Route path="payment" element={<PrivateRoute isAuthenticated={token}><Payment /></PrivateRoute>}/>
               <Route path="my-order" element={<PrivateRoute isAuthenticated={token}><MyOder /></PrivateRoute>}/>
               <Route path="category/:id" element={<Detail />}/>
               <Route path="posts" element={<PrivateRoute isAuthenticated={token}><PostPage /></PrivateRoute>}/>
               <Route path="markets" element={<PrivateRoute isAuthenticated={token}><MarketPage /></PrivateRoute>}/>
               <Route path="product/:id" element={<ProductDetail />}/>
               <Route path="cart" element={<PrivateRoute isAuthenticated={token}><Cart /></PrivateRoute>}/>
               <Route path="account-setting" element={<PrivateRoute isAuthenticated={token}><AccountSetting /></PrivateRoute>}/>
               <Route path="checkout" element={<PrivateRoute isAuthenticated={token}><Checkout /></PrivateRoute>}/>
               <Route path="purchase" element={<PrivateRoute isAuthenticated={token}><Purchase /></PrivateRoute>}/>
               <Route path="voucher-wallet" element={<PrivateRoute isAuthenticated={token}><Voucher /></PrivateRoute>}/>
               <Route path="notification-activity" element={<PrivateRoute isAuthenticated={token}><NotificationActivity /></PrivateRoute>}/>
               <Route path="notification-order" element={<PrivateRoute isAuthenticated={token}><NotificationOrder /></PrivateRoute>}/>
               <Route path="notification-promotion" element={<PrivateRoute isAuthenticated={token}><NotificationPromotion /></PrivateRoute>}/>
               <Route path="notification-rating" element={<PrivateRoute isAuthenticated={token}><NotificationRating /></PrivateRoute>}/>
               <Route path="notification-wallet" element={<PrivateRoute isAuthenticated={token}><NotificationWallet /></PrivateRoute>}/>
               <Route path="success-product" element={<PrivateRoute isAuthenticated={token}><CheckPaymentProduct /></PrivateRoute>}/>
               <Route path="shop-list" element={<ShopList />}/>
               <Route path="shop-detail/:id" element={<ShopDetail />}/>


             </Route>

             <Route path="shop/*" element={<LayoutShop />}>
               <Route index element={<PrivateRoute isAuthenticated={token}><ShopPage /></PrivateRoute>}/>
               <Route path="order-shop" element={<PrivateRoute isAuthenticated={token}><OrderShop /></PrivateRoute>}/>
               <Route path="product-shop" element={<PrivateRoute isAuthenticated={token}><ProductShop /></PrivateRoute>}/>
               <Route path="addproduct-shop" element={<PrivateRoute isAuthenticated={token}><AddProduct /></PrivateRoute>}/>
               <Route path="editproduct-shop/:id" element={<PrivateRoute isAuthenticated={token}><EditProductShop /></PrivateRoute>}/>
               <Route path="voucher-shop" element={<PrivateRoute isAuthenticated={token}><VoucherPage /></PrivateRoute>}/>
               <Route path="profile-shop" element={<PrivateRoute isAuthenticated={token}><ProfileShop /></PrivateRoute>}/>
               <Route path="add-voucher" element={<PrivateRoute isAuthenticated={token}><AddVoucher /></PrivateRoute>}/>
               <Route path="add-voucher" element={<PrivateRoute isAuthenticated={token}><AddVoucher /></PrivateRoute>}/>
               <Route path="portal/income" element={<PrivateRoute isAuthenticated={token}><PortalIncome /></PrivateRoute>}/>
               <Route path="portal/balance" element={<PrivateRoute isAuthenticated={token}><PortalBalance /></PrivateRoute>}/>
             </Route>

             <Route path="/register-shop" element={<PrivateRoute isAuthenticated={token}><RegisterShop /></PrivateRoute>}/>
             {/*---------------------------------------*/}
             <Route path="login" element={<LoginPage />} />
             <Route path="/reset-password" element={<ResetPassword />} />
             <Route path="register" element={<RegisterForm />} />
        
           </Routes>
         </LoadData>
        </BrowserRouter>
      </div>
    </Provider>
  );
}
const makeStore = () => index;
const wrapper = createWrapper(makeStore);
export default wrapper.withRedux(App);
