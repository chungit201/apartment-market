export const getParams = (data) => {
  return Object.keys(data).reduce((param, field) => {
    return param += `${field}=${data[field]}&`
  }, '?');
}
