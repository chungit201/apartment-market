import instance from "./instance";
import {getParams} from "../Utils";

export const addShop = (data) => {
  let url = "/shops/add";
  return instance.post(url, data);
};

export const getShops = (data) => {
  const param = getParams(data);
  let url = '/shops' + (param ? param : '');
  return instance.get(url);
}

export const getShop = (userId) => {
  let url = `/shops/${userId}`;
  return instance.get(url);
};

export const getShopId = (shopId) => {
  let url = `/shops/shop-info/${shopId}`;
  return instance.get(url);
}

export const updateShop = (shopId, data) => {
  let url = `/shops/update/${shopId}`;
  return instance.post(url, data)
};

export const sendPaymentShop = (data) => {
  const param = getParams(data);
  let url = '/payment-shop/add';
  return instance.post(url, data)
}
