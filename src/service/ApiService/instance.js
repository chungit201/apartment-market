import axios from "axios";
const instance = axios.create({
  baseURL:" https://apartment-001.herokuapp.com/api",
  // baseURL:"http://localhost:4000/api",
  headers:{
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const TOKEN_PAYLOAD_KEY = 'Authorization'

instance.interceptors.request.use(async (config) => {
  const tokens = await JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
  if (tokens) {
    config.headers[TOKEN_PAYLOAD_KEY] = `Bearer ${tokens.token}`
  }
  return config
})

export default instance;