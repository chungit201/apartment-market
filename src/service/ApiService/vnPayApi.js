import instance from "./instance";

export const createPayment = (data) => {
  let url = "/vnPay/create_payment_url";
  return instance.post(url, data)
}

export const checkPayment = (query) => {
  let url = `/vnPay/vnpay_ipn${query}`;
  return instance.get(url)
}