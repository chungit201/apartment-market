import instance from "./instance";
import {getParams} from "../Utils";

export const getProducts = (page,limit) => {
  let url = `/products?page=${page}&limit=${limit}&action=active&sortBy=-createdAt`;
  return instance.get(url)
}

export const searchProducts = (name) =>{
  let url = `/products?name=${name}`;
  return instance.get(url)
}

export const addProduct = (data) => {
  let url = `/products/add`;
  return instance.post(url,data)
}

export const geTotalView = (productId)  => {
  let url =`/views/product/${productId}`;
  return instance.get(url);
}

export const getProductSale = () =>{
  let url ="/products?sale=true&action=active";
  return instance.get(url);
}

export const getProduct = (id) => {
  let url = `/products/${id}`;
  return instance.get(url)
}

export const getCateProducts = (data) => {
  const param = getParams(data);
  let url = '/products' + (param ? param : '');
  return instance.get(url)
}
export const getProductSortBy = (category,page,limit,sortBy) => {
  let url = `/products?categories=${category}&page=${page}&limit=${limit}&sortBy=${sortBy}`;
  return instance.get(url)
}

export const listProductTime = (type,page,limit) =>{
  let url =`/products?type=${type}&page=${page}&limit=${limit}`;
  return instance.get(url);
}

export const getProductShop = (data) =>{
  const param = getParams(data);
  let url = '/products' + (param ? param : '');
  return instance.get(url);
}
export const removeProduct = (id) => {
  const url = "/products/delete/" + id;
  return instance.post(url);
};
export const read = (id) => {
  const url = "/products/" + id;
  return instance.get(url);
};
export const updateProduct = ( id,product) => {
  const url = "/products/update/" + id;
  return instance.post(url, product);
};

export const getProductViewTop = (data) =>{
  const url = `/views?page=${data.page}&limit=${data.limit}&action=${data.action}&start=${data.start}&end=${data.end}&sortBy=${data.sortBy}`;
  return instance.get(url,data);
}

export const addViewProduct = (data) =>{
  const url = "/views/add";
  return instance.post(url,data);
}

export const addRating = (data) =>{
  const url = "/products/rate";
  return instance.post(url,data);
}

