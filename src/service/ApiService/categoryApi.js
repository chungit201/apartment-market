import instance from "./instance";

export const getCategory = (page,limit) => {
  let url = `/categories?page=${page}&limit=${limit}`;
  return instance.get(url)
}

export const getCategories = (id) => {
  let url = `/categories/${id}`;
  return instance.get(url)
}

export const getCategoryProduct = (categories,page,limit) => {
  let url = `/products?action=active&categories=${categories}&page=${page}&limit=${limit}`;
  return instance.get(url)
}