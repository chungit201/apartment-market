import instance from "./instance";

export const addComment = (data) => {
  let url = "/comments/add";
  return instance.post(url, data)
}

export const getCommentsProduct = (productId) =>{
  let url = `/comments?product=${productId}`;
  return instance.get(url);
}
