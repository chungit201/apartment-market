import instance from "./instance";

export const addVoucher = (data) => {
  let url = "/vouchers/add";
  return instance.post(url, data)
}

export const getVoucherAllShop = (type) => {
  let url = `/vouchers?type=${type}`;
  return instance.get(url)
}


export const getVouchers = (shopId) => {
  let url = `/vouchers?shop=${shopId}`;
  return instance.get(url)
}

export const getVoucher = (data) => {
  let url = "/vouchers/use";
  return instance.post(url, data)
}

export const addVoucherUser = (data) => {
  let url = "/voucher-users/add";
  return instance.post(url, data)
}

export const updateVoucher = (voucherId,data) =>{
  let url = `/vouchers/update/${voucherId}`;
  return instance.post(url,data)
}