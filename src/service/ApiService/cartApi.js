import instance from "./instance";

export const addToCart = (data) => {
  let url = "/carts/add";
  return instance.post(url, data)
}

export const getCarts = (user) => {
  let url = `/carts?user=${user}`;
  return instance.get(url)
}

export const deleteCarts = (cartId) => {
  let url = `/carts/delete/${cartId}`;
  return instance.post(url)
}