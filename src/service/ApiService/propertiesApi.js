import instance from "./instance";
import {getParams} from "../Utils";

export const getProperties = () => {
  let url = `/properties`;
  return instance.get(url)
}
