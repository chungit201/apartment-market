import instance from "./instance";
import {getParams} from "../Utils";

export const addMyOder = (data) => {
  let url = `/oders/add`;
  return instance.post(url, data)
}

export const getShopOrder = (shopId,page,limit,) => {
  let url = `/oders?shop=${shopId}&page=${page}&limit=${limit}$&sortBy=-createdAt`;
  return instance.get(url)
}

export const getShopFilter = (data) =>{
  const param = getParams(data);
  let url = `/oders` + (param ? param : '');
  return instance.get(url)
}

export const getShopOrderSuccess = (shopId,page,limit,) => {
  let url = `/oders?shop=${shopId}&status=true&page=${page}&limit=${limit}$&sortBy=-createdAt`;
  return instance.get(url)
}

export const getShopOrderStatus = (shopId,page,limit,status) => {
  let url = `/oders?shop=${shopId}&page=${page}&limit=${limit}&status=${status}&${status}&sortBy=-createdAt`;
  return instance.get(url)
}


export const getShopPayment = (data) => {
  const param = getParams(data);
  let url = '/oders/shopPayment' + (param ? param : '') ;
  return instance.get(url)
}

export const getDataOder = (userId) => {
  let url = `/oders?user=${userId}`;
  return instance.get(url)
}

export const getMyOder = (userId, status) => {
  let url = `/oders?user=${userId}&status=${status}`;
  return instance.get(url)
}

export const updateOder = (oderId,body) => {
  let url = `/oders/update/${oderId}`;
  return instance.post(url,body)
}

export const searchCodeOrder = (code) => {
  let url = `/oders?code=${code}`;
  return instance.get(url)
}

export const getTimeOrders = (start,end) => {
  let url = `/oders?start=${start}&end=${end}`;
  return instance.get(url)
}

