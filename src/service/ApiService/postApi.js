import instance from "./instance";

export const getPosts = (page,limit) => {
  let url = `/posts?page=${page}&limit=${limit}?sortBy=createdAt`;
  return instance.get(url)
}

export const addPosts = (data) => {
  let url = `/posts/add`;
  return instance.post(url,data)
}


