import instance from "./instance";
import {getParams} from "../Utils";

export const addSingle = (data) => {
  let url = "/singles/add";
  return instance.post(url, data);
};
export const getSingle = (singleId) => {
  let url = `/singles/${singleId}`;
  return instance.get(url);
}