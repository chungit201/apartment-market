import instance from "./instance";
import {getParams} from "../Utils";

export const addNotification = (data) => {
  let url = `/notifications/add`;
  return instance.post(url, data)
}

export const getMyNotifications = (data) => {
  const param = getParams(data);
  let url = '/notifications' + (param ? param : '') ;
  return instance.get(url)
}