import instance from "./instance";

export const sendToOne = (data) => {
  let url = "/send-notifications/sendToOne";
  return instance.post(url, data)
}
