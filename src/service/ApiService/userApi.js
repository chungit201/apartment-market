import instance from "./instance";

export const register = (user) => {
  let url = "/auth/register";
  return instance.post(url, user)
}

export const login = (user) => {
  let url = "/auth/login";
  return instance.post(url, user)
}

export const verifyPhone = (phone) =>{
  let url = '/phone/verify';
  return instance.post(url,phone)
}

export const updateUser = (userId,data) =>{
  let url = `/users/edit/${userId}`;
  return instance.post(url,data)
}

export const checkCodePhone = (body) =>{
  const url = `/phone/check-code`;
  return instance.post(url,body);
}

export const getUserProfile = (id) => {
  let url = `/users/${id}`;
  return instance.get(url);
}

export const resetPassword = (data) => {
  let url = `/auth/reset-password`;
  return instance.post(url,data);
}

export const getUserPhone = (body) => {
  let url = `/users/phone`;
  return instance.post(url,body);
}

export const logout = (refreshToken) => {
  let url = "/auth/logout";
  return instance.post(url, refreshToken);
}

export const refreshTokenApi = (refreshToken) =>{
  let url = "/auth/refresh-tokens";
  return instance.post(url,refreshToken);
}