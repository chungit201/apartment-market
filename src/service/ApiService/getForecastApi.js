import instance from "./instance";

export const getForecast = (data) => {
  let url = `/weather/getForecast`;
  return instance.post(url,data)
}