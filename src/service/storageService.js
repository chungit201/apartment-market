export const getData = (key, defaultValue) => {
   return new Promise((resolve, reject) => {
      const data = JSON.parse(localStorage.getItem(key));
      resolve(data || defaultValue);
   })
}

export const setData = async (key, value) => {
   await localStorage.setItem(key, JSON.stringify(value));
}

export const removeData = (key) => {
   localStorage.removeItem(key);
}

export const authDataStorage = (tokens) => {
   return new Promise(async (resolve) => {
      try {
         const { refresh, access } = tokens;
         if(!refresh) resolve(false);
         await setData('ACCESS_TOKEN', access);
         await setData('REFRESH_TOKEN', refresh);
         resolve(true);
      } catch(err) {
         resolve(false);
      }
   })
}